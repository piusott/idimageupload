/*
 * Copyright (C) The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.google.android.gms.samples.vision.face.facetracker.ui.camera;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.Log;

import com.abcorp.abid.idimageupload.MainActivity;
import com.abcorp.abid.imagedetection.Marker;
import com.abcorp.abid.imagedetection.SignatureCrop;
import com.google.android.gms.vision.face.Face;

import org.opencv.core.Point;

import java.util.List;
import java.util.Vector;

/**
 * Graphic instance for rendering face position, orientation, and landmarks within an associated
 * graphic overlay view.
 */
public class FaceGraphic extends GraphicOverlay.Graphic
{
	private static final float FACE_POSITION_RADIUS = 10.0f;
	private static final float ID_TEXT_SIZE = 40.0f;
	private static final float ID_Y_OFFSET = 50.0f;
	private static final float ID_X_OFFSET = -50.0f;
	private static final float BOX_STROKE_WIDTH = 5.0f;
	private static final float SIGBOX_STROKE_WIDTH = 25.0f;

	private static final int COLOR_CHOICES[] = {
			  /* pto - for the moment, we're only ever expecting one face in the image so keep the colour the same for
			           every capture. Otherwise it confuses people because they think the colour represents a particular
			           status.
			  Color.BLUE,
			  Color.CYAN,
			  Color.GREEN,
			  Color.MAGENTA,
			  Color.RED,
			  Color.WHITE, */
			  Color.YELLOW
	};
	private static int mCurrentColorIndex = 0;

	private Paint mFacePositionPaint;
	private Paint mIdPaint;
	private Paint mMarkerPaint;
	private Paint mBoxPaint;
	private Paint mMarkerBoxPaint;
	private Paint mSigBoxPaint;

	private volatile Face mFace;
	private volatile RectF mFaceRect;
	private volatile Rect mSigRect;
	private volatile Rect mQRRect;
	private int mFaceId;
	private float mFaceHappiness;

	private Vector<Marker> mSigMarkers;		// Temp - needs to be done properly. For the moment I just want a quick way to show markers with the face
	private Vector<SignatureCrop.SparseMarker> mAdjustedSigMarkers;	// Markers that have been adjusted for the preview size and rotated
	private int mCanvasWidth;

	public FaceGraphic(GraphicOverlay overlay)
	{
		super(overlay);

		mCurrentColorIndex = (mCurrentColorIndex + 1) % COLOR_CHOICES.length;
		final int selectedColor = COLOR_CHOICES[mCurrentColorIndex];

		mFacePositionPaint = new Paint();
		mFacePositionPaint.setColor(selectedColor);

		mIdPaint = new Paint();
		mIdPaint.setColor(selectedColor);
		mIdPaint.setTextSize(ID_TEXT_SIZE);

		mMarkerPaint = new Paint();
		mMarkerPaint.setColor(Color.CYAN);
		mMarkerPaint.setTextSize(ID_TEXT_SIZE);

		mBoxPaint = new Paint();
		mBoxPaint.setColor(selectedColor);
		mBoxPaint.setStyle(Paint.Style.STROKE);
		mBoxPaint.setStrokeWidth(BOX_STROKE_WIDTH);

		mMarkerBoxPaint = new Paint();
		mMarkerBoxPaint.setColor(Color.CYAN);
		mMarkerBoxPaint.setStyle(Paint.Style.STROKE);
		mMarkerBoxPaint.setStrokeWidth(BOX_STROKE_WIDTH);

		// make the signature box somewhat transparent so we can see if part of the signature would get cropped off
		mSigBoxPaint = new Paint();
		/*mSigBoxPaint.setColor(Color.BLACK);
		mSigBoxPaint.setAlpha(75);
		mSigBoxPaint.setStyle(Paint.Style.STROKE);
		mSigBoxPaint.setStrokeWidth(SIGBOX_STROKE_WIDTH);*/
		mSigBoxPaint.setColor(Color.YELLOW);
		mSigBoxPaint.setStyle(Paint.Style.STROKE);
		mSigBoxPaint.setStrokeWidth(BOX_STROKE_WIDTH);

		mFaceRect = new RectF();

		if (mAdjustedSigMarkers == null)
			mAdjustedSigMarkers = new Vector<>();
	}

	// -----------------------------------------------------------------------------------------------------------------

	public void setMarkers(Vector<Marker> markers)
	{
		mSigMarkers = markers;

		// the markers we receive in this function are based on a differently sized landscape image. Rotate by 90 degrees
		// and adjust the position to fit our preview image size
		if (mAdjustedSigMarkers == null)
			mAdjustedSigMarkers = new Vector<>();

		mAdjustedSigMarkers.clear();
		if ((mSigMarkers != null) && (mCanvasWidth > 0))
		{
			for (Marker m : mSigMarkers)
			{
				SignatureCrop.SparseMarker rotatedMarker = new SignatureCrop.SparseMarker(m.getId());

				List<Point> points = new Vector<>();

				for (Point p : m.getPoints())
				{
					Point pRotated = rotateAndAdjust(p, mCanvasWidth);
					points.add(pRotated);
				}
				rotatedMarker.setPoints(points);
				mAdjustedSigMarkers.add(rotatedMarker);
			}
		}
	}

	// -----------------------------------------------------------------------------------------------------------------

	private Point rotateAndAdjust(Point p, int canvasWidth)
	{
		// change coordinates according to our preview image size
		float fromX = translateX((float)p.x);
		float fromY = translateY((float)p.y);

		// Markers are detected on landscape image, rotate back to portrait
		return rotate90Degrees(fromX, fromY, canvasWidth);
	}

	// -----------------------------------------------------------------------------------------------------------------

	public void setId(int id)
	{
		mFaceId = id;
	}


	/**
	 * Updates the face instance from the detection of the most recent frame.  Invalidates the
	 * relevant portions of the overlay to trigger a redraw.
	 */
	public void updateFace(Face face)
	{
		mFace = face;
		postInvalidate();
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * The coordinates are detected on a landscape image while we're showing a portrait image. To get the correct
	 * coordinates we need to rotate the data clockwise by 90 degrees
	 * @return
	 */
	private Point rotate90Degrees(float x, float y, int canvasWidth)
	{
		Point result = new Point();
		result.y = x;
		result.x = canvasWidth - y;
		return result;
	}

	// -----------------------------------------------------------------------------------------------------------------

	private void drawSigMarkers(Canvas canvas)
	{
		if (mAdjustedSigMarkers != null)
		{
			for (SignatureCrop.SparseMarker marker : mAdjustedSigMarkers)
			{
				List<Point> pt = marker.getPoints();

				if (pt.size() == 4)
				{
					Point p1 = pt.get(0);
					Point p2 = pt.get(1);
					Point p3 = pt.get(2);
					Point p4 = pt.get(3);

					// We need to draw the markers as lines rather than a rectangle because of the way the camera is
					// likely angled towards the sheet, they're unlikely to be perfectly square.
					canvas.drawLine((float)p1.x, (float)p1.y, (float)p2.x, (float)p2.y, mMarkerBoxPaint);
					canvas.drawLine((float)p2.x, (float)p2.y, (float)p3.x, (float)p3.y, mMarkerBoxPaint);
					canvas.drawLine((float)p3.x, (float)p3.y, (float)p4.x, (float)p4.y, mMarkerBoxPaint);
					canvas.drawLine((float)p4.x, (float)p4.y, (float)p1.x, (float)p1.y, mMarkerBoxPaint);

					Point p = marker.GetTopLeft();
					canvas.drawText(String.format("%d", marker.Id), (float)p.x + 5f, (float)p.y + 35f, mMarkerPaint);
				}
			}

			Vector<Rect> imgRect = SignatureCrop.getSignatureRect(mAdjustedSigMarkers);
			if ((imgRect != null) && (imgRect.size() == 2))
			{
				mSigRect = imgRect.get(SignatureCrop.RECT_ID_SIGNATURE);
				mQRRect = imgRect.get(SignatureCrop.RECT_ID_QR);
/*				Rect drawSigRect = mSigRect;
				// resize rectangle to show whole signature for our large brush width rectangle
				drawSigRect.left = drawSigRect.left - (int)SIGBOX_STROKE_WIDTH;
				drawSigRect.top = drawSigRect.top - (int)SIGBOX_STROKE_WIDTH;
				drawSigRect.right = drawSigRect.right + (2 * (int)SIGBOX_STROKE_WIDTH);
				drawSigRect.bottom = drawSigRect.bottom + (2 * (int)SIGBOX_STROKE_WIDTH);

				canvas.drawRect(drawSigRect, mSigBoxPaint);
*/
				canvas.drawRect(mSigRect, mSigBoxPaint);
				canvas.drawRect(mQRRect, mSigBoxPaint);
			}
		}
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * Draws the face annotations for position on the supplied canvas.
	 */
	@Override
	public void draw(Canvas canvas)
	{
		if (mCanvasWidth == 0)
			mCanvasWidth = canvas.getWidth();

		drawSigMarkers(canvas);

		Face face = mFace;
		if (face == null)
		{
			return;
		}

		// Draws a circle at the position of the detected face, with the face's track id below.
		float x = translateX(face.getPosition().x + face.getWidth() / 2);
		float y = translateY(face.getPosition().y + face.getHeight() / 2);
		canvas.drawCircle(x, y, FACE_POSITION_RADIUS, mFacePositionPaint);
		canvas.drawText("id: " + mFaceId, x + ID_X_OFFSET, y + ID_Y_OFFSET, mIdPaint);
		canvas.drawText("happiness: " + String.format("%.2f", face.getIsSmilingProbability()), x - ID_X_OFFSET, y - ID_Y_OFFSET, mIdPaint);
		canvas.drawText("right eye: " + String.format("%.2f", face.getIsRightEyeOpenProbability()), x + ID_X_OFFSET * 2, y + ID_Y_OFFSET * 2, mIdPaint);
		canvas.drawText("left eye: " + String.format("%.2f", face.getIsLeftEyeOpenProbability()), x - ID_X_OFFSET*2, y - ID_Y_OFFSET*2, mIdPaint);

		// Draws a bounding box around the face.
		float xOffset = scaleX(face.getWidth() / 2.0f);
		float yOffset = scaleY(face.getHeight() / 2.0f);
		float left = x - xOffset;
		float top = y - yOffset;
		float right = x + xOffset;
		float bottom = y + yOffset;

		// because we'll eventually add some padding when cropping the face, also show the larger rectangle here to
		// make sure that we don't end up attempting to capture part of the headshot that sits outside the captured image
		int horzPadding = (int) (((right - left) / 100) * 15); // 15% at either side = total padding = 30%

		// For vertical padding we add less at the top than at the bottom because we want to see some of the
		// neck and chest while we don't need much padding at the top
		int vertPaddingTop = (int) (((bottom - top) / 100) * 8);
		int vertPaddingBottom = (int) (((bottom - top) / 100) * 32);

		mFaceRect.left = (left - horzPadding);
		mFaceRect.top = top - vertPaddingTop;
		mFaceRect.right = right + horzPadding;
		mFaceRect.bottom = bottom + vertPaddingBottom;
		canvas.drawRect(mFaceRect, mBoxPaint);  // (left - horzPadding, top - vertPaddingTop, right + horzPadding, bottom + vertPaddingBottom, mBoxPaint);
		//canvas.drawRect(left - horzPadding, top - vertPaddingTop, right + horzPadding, bottom + vertPaddingBottom, mBoxPaint);
	}

	// -----------------------------------------------------------------------------------------------------------------

	public RectF getFaceRectangle()
	{
		return mFaceRect;
	}

	public Rect getSigRectangle()
	{
		return mSigRect;
	}

	public Rect getQRRectangle()
	{
		return mQRRect;
	}

	// -----------------------------------------------------------------------------------------------------------------

}
