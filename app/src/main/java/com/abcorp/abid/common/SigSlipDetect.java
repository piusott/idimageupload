package com.abcorp.abid.common;

import android.graphics.Bitmap;
import android.graphics.Rect;
import android.util.Log;

import com.abcorp.abid.idimageupload.MainActivity;
import com.abcorp.abid.imagedetection.FindQRCode;
import com.abcorp.abid.imagedetection.Marker;
import com.abcorp.abid.imagedetection.MarkerDetector;
import com.abcorp.abid.imagedetection.SignatureCrop;
import com.google.zxing.NotFoundException;

import java.util.Vector;

/**
 * Helper class that finds a signature image and a QR code from a source image, based on the location of a couple of
 * ArUCO markers on the original image.
 */
public class SigSlipDetect
{
	private Bitmap mSourceImage;			// Source image from camera capture
	private Bitmap mQRImage;				// Cropped image that has the QR code in it

	// Deciphered data that will be populated once the source image has been inspected
	public Bitmap SignatureImage;					// Signature slip part of source image

	public String CardType = "";					// Will be populated with the card type when we read the QR data
	public String HolderID = "";					// Will be populated with the Holder ID when we read the QR data
	public String ApplicantName = "";			// Will be populated with the applicant name when we read the QR data

	// -----------------------------------------------------------------------------------------------------------------

	public SigSlipDetect(Bitmap sourceImage)
	{
		mSourceImage = sourceImage;
		decipherImage();
	}

	// -----------------------------------------------------------------------------------------------------------------

	private void decipherImage()
	{
		try
		{
			cropSignatureAndQR(mSourceImage);

			if ((SignatureImage == null) || (mQRImage == null))
			{
				if (SignatureImage == null)
					Log.i(MainActivity.TAG, "No signature detected");

				if (mQRImage == null)
					Log.i(MainActivity.TAG, "No QR code detected");
			}
		}
		catch(Exception e)
		{
			Log.e(MainActivity.TAG, "Exception on creating sigslip image : " + e.getMessage());
			SignatureImage = null;
			mQRImage = null;
		}

		Log.d(MainActivity.TAG, "** 8.4. About to set signature image and detect QR data");

		try
		{
			FindQRCode.QRData qrValue = FindQRCode.readQRCode(mQRImage);
			Log.d(MainActivity.TAG, "** 8.5. Read QR code complete");
			ApplicantName = qrValue.HolderName;
			CardType = qrValue.CardType;
			HolderID = qrValue.HolderId + " " + qrValue.CheckDigit;
		}
		catch(NotFoundException e)
		{
			Log.e(MainActivity.TAG, "QR Code not found exception: " + e.getMessage());
			ApplicantName = "-";
			CardType = "-";
			HolderID = "*** Not detected ***";
		}
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * Find the signature and QR code rectangle on the signature slip. Although we do keep track of those locations
	 * as we're live previewing the camera, I've stopped using that data. This is because there is a bit of a delay
	 * between instructing the camera to take a shot and it actually capturing a photo. During this delay the operator
	 * often moves the handset, which means the image location data is no longer accurate.
	 * Since it's a very quick operation to re-detect them on the passed in source image, I'm doing that here which
	 * results in much more accurate image locations.
	 * @param sourceImage captured photo of the signature slip
	 * @return Vector containing rectangles of the signature and QR locations within the source image
	 */
	private Vector<Rect> findSigAndQRRect(Bitmap sourceImage)
	{
		Vector<Rect> result = new Vector<>();

		// detect signature markers from captured image
		Vector<Integer> validMarkers = new Vector<>();
		validMarkers.add(SignatureCrop.MARKER_ID_TOPLEFT);   // IDs of our two markers on the signature slip, bottom-right marker
		validMarkers.add(SignatureCrop.MARKER_ID_BOTTOMRIGHT);   // top-left marker

		MarkerDetector md = new MarkerDetector(validMarkers);
		Vector<Marker> markers = md.processFrame(sourceImage);

		if (markers.size() == 2)
		{
			Vector<SignatureCrop.SparseMarker> sigMarkers = new Vector<>();
			Marker m1 = markers.elementAt(0);
			SignatureCrop.SparseMarker sm1 = new SignatureCrop.SparseMarker(m1.getId());
			sm1.setPoints(m1.getPoints());
			Marker m2 = markers.elementAt(1);
			SignatureCrop.SparseMarker sm2 = new SignatureCrop.SparseMarker(m2.getId());
			sm2.setPoints(m2.getPoints());

			if (m1.getId() == SignatureCrop.MARKER_ID_TOPLEFT)
			{
				sigMarkers.add(0, sm1);
				sigMarkers.add(1, sm2);
			}
			else
			{
				sigMarkers.add(0, sm2);
				sigMarkers.add(1, sm1);
			}

			result = SignatureCrop.getSignatureRect(sigMarkers);
		}

		return result;
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * Crop the signature based on the ArUco Markers on the signature slip. Also create the image that holds the QR
	 * code so we can speed up detecting the QR code data.
	 */
	private void cropSignatureAndQR(Bitmap sourceImage)
	{
		Vector<Rect> imgRecs = findSigAndQRRect(sourceImage);
		if ((imgRecs != null) && (imgRecs.size() == 2))
		{
			Rect sigRect = imgRecs.get(SignatureCrop.RECT_ID_SIGNATURE);

			Log.d(MainActivity.TAG, "** 8.1.4 Creating signature image");
			if (sigRect.left < 0)
				sigRect.left = 0;

			if (sigRect.top < 0)
				sigRect.top = 0;

			if (sigRect.right > (sourceImage.getWidth()))
				sigRect.right = sourceImage.getWidth();

			if (sigRect.bottom > sourceImage.getHeight())
				sigRect.bottom = sourceImage.getHeight();

			SignatureImage = Bitmap.createBitmap(sourceImage, sigRect.left, sigRect.top, sigRect.width(), sigRect.height());
			//// Testing - save Sig image so I can check if the cropping rectangle looks ok
			MainActivity.saveBitmap(SignatureImage, MainActivity.getOutputMediaFile("Test", "-", "SIG"), 85);

			Rect qrRect = imgRecs.get(SignatureCrop.RECT_ID_QR);
			Log.d(MainActivity.TAG, "** 8.1.5 Creating QR image");

			if (qrRect.left < 0)
				qrRect.left = 0;

			if (qrRect.top < 0)
				qrRect.top = 0;

			if (qrRect.right > (sourceImage.getWidth()))
				qrRect.right = sourceImage.getWidth();

			if (qrRect.bottom > sourceImage.getHeight())
				qrRect.bottom = sourceImage.getHeight();

			mQRImage = Bitmap.createBitmap(sourceImage, qrRect.left, qrRect.top, qrRect.width(), qrRect.height());

			//// Testing - save QR image so I can check if the cropping rectangle looks ok
			MainActivity.saveBitmap(mQRImage, MainActivity.getOutputMediaFile("Test", "-", "QR"), 85);
		}
	}

	// -----------------------------------------------------------------------------------------------------------------


}
