package com.abcorp.abid.common;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.util.Log;

import com.abcorp.abid.idimageupload.MainActivity;

import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import java.nio.ByteBuffer;

/**
 * Created by pott on 17/5/17.
 * Ported from LICS/ABiD - Class that transforms a 1-bit signature image into a smoother, more readable and printable
 * signature. The signature can also be more easily scaled while retaining its image quality.
 */

public class SignatureTransform
{
	private byte[] mRawImageBright;		// array that holds brightness level of each pixel in the image
	private boolean[] mResult;				// 1-bit result array. True = black, false = white

	private int mImageWidth;
	private int mImageHeight;
	private Bitmap mGrayImg;				// grayscale converted source image

	// Using RGB_565, the blue value, which we use as an indicator of brightness here, is stored in 5 bits, so the
	// maximum value it can have is 1F (31)
	private final int MAX_BLUE_VALUE = 0x1F;

	// --------------------------------------------------------------------------------------------------------------

	public SignatureTransform(Bitmap sourceImage)
	{
		if (sourceImage != null)
		{
			mImageWidth = sourceImage.getWidth();
			mImageHeight = sourceImage.getHeight();

			mGrayImg = toGrayscale(sourceImage);      // convert image to grayscale and populate mRawImageBright
		}
	}

	// --------------------------------------------------------------------------------------------------------------

	private Bitmap toGrayscale(Bitmap bmpOriginal)
	{
		Bitmap bmpGrayscale = Bitmap.createBitmap(mImageWidth, mImageHeight, Bitmap.Config.RGB_565);
		Canvas c = new Canvas(bmpGrayscale);
		Paint paint = new Paint();
		ColorMatrix cm = new ColorMatrix();
		cm.setSaturation(0);
		ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
		paint.setColorFilter(f);
		c.drawBitmap(bmpOriginal, 0, 0, paint);

		int bufferSize = bmpGrayscale.getByteCount();
		ByteBuffer origImage = ByteBuffer.allocate(bufferSize);
		bmpGrayscale.copyPixelsToBuffer(origImage);

		byte[] bufRGB = origImage.array();

		mRawImageBright = new byte[mImageWidth * mImageHeight];
		int offset = 0;
		for (int i = 0; i < bufRGB.length; i+= 2)
		{
			// RGB_565 is stored with two bytes for every pixel, (R -> 5 bits, G -> 6 bits, B -> 5 bits)
			// find an average of all three colour levels which gives us a rough idea of the intensity
			int redVal = (bufRGB[i] & 0xFF);
			redVal = (redVal >> 3);
			int greenVal1 = (((bufRGB[i] & 7) << 3));
			int greenVal2 = (((bufRGB[i + 1] & 0xFF) >> 5));
			int greenVal = (greenVal1 | greenVal2);
			//int greenVal = (((bufRGB[i] & 7) << 3) | (bufRGB[i + 1] >> 5));
			byte blueVal = (byte)(bufRGB[i + 1] & MAX_BLUE_VALUE);

			byte avg = (byte)(((redVal * 2) + greenVal + (blueVal * 2)) / 6);
			mRawImageBright[offset++] = avg;
		}

		return bmpGrayscale;
	}

	// --------------------------------------------------------------------------------------------------------------

	public Bitmap getImage()
	{
		if ((mResult == null) || (mResult.length == 0))
			return null;

		Bitmap result = Bitmap.createBitmap(mImageWidth, mImageHeight, Bitmap.Config.RGB_565);
		try
		{
			byte[] tmp = new byte[mResult.length * 2];
			int offset = 0;
			for(int i = 0; i < mResult.length; i++)
			{
				if (mResult[i])
				{
					// black pixel
					tmp[offset++] = 0;
					tmp[offset++] = 0;
				}
				else
				{
					// white pixel
					tmp[offset++] = (byte)0xFF;
					tmp[offset++] = (byte)0xFF;
				}
			}

			ByteBuffer buffer = ByteBuffer.wrap(tmp);
			result.copyPixelsFromBuffer(buffer);
		}
		catch (Exception ex)
		{
			Log.e(MainActivity.TAG, "Exception creating Signature image : " + ex.getMessage());
		}
		return result;
	}

	// --------------------------------------------------------------------------------------------------------------

	public Bitmap getImageGrayscale()
	{
		if ((mRawImageBright == null) || (mRawImageBright.length == 0))
			return null;

		Bitmap result = Bitmap.createBitmap(mImageWidth, mImageHeight, Bitmap.Config.RGB_565);
		try
		{
			int offset = 0;
			byte[] tmp = new byte[mImageHeight * mImageWidth * 2];
			for (int i = 0; i < mRawImageBright.length; i++)
			{
				// convert from 0->255 brightness value to 0->31 blue value
				byte blueVal = mRawImageBright[i];
				byte greenValB1 = (byte)(blueVal >> 3);
				byte greenValB2 = (byte)(blueVal << 5);
				byte redVal = (byte)(blueVal << 3);
				byte b1 = (byte)(redVal | greenValB1);
				byte b2 = (byte)(greenValB2 | blueVal);
				tmp[offset++] = b1;
				tmp[offset++] = b2;
			}
			ByteBuffer buffer = ByteBuffer.wrap(tmp);
			result.copyPixelsFromBuffer(buffer);
		}
		catch (Exception ex)
		{
			Log.e(MainActivity.TAG, "Exception creating Signature image : " + ex.getMessage());
		}
		return result;
	}

	// --------------------------------------------------------------------------------------------------------------

	/**
	 * Remove tiny spots which are usually introduced when converting the colour/grayscale image into a 1-bit image.
	 * We do this by inspecting each pixel and evaluating all surrounding pixels. If the pixel is black but all or
	 * most of the surrounding pixels are white, it's likely that this is a spot and we change it to white as well.
	 */
	public void KillSpots()
	{
		/*
		final int LIFETHRESH = 6;

		int lineWidth = mImageWidth * 2;	// each pixel uses two bytes in RGB_565
		byte[] imgSrc = new byte[mRawImageResult.length];

		// set the first two lines to white
		java.util.Arrays.fill(mRawImageResult, 0, (lineWidth * 2), (byte)0xFF);

		System.arraycopy(mRawImageResult, 0, imgSrc, 0, mRawImageResult.length);
		byte[] pSrc = new byte[5 * lineWidth];
		for (int i = 0; i < 4; i++)	// Assign a group of lines to the array
		{
			System.arraycopy(imgSrc, lineWidth * i, pSrc, lineWidth * i, lineWidth);
		}

		// Loop through each line in the image except the first and last two
		for (int i = 2; i < mImageHeight - 2; i++)
		{
			// Point to new line of data
			System.arraycopy(imgSrc, lineWidth * (i + 2), pSrc, ((i + 2) % 5) * lineWidth, lineWidth);
			byte[] pLine = new byte[lineWidth];
			System.arraycopy(imgSrc, lineWidth * i, pLine, 0, lineWidth);

			// Loop through each pixel in the line except the first and last two:
			for (int j = 4; j < lineWidth - 4; j += 2)
			{
				int blueVal = pSrc[((i % 5) * lineWidth) + j + 1] & MAX_BLUE_VALUE;
				if (blueVal == 0)
				{
					int iTot = 0;
					for (int k = 0; k < 5; k++)
					{
						for (int l = j - 2; l < j + 2 + 1; l+=2)
						{
							int neighbourPixel = pSrc[(k * lineWidth) + l + 1];
							if (neighbourPixel == 0)
							{
								iTot++;		// total of black pixels found
							}
						}
					}

					if (iTot > LIFETHRESH)
					{
						pLine[j] = 0;
						pLine[j+1] = 0;
					}
					else
					{
						pLine[j] = (byte)0xFF;
						pLine[j+1] = (byte)0xFF;
					}
				}
				else
				{
					pLine[j] = (byte)0xFF;
					pLine[j+1] = (byte)0xFF;
				}
			}

			// set the first and last two bytes of the line to white
			java.util.Arrays.fill(pLine, 0, 4, (byte)0xFF);
			java.util.Arrays.fill(pLine, pLine.length - 5, pLine.length - 1, (byte)0xFF);

			// copy the modified data back into our image
			System.arraycopy(pLine, 0, imgSrc, lineWidth * i, lineWidth);
		}

		// set the last two lines to white
		int lastTwoLineOffset = mRawImageResult.length - (lineWidth * 2);
		java.util.Arrays.fill(mRawImageResult, lastTwoLineOffset, mRawImageResult.length - 1, (byte)0xFF);

		System.arraycopy(imgSrc, 0, mRawImageResult, 0, mRawImageResult.length);
*/
	}

	// --------------------------------------------------------------------------------------------------------------

	/**
	 * Convert the image to a 1-bit black/white image based on a threshold value. The threshold is basically the byte
	 * value above which we interpret a pixel as black
	 * @param thresholdValue - If supplied, use this as the threshold. Otherwise attempt to find a suitable value
	 * @return - the threshold value used
	 */
	public int Threshold(int thresholdValue)
	{
		if ((mRawImageBright == null) || (mRawImageBright.length == 0))
			return 0;

		//int thresholdValue = 130; 		// default for testing
		if (thresholdValue == 0)
			thresholdValue = PickThreshold();	// if none supplied, attempt to find a suitable threshold

		// Use OpenCV to convert it to a 1-bit black/white image
		Mat tmp = new Mat (mImageWidth, mImageHeight, CvType.CV_32SC1);
		Utils.bitmapToMat(mGrayImg, tmp);

		Mat dst = new Mat();
		Imgproc.threshold(tmp, dst, thresholdValue, 255, Imgproc.THRESH_BINARY);

		Bitmap sigBitmap = Bitmap.createBitmap(mImageWidth, mImageHeight, Bitmap.Config.RGB_565);
		Utils.matToBitmap(dst, sigBitmap);

		// load the result array with boolean values
		int bufferSize = sigBitmap.getByteCount();
		ByteBuffer origImage = ByteBuffer.allocate(bufferSize);
		sigBitmap.copyPixelsToBuffer(origImage);

		byte[] buf = origImage.array();
		mResult = new boolean[mImageHeight * mImageWidth];
		int pixelIndex = 0;
		for (int i = 0; i < buf.length; i+=2)
		{
			if (buf[i] != 0)
			{
				mResult[pixelIndex++] = false;	// white pixel
			}
			else
			{
				mResult[pixelIndex++] = true;		// black pixel
			}
		}

		return thresholdValue;
	}

	// --------------------------------------------------------------------------------------------------------------

	/**
	 * Attempt to find a suitable threshold value. We do this by building a histogram of all intensity values found
	 * in the grayscale data and attempt to find a value above which we can interpret a pixel as likely being part
	 * of a signature.
	 * @return threshold value, scaled up from the 5 bit intensity level to an 8 bit value that works with OpenCV
	 */
	private int PickThreshold()
	{
		int[] palHist = new int[MAX_BLUE_VALUE];

		// Create a histogram by counting how often a brightness level is found in the image.
		for (int iOffset = 0; iOffset < mRawImageBright.length; iOffset++)
		{
			// Add the found intensity level to the histogram
			palHist[mRawImageBright[iOffset]]++;
		}

		int iDarkness = 0;
		for (int i = 0; i < MAX_BLUE_VALUE; i++)
		{
			// try and put a weight on the histogram and calculate a total "darkness" value
			iDarkness += palHist[i] * i;
		}

		int iThreshold = iDarkness / (mRawImageBright.length);
		Log.d(MainActivity.TAG, "5-bit Calculated Threshold : " + iThreshold);
		//scale up the found threshold value from a 5bit to an 8bit value
		iThreshold = ((iThreshold * 255) / 31);
		Log.d(MainActivity.TAG, "8-bit Calculated Threshold : " + iThreshold);
		iThreshold = iThreshold / 5 * 8;   // trial and error really.....
		Log.d(MainActivity.TAG, "8-bit Adjusted Calculated Threshold : " + iThreshold);
		return iThreshold;
	}

	// --------------------------------------------------------------------------------------------------------------

	/**
	 * Change the thickness of the signature lines. We do this by inspecting a pixel and a number of pixels that
	 * surround it. If we've found a black pixel, colour the surrounding ones, up to a given width, as well.
	 * @param iThickness - value between 1 (thinnest) and 5 (thickest)
	 */
	public void Thicken(int iThickness)
	{
		if ((mResult == null) || (mResult.length == 0))
			return;

		int iWidth, iCenter;
		switch (iThickness)
		{
			case 0: return;
			case 1: iWidth = 3; iCenter = 1; break;
			case 2: iWidth = 5; iCenter = 2; break;
			case 3: iWidth = 7; iCenter = 3; break;
			case 4: iWidth = 9; iCenter = 4; break;
			case 5: iWidth = 11; iCenter = 5; break;
			default: iWidth = 5; iCenter = 2; break;
		}

		// create an array to hold each individual line of the source data
		int lineWidth = mImageWidth;
		boolean[] pSrc = new boolean[iWidth * mImageWidth];

		// Loop through each line in the image
		for (int iLine = 0; iLine < mImageHeight; iLine++)
		{
			// Copy the next line of source data
			int iOffset = (iLine % iWidth) * lineWidth;
			System.arraycopy(mResult, lineWidth * iLine, pSrc, iOffset, lineWidth);

			// array that will hold our modified line data
			boolean[] pLine = new boolean[lineWidth];

			// Loop through each pixel in the line and widen any black data found
			for (int j = 0; j < lineWidth; j++)
			{
				int iTot = 0;
				for (int k = 0; k < iWidth; k++)
				{
					int iIndex = k * lineWidth; // pixel position in the line as an offset to the source array

					for (int l = j - iCenter; l < j + iCenter + 1; l++)
					{
						if ((l >= 0) && (iIndex < pSrc.length) && ((iIndex + l) < pSrc.length) && (l < lineWidth))
						{
							if (pSrc[iIndex] == false)
							{
								if (pSrc[iIndex + l] == true)
									iTot++;
							}
						}
					}
				}

				if (iTot > 0)
				{
					// black pixel
					pLine[j] = true;
				}
				else
				{
					// white pixel
					pLine[j] = false;
				}
			}

			// copy the modified data back into our image
			System.arraycopy(pLine, 0, mResult, lineWidth * iLine, lineWidth);
		}
	}

	// --------------------------------------------------------------------------------------------------------------

}
