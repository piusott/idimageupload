package com.abcorp.abid.map;

/**
 * Created by pott on 21/2/17.
 */

public class OfficeLocation
{
	public String OfficeId;		// DoT six character office ID
	public String Name;			// descriptive name
	public String Address;		// Street address
	public String Phone;			// Site's phone number
	public Double Longitude;
	public Double Latitude;
	public String Type;			// Site type, DoT Office, Agency, Handheld

//	<string name="LICALB" address="178 Sterling Tce. Albany, WA, 6331" phone="+618 98927318" type="full" lang="-35.026526" lat="117.882612" />

}
