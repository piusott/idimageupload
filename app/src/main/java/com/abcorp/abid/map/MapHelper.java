package com.abcorp.abid.map;

import android.app.AlarmManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.abcorp.abid.idimageupload.MainActivity;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;

/**
 * Created by pott on 17/3/17.
 * Very basic class that deals with map locations
 */

public class MapHelper
{

	private static Collection<OfficeLocation> mOffices = null;

	// -----------------------------------------------------------------------------------------------------------------

	public static Collection<OfficeLocation> Offices(Context ctx)
	{
		if (mOffices == null) loadOfficeLocations(ctx);

		return mOffices;
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * Check if we have access to use the GPS location details.
	 * This should really make a request if not granted, but I'll leave that off for the prototype.
	 *
	 * @param ctx context
	 * @return true if our application is allowed to use both, fine and coarse location
	 */
	public static boolean isMapPermissionsGranted(Context ctx)
	{
		int accFine = ActivityCompat.checkSelfPermission(ctx, android.Manifest.permission.ACCESS_FINE_LOCATION);
		int accCoarse = ActivityCompat.checkSelfPermission(ctx, android.Manifest.permission.ACCESS_COARSE_LOCATION);
		if ((accFine != PackageManager.PERMISSION_GRANTED) || (accCoarse != PackageManager.PERMISSION_GRANTED))
		{
			// We are not allowed locations, so we can't continue here
			return false;
		}
		return true;
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * Load the office locations from a JSON file in our assets folder.
	 */
	private static void loadOfficeLocations(Context ctx)
	{
		AssetManager asm = ctx.getAssets();
		try
		{
			Gson gson = new GsonBuilder().create();

			try
			{
				String persoFile = "dot_site_locations.json";
				InputStream is = asm.open(persoFile);
				int size = is.available();
				byte[] buffer = new byte[size];
				is.read(buffer);
				is.close();
				String jsonData = new String(buffer, "UTF-8");

				Type collectionType = new TypeToken<Collection<OfficeLocation>>()
				{
				}.getType();
				mOffices = gson.fromJson(jsonData, collectionType);
			}
			catch(FileNotFoundException ex)
			{
				Log.e(MainActivity.TAG, "FileNotFoundException on loading office sites : " + ex.getMessage());
			}
		}
		catch(Exception e)
		{
			Log.e(MainActivity.TAG, "Exception on loading office sites : " + e.getMessage());
		}
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * Find our current location. Used to make sure we can restrict the phone to only work within a given range
	 * @param ctx application context
	 * @return LatLng of current location
	 */
	public static LatLng getMyLocation(Context ctx)
	{
		LocationManager service = (LocationManager)ctx.getSystemService(Context.LOCATION_SERVICE);
		Location myLocation = getLocation(service);
		if (myLocation != null)
			return new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
		else
			return null;
	}

	// -----------------------------------------------------------------------------------------------------------------


	private static Location getLocation(LocationManager service)
	{
		Location bestResult = null;
		Criteria criteria = new Criteria();
		criteria.setAccuracy(Criteria.ACCURACY_FINE);

		String bestProvider = service.getBestProvider(criteria, true);

		try
		{
			bestResult = service.getLastKnownLocation(bestProvider);
			if (bestResult == null)
			{
				// often our "best provider" does not have a current last known location available and returns null. If
				// that's the case then go through all providers and pick the most recent reading instead.
				float bestAccuracy = Float.MAX_VALUE;
				long bestTime = Long.MIN_VALUE;
				long minTime = System.currentTimeMillis() - AlarmManager.INTERVAL_FIFTEEN_MINUTES;

				List<String> providers = service.getProviders(false);
				for(String provider : providers)
				{
					Location loc = service.getLastKnownLocation(provider);
					if (loc != null)
					{
						float accuracy = loc.getAccuracy();
						long time = loc.getTime();

						if (((time > minTime) && (accuracy < bestAccuracy)))
						{
							bestResult = loc;
							bestAccuracy = accuracy;
							bestTime = time;
						}
						else if ((time < minTime) && (bestAccuracy == Float.MAX_VALUE) && (time > bestTime))
						{
							bestResult = loc;
							bestTime = time;
						}
					}
				}
			}
		}
		catch (SecurityException ex)
		{
			Log.e(MainActivity.TAG, "Exception in getLocation : " + ex.getMessage());
			ex.printStackTrace();
		}

		return bestResult;
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * Find the absolute distance from our location to a particular site so we can determine which site is closed to us
	 * @param site - The DoT office, agency or IUS site's coordinates
	 * @param myLocation - Our own location read from the phone's GPS
	 * @return - Distance in meters between the two locations
	 */
	public static Double getDistanceToSite(LatLng site, LatLng myLocation)
	{
		if ((myLocation == null) || (site == null))
			return (double)-1;

		double earthRadius = 3958.75;
		double latDiff = Math.toRadians(site.latitude - myLocation.latitude);
		double lngDiff = Math.toRadians(site.longitude - myLocation.longitude);
		double a = Math.sin(latDiff /2) * Math.sin(latDiff /2) +
				  Math.cos(Math.toRadians(myLocation.latitude)) * Math.cos(Math.toRadians(site.latitude)) *
							 Math.sin(lngDiff / 2) * Math.sin(lngDiff /2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double distance = earthRadius * c;

		int meterConversion = 1609;

		return (distance * meterConversion);
	}

	// -----------------------------------------------------------------------------------------------------------------

}
