package com.abcorp.abid.imagedetection;

import android.support.annotation.NonNull;

import java.util.Collections;
import java.util.List;
import java.util.Vector;

import org.opencv.calib3d.Calib3d;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.MatOfPoint3f;
import org.opencv.core.Point;
import org.opencv.core.Point3;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;


/**
 * Marker detected in an image. It must be a four-squared contour with black border and
 * a valid code inside it. Modified version of Aruco Marker class by Rafael Ortega
 *
 * @author Dan Szafir, Rafael Ortega
 */
public class Marker implements Comparable<Marker>
{

	protected int id;
	protected float size;
	private int rotations;

	private Code code; // a matrix of integer representing the code (see the class to further explanation)

	private MatOfPoint2f mat; //the cvMat representing the camera capture of the marker
	private List<Point> points; //mat in list form

	private double perimeter;

	private Mat cononicalMat; // the cvMat of the CANONICAL marker (not the one taken from the capture)

	private Point center;

	// -----------------------------------------------------------------------------------------------------------------

	private Marker()
	{
		id = -1;
		code = new Code();
		cononicalMat = new Mat();
		mat = new MatOfPoint2f();
	}

	// -----------------------------------------------------------------------------------------------------------------

	public Marker(float size, Vector<Point> p)
	{
		this();
		this.size = size;
		points = p;
		this.mat.fromList(points);

		calcCenter();
		calcPerimeter();
	}

	// -----------------------------------------------------------------------------------------------------------------

	private void calcPerimeter()
	{
		perimeter = 0;
		for(int i=0; i<4; ++i)
		{
			perimeter += pointDistance(points.get(i), points.get((i+1)%4));
		}
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * Returns the perimeter of the marker, the addition of the distances between
	 * consecutive points.
	 * @return the perimeter.
	 */
	public double perimeter()
	{
		return perimeter;
	}

	// -----------------------------------------------------------------------------------------------------------------

	private void calcCenter()
	{
		setPointsCounterClockwise();

		center = new Point(0,0);

		for(int i = 0; i < 4; i++)
		{
			center.x += points.get(i).x;
			center.y += points.get(i).y;
		}
		center.x /= 4.;
		center.y /= 4.;
	}

	// -----------------------------------------------------------------------------------------------------------------

	public Mat getMat()
	{
		return mat;
	}

	// -----------------------------------------------------------------------------------------------------------------

	protected void setCononicalMat(Mat in)
	{
//		in.copyTo(cononicalMat);
		cononicalMat = in;
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * construct the matrix of integers from the mat stored.
	 */
	protected void extractCode()
	{

		int rows = cononicalMat.rows();
		int cols = cononicalMat.cols();
		assert(rows == cols);
		Mat grey = new Mat();
		// change the color space if necessary
		if(cononicalMat.type() == CvType.CV_8UC1)
			grey = cononicalMat;
		else
			Imgproc.cvtColor(cononicalMat, grey, Imgproc.COLOR_RGBA2GRAY);

		// apply a threshold
		Imgproc.threshold(grey, grey, 125, 255, Imgproc.THRESH_OTSU);
		// the swidth is the width of each row
		int swidth = rows / 7;
		// we go through all the rows
		for(int y = 0; y < 7; ++y)
		{
			for(int x = 0; x < 7; ++x)
			{
				int Xstart = x * swidth;
				int Ystart = y * swidth;
				Mat square = grey.submat(Xstart, Xstart + swidth, Ystart, Ystart + swidth);
				int nZ = Core.countNonZero(square);
				if(nZ > (swidth * swidth) / 2)
					code.set(x, y, 1);
				else
					code.set(x, y, 0);
			}
		}
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * Return the id read in the code inside a marker. Each marker is divided into 7x7 regions
	 * of which the inner 5x5 contain info, the border should always be black. This function
	 * assumes that the code has been extracted previously.
	 * @return the id of the marker
	 */
	protected int calculateMarkerId()
	{
		// check all the rotations of code
		Code[] rotations = new Code[4];
		rotations[0] = code;
		int[] dists = new int[4];
		dists[0] = hammDist(rotations[0]);
		int[] minDist = {dists[0], 0};
		for(int i = 1; i < 4; i++)
		{
			// rotate
			rotations[i] = Code.rotate(rotations[i-1]);
			dists[i] = hammDist(rotations[i]);
			if(dists[i] < minDist[0])
			{
				minDist[0] = dists[i];
				minDist[1] = i;
			}
		}

		this.rotations = minDist[1];
		if(minDist[0] != 0)
		{
			return -1; // matching id not found
		}
		else
		{
			this.id = mat2id(rotations[minDist[1]]);
		}

		return id;
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * this functions checks if the whole border of the marker is black
	 * @return true if the border is black, false otherwise
	 */
	protected boolean checkBorder()
	{
		for(int i = 0; i < 7; i++)
		{
			// normally we'll only check first and last square
			int inc = 6;
			if((i == 0) || (i == 6))  // in first and last row the whole row must be checked
				inc = 1;

			for(int j = 0; j < 7; j += inc)
			{
				if (code.get(i, j) == 1)
					return false;
			}
		}
		return true;
	}

	// -----------------------------------------------------------------------------------------------------------------

	protected void setPoints(List<Point> p)
	{
		this.mat.fromList(p);
		this.points = p;

		calcCenter();
		calcPerimeter();
	}

	// -----------------------------------------------------------------------------------------------------------------

	private int hammDist(Code code)
	{
		int ids[][] = {
				  {1,0,0,0,0},
				  {1,0,1,1,1},
				  {0,1,0,0,1},
				  {0,1,1,1,0}
		};
		int dist = 0;
		for(int y = 0; y < 5; y++)
		{
			int minSum = Integer.MAX_VALUE;
			// hamming distance to each possible word
			for(int p = 0; p < 4; p++)
			{
				int sum=0;
				for(int x = 0; x < 5; x++)
					sum+= code.get(y + 1, x + 1) == ids[p][x] ? 0:1;

				minSum = sum < minSum ? sum:minSum;
			}
			dist += minSum;
		}
		return dist;
	}

	// -----------------------------------------------------------------------------------------------------------------

	private int mat2id(Code code)
	{
		int val = 0;
		for(int x = 1; x < 6; x++)
		{
			val <<= 1;
			if(code.get(x, 2) == 1)
				val |= 1;
			val <<= 1;
			if(code.get(x, 4) == 1)
				val |= 1;
		}
		return val;
	}

	// -----------------------------------------------------------------------------------------------------------------

	public int getRotations()
	{
		return this.rotations;
	}

	// -----------------------------------------------------------------------------------------------------------------

	@Override
	public int compareTo(@NonNull Marker other)
	{
		return id - other.id;
	}

	// -----------------------------------------------------------------------------------------------------------------

	@Override
	public boolean equals(Object o)
	{
		if (!(o instanceof Marker))
			return false;

		Marker other = (Marker)o;

		return this.id == other.id;
	}

	// -----------------------------------------------------------------------------------------------------------------

	public int getId()
	{
		return id;
	}

	// -----------------------------------------------------------------------------------------------------------------

	private void setPointsCounterClockwise()
	{
		// trace a line between the first and second point.
		// if the third point is at the right side, then the points are counter-clockwise
		double dx1 = points.get(1).x - points.get(0).x;
		double dy1 = points.get(1).y - points.get(0).y;
		double dx2 = points.get(2).x - points.get(0).x;
		double dy2 = points.get(2).y - points.get(0).y;
		double o = dx1*dy2 - dy1*dx2;
		if(o < 0.0)
		{ // the third point is in the left side, we have to swap
			Collections.swap(points, 1, 3);
		}

		mat.fromList(points);
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * Calculate average distance from one marker to another
	 * @param m1
	 * @param m2
	 * @return
	 */
	public static double distance(Marker m1, Marker m2)
	{
		if (m1 == null || m2 == null) throw new IllegalArgumentException("Error: null input");

		if (m1.center == null) m1.calcCenter();
		if (m2.center == null) m2.calcCenter();

		return Math.sqrt(Math.pow(m1.center.x - m2.center.x,2) +
				  Math.pow(m1.center.y - m2.center.y, 2));


//		dist+=Math.sqrt(Math.pow(m1.points.get(0).x - m2.points.get(0).x,2) +
//				Math.pow(m1.points.get(0).y - m2.points.get(0).y,2));
//
//		dist+=Math.sqrt((fromPoints.get(1).x-toPoints.get(1).x)*(fromPoints.get(1).x-toPoints.get(1).x)+
//				(fromPoints.get(1).y-toPoints.get(1).y)*(fromPoints.get(1).y-toPoints.get(1).y));
//
//		dist+=Math.sqrt((fromPoints.get(2).x-toPoints.get(2).x)*(fromPoints.get(2).x-toPoints.get(2).x)+
//				(fromPoints.get(2).y-toPoints.get(2).y)*(fromPoints.get(2).y-toPoints.get(2).y));
//
//		dist+=Math.sqrt((fromPoints.get(3).x-toPoints.get(3).x)*(fromPoints.get(3).x-toPoints.get(3).x)+
//				(fromPoints.get(3).y-toPoints.get(3).y)*(fromPoints.get(3).y-toPoints.get(3).y));
//
//		return dist/4;
	}

	// -----------------------------------------------------------------------------------------------------------------

	public List<Point> getPoints()
	{
		return points;
	}

	// -----------------------------------------------------------------------------------------------------------------

	public float getSize(){
		return size;
	}

	// -----------------------------------------------------------------------------------------------------------------

	private static double pointDistance(Point p1, Point p2)
	{
		return Math.sqrt(Math.pow(p1.x-p2.x,2) + Math.pow(p1.y-p2.y, 2));
	}

	// -----------------------------------------------------------------------------------------------------------------

	public void draw(Mat in, Scalar color, int lineWidth, boolean writeId)
	{
/*
		//Set up the cube
		double halfSize = size / 2.0;
		Vector<Point3> cubeVectorPoints = new Vector<Point3>();
		cubeVectorPoints.add(new Point3(-halfSize, -halfSize, 0));
		cubeVectorPoints.add(new Point3(-halfSize, halfSize, 0));
		cubeVectorPoints.add(new Point3(halfSize, halfSize, 0));
		cubeVectorPoints.add(new Point3(halfSize, -halfSize, 0));
		cubeVectorPoints.add(new Point3(-halfSize, -halfSize, size));
		cubeVectorPoints.add(new Point3(-halfSize, halfSize, size));
		cubeVectorPoints.add(new Point3(halfSize, halfSize, size));
		cubeVectorPoints.add(new Point3(halfSize, -halfSize, size));

		MatOfPoint3f objectPoints = new MatOfPoint3f();
		objectPoints.fromList(cubeVectorPoints);
		MatOfPoint2f imagePoints = new MatOfPoint2f();

		//Project points into camera space
		Calib3d.projectPoints(objectPoints, rVec, tVec, cp.getCameraMatrix(), cp.getDistortionMatrixAsMat(), imagePoints);

		List<Point> pts = imagePoints.toList();

		for (int i = 0; i < 4; i++)
		{
			//draw the cube
			Point p1 = pts.get(i);
			Point p2 = pts.get((i + 1) % 4);
			Imgproc.line(in, p1, p2, color, 2);
			//Imgproc.line(in, pts.get(i), pts.get((i+1)%4), color, 2);
			Imgproc.line(in, pts.get(i + 4), pts.get(4 + (i + 1) % 4), color, 2);
			Imgproc.line(in, pts.get(i), pts.get(i + 4), color, 2);

			//outline the marker
			Imgproc.line(in, points.get(i), points.get((i + 1) % 4), color, lineWidth);
		}
*/
		if (writeId)
		{
			String cad = "id=" + Integer.toString(id);
			//Imgproc.putText(in, cad, center, Core.FONT_HERSHEY_SIMPLEX, 0.5, color, 2);
			Imgproc.putText(in, cad, center, Core.FONT_HERSHEY_SIMPLEX, 1.0, color, 2);
		}

//		draw3dAxis(in, cp);
	}

	// -----------------------------------------------------------------------------------------------------------------

}