package com.abcorp.abid.imagedetection;

import android.content.Context;
import android.graphics.Bitmap;

import com.abcorp.abid.idimageupload.MainActivity;
import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.face.Face;
//import com.google.android.gms.vision.face.FaceDetector;

import android.util.Log;
import android.util.SparseArray;

/**
 * Created by pott on 1/3/17.
 * Class that uses the Google Vision library in Play Services 8.1 to faces to detect faces in images.
 */

public class FaceDetect
{
	private SparseArray<Face> mFaces;	// array of faces found in image
	private Context mContext;

	// -----------------------------------------------------------------------------------------------------------------

	public FaceDetect(Context ctx)
	{
		mContext = ctx;
	}

	// -----------------------------------------------------------------------------------------------------------------

	public void findFaces(Bitmap bitmap)
	{
		Log.d(MainActivity.TAG, "** 6.1. Creating FaceDetector");
		ExtFaceDetector detector = new ExtFaceDetector.Builder(mContext)
				  .setTrackingEnabled(false)
				  .setLandmarkType(ExtFaceDetector.FAST_MODE)
				  .setMode(ExtFaceDetector.FAST_MODE)
				  .build();

		if (!detector.isOperational())
		{
			Log.e(MainActivity.TAG, "FaceDetector is not operational");
			// TODO : Handle contingency
		}
		else
		{
			Log.d(MainActivity.TAG, "** 6.2. Creating Frame and setting bitmap");
			Frame frame = new Frame.Builder().setBitmap(bitmap).build();
			Log.d(MainActivity.TAG, "** 6.3. Running detection");
			mFaces = detector.detect(frame);
			Log.d(MainActivity.TAG, "** 6.4. Faces detection complete");
			detector.release();
			Log.d(MainActivity.TAG, "** 6.5. Releasing detector");
		}
	}

	// -----------------------------------------------------------------------------------------------------------------

	public Face getFirstFace()
	{
		Face resultFace = null;
		if ((mFaces != null) && (mFaces.size() > 0))
			resultFace = mFaces.valueAt(0);

		return resultFace;
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * Find the first face in an image. We normally expect only one face in our image, if there are multiple faces we
	 * just pick whichever is the first found rather than give an error. This may need to be changed at a later stage.
	 * This is mainly a test function to allow us to run a face detection from scratch. Use the getFirstFace function
	 * instead.
	 * @param bitmap - source image where we'll search for a face
	 * @param ctx - Context, required for the FaceDetector builder
	 * @return - If found, the first face in the image
	 */
	public static Face findFirstFaceCoordinates(Bitmap bitmap, Context ctx)
	{
		Face resultFace = null;

		ExtFaceDetector detector = new ExtFaceDetector.Builder(ctx)
				  .setTrackingEnabled(false)
				  .setLandmarkType(ExtFaceDetector.FAST_MODE)
				  .setMode(ExtFaceDetector.FAST_MODE)
				  .build();

		if (!detector.isOperational())
		{
			// TODO : Handle contingency
		}
		else
		{
			Frame frame = new Frame.Builder().setBitmap(bitmap).build();
			SparseArray<Face> faces = detector.detect(frame);
			detector.release();
			if (faces.size() > 0)
				resultFace = faces.valueAt(0);
		}

		return resultFace;
	}

	// -----------------------------------------------------------------------------------------------------------------

}
