package com.abcorp.abid.imagedetection;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Size;
import org.opencv.core.TermCriteria;
import org.opencv.imgproc.Imgproc;
import org.opencv.android.Utils;

import android.graphics.Bitmap;
import android.util.Log;

import com.abcorp.abid.idimageupload.MainActivity;

/**
 * Marker Detector
 *
 * Class to detect markers. Based on Aruco MarkerDetector class by Rafael Ortega
 * and http://image2measure.net/files/Mastering_OpenCV.pdf
 *
 * Thresholds image, analyzes contours, and looks for valid marker codes.
 * @author Dan Szafir
 */
// TODO eliminate unnecessary native calls, for example store the frame info
// such as type in member fields and call it only once
public class MarkerDetector
{

	private enum ThresholdMethod {FIXED_THRES,ADPT_THRES,CANNY};

	private float markerSizeMeters;
	private Size canonicalMarkerSize;
	private Mat markerCorners2d;
	private Vector<Integer> mValidMarkers;		// make sure the detected marker IDs match or signature slip

	private final static double MIN_DISTANCE = 10;

	// -----------------------------------------------------------------------------------------------------------------

	public MarkerDetector(Vector<Integer> validMarkers)
	{
		mValidMarkers = validMarkers;
		canonicalMarkerSize = new Size(50,50);
//		canonicalMarkerSize = new Size(100,100);
		markerCorners2d = new Mat(4,1,CvType.CV_32FC2);
		markerCorners2d.put(0,0, 0,0,
				  canonicalMarkerSize.width-1,0,
				  canonicalMarkerSize.width-1,canonicalMarkerSize.height-1,
				  0,canonicalMarkerSize.height-1);

		markerSizeMeters = 5;	// TODO : no idea what this signifies or what acceptable values are, or if it is even used
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * Detect markers in a given Bitmap
	 * @param bitmap source image where we look for markers
	 * @return A list of detected markers
	 */
	public Vector<Marker> processFrame(Bitmap bitmap)
	{
		Bitmap bmp32 = bitmap.copy(Bitmap.Config.ARGB_8888, true);
		int width = bmp32.getWidth();
		int height = bmp32.getHeight();

		// Create a Mat from the source image and convert it to greyscale
		//Mat tmp = new Mat (height, width , CvType.CV_8UC1);
		Mat tmp = new Mat (height, width , CvType.CV_32SC1);
		Utils.bitmapToMat(bmp32, tmp);

		Mat gray = new Mat();
		Imgproc.cvtColor(tmp, gray, Imgproc.COLOR_RGBA2GRAY);
		Vector<Marker> detectedMarkers = findMarkers(gray);
		return detectedMarkers;
	}

	// -----------------------------------------------------------------------------------------------------------------

	public Vector<Marker> processFrame(Mat mat, boolean isGrey)
	{
		if (isGrey)
		{
			return findMarkers(mat);
		}
		else
		{
			Mat gray = new Mat();
			Imgproc.cvtColor(mat, gray, Imgproc.COLOR_RGBA2GRAY);
			return findMarkers(gray);
		}
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * Find the top-left corner of our marker. OpenCV doesn't always give us the four points of the marker in the same
	 * order so we need to find out for ourselves which one the top-right corner is so we can use the marker to read
	 * a correct position on the signature slip.
	 * Since the image is almost certainly tilted in unpredictable ways, we can't just take "lowest left point & lowest
	 * top point". But we can assume that the sum of x and y will always be the lowest for the top-left corner, so
	 * we simply grab that one.
	 * @param mrk
	 * @return
	 */
	public static Point GetTopLeft(Marker mrk)
	{
		Point result = null;
		List<Point> points = mrk.getPoints();
		Map<Double, Integer> ranking = new TreeMap<>();
		if (points.size() == 4)
		{
			for (int i = 0; i < 4; i++)
			{
				Point pt = points.get(i);
				ranking.put((pt.x + pt.y), i);
			}

			Map.Entry<Double, Integer> entry = ranking.entrySet().iterator().next();
			result = points.get(entry.getValue());
		}

		return result;
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * As for the above function, we also need to be able to find the bottom-right corner of a marker.
	 * We can assume that the sum of x and y will always be the highest for the bottom-right corner.
	 * @param mrk
	 * @return
	 */
	public static Point GetBottomRight(Marker mrk)
	{
		Point result = null;
		List<Point> points = mrk.getPoints();
		Map<Double, Integer> ranking = new TreeMap<>(Collections.reverseOrder());
		if (points.size() == 4)
		{
			for (int i = 0; i < 4; i++)
			{
				Point pt = points.get(i);
				Double sum = pt.x + pt.y;
				ranking.put(sum, i);
			}

			Map.Entry<Double, Integer> entry = ranking.entrySet().iterator().next();
			result = points.get(entry.getValue());
		}

		return result;
	}

	// -----------------------------------------------------------------------------------------------------------------

/*
	private static Object getKeyFromValue(Map hm, Object value)
	{
		for (Object o : hm.keySet())
		{
			if (hm.get(o).equals(value))
			{
				return o;
			}
		}
		return null;
	}
*/
	// -----------------------------------------------------------------------------------------------------------------


	/**
	 * Detect markers given a Mat which must be greyscale
	 * @param grayImage - source image where we want to find markers in
	 * @return A List of detected markers
	 */
	private Vector<Marker> findMarkers(Mat grayImage)
	{
		// Make it binary
		Mat thresholdImg = performThreshold(grayImage, ThresholdMethod.FIXED_THRES);

		//Detect Contours
		Vector<MatOfPoint> contours = findContours(thresholdImg, grayImage.cols() / 5);
		/////Log.d(MainActivity.TAG, "Contours: " + contours.size());

		// Find closed contours that can be approximated with 4 points
		Vector<Marker> detectedMarkers = findCandidates(contours);

		/////Log.d(MainActivity.TAG, "Detected Markers: " + detectedMarkers.size());

		// Decode markers and ensure each is detected only once
		//Vector<Marker> visibleMarkers = testMarkers(grayImage, detectedMarkers);
		Vector<Marker> visibleMarkers = recognizeMarkers(grayImage, detectedMarkers);
		//////Log.d(MainActivity.TAG, "Visible Markers: " + visibleMarkers.size());

		return visibleMarkers;
	}

	// -----------------------------------------------------------------------------------------------------------------

	private static Mat performThreshold(Mat src, ThresholdMethod method)
	{
		Mat dst = new Mat();
		switch(method)
		{
			case FIXED_THRES:
				Imgproc.threshold(src, dst, 127, 255, Imgproc.THRESH_BINARY_INV);
				break;
			case ADPT_THRES:
				Imgproc.adaptiveThreshold(src, dst, 255.0, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C, Imgproc.THRESH_BINARY_INV, 7, 7);
				break;
			case CANNY:
				Imgproc.Canny(src, dst, 10, 220);// TODO what are appropriate parameters for canny?
				break;
		}
		return dst;
	}

	// -----------------------------------------------------------------------------------------------------------------

	private Vector<MatOfPoint> findContours(Mat thresholdImg, int minContourPointsAllowed)
	{

		Vector<MatOfPoint> allContours = new Vector<>();
		Mat hierarchy = new Mat();
		Imgproc.findContours(thresholdImg, allContours, hierarchy, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_NONE);

		//remove all contours that are too small to be candidates
		for (int i = 0; i < allContours.size(); ++i)
		{
			if (allContours.get(i).total() < minContourPointsAllowed)
			{
				allContours.remove(i);
				--i;
			}
		}
		return allContours;
	}

	// -----------------------------------------------------------------------------------------------------------------

	private Vector<Marker> findCandidates(Vector<MatOfPoint> contours)
	{

		Vector<Marker> detectedMarkers = new Vector<>();

		MatOfPoint2f approxCurve = new MatOfPoint2f();
		for(int i=0;i<contours.size();i++)
		{

			// Approximate to a polygon
			MatOfPoint2f contour = new MatOfPoint2f();
			contours.get(i).convertTo(contour, CvType.CV_32FC2);
			double eps = contour.total() * .05;
			Imgproc.approxPolyDP(contour, approxCurve, eps, true);

			// We interested only in polygons that contains only four points
			if (approxCurve.total() != 4)
				continue;

			// And they have to be convex
			MatOfPoint mat = new MatOfPoint();
			approxCurve.convertTo(mat, CvType.CV_32SC2);
			if (!Imgproc.isContourConvex(mat))
				continue;

			// Ensure that the distance between consecutive points is large enough
			double minDist = Double.MAX_VALUE;;

			Vector<Point> p = new Vector<>();
			float[] points = new float[8];// [x1 y1 x2 y2 x3 y3 x4 y4]
			approxCurve.get(0,0,points);
			for (int j = 0; j < 8; j += 2)
			{
				double d = Math.sqrt(Math.pow((points[j]-points[(j+2)%8]), 2) +
						  Math.pow((points[j+1]-points[(j+3)%8]),2));
				minDist = Math.min(minDist, d);

				p.add(new Point(points[j], points[j+1]));
			}

			if (minDist < MIN_DISTANCE)
				continue;

			// All tests are passed. Save marker candidate:
//	        Marker m = new Marker(markerSizeMeters, p);
//	        detectedMarkers.add(m);
			detectedMarkers.add(new Marker(markerSizeMeters, p));//approxCurve));
		}

		//Remove elements whose corners are too close
		Marker m1, m2;
		for(int i = 0; i < detectedMarkers.size(); ++i)
		{
			for (int j = i+1; j < detectedMarkers.size(); ++j)
			{
				m1 = detectedMarkers.get(i);
				m2 = detectedMarkers.get(j);

				if (Marker.distance(m1, m2) < 10)
				{
					if(m1.perimeter()<m2.perimeter())
					{
						//m1 is bad
						detectedMarkers.remove(i);
						j = detectedMarkers.size();
						--i;
					}
					else
					{
						//m2 is bad
						detectedMarkers.remove(j);
						--j;
					}
				}
			}
		}

		return detectedMarkers;
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * Test the detected markers to ensure these are actual markers rather than just similar patterns found in the
	 * image
	 * @param grayImage - source image
	 * @param detectedMarkers - Potential markers that were detected and need to be tested
	 * @return - Markers that have been found to be valid
	 */
	private Vector<Marker> recognizeMarkers(Mat grayImage, Vector<Marker> detectedMarkers)
	{
		Vector<Marker> visibleMarkers = new Vector<>();

		for(int i = 0; i < detectedMarkers.size(); ++i)
		{
			Marker marker = detectedMarkers.get(i);

			// Find the perspective transformation that brings current marker to rectangular form
			Mat markerTransform = Imgproc.getPerspectiveTransform(marker.getMat(), markerCorners2d);

			// Transform image to get a canonical marker image
			Mat canonicalMarker = new Mat();
			Imgproc.warpPerspective(grayImage, canonicalMarker,  markerTransform, canonicalMarkerSize);

			marker.setCononicalMat(canonicalMarker);
			marker.extractCode();

			if(marker.checkBorder())
			{
				int id = marker.calculateMarkerId();
				if ((id == -1) || (!mValidMarkers.contains(id)))
				{
					detectedMarkers.remove(i);
					i--;
				}
				else
				{
					int index = visibleMarkers.indexOf(marker);
					if (index == -1)
					{
						// rotate the points of the marker so they are always in the same order no matter the camera orientation
						Collections.rotate(marker.getPoints(), 4 - marker.getRotations());
						visibleMarkers.add(marker);
					}
					else
					{
						if (marker.perimeter() > visibleMarkers.get(index).perimeter())
						{
							visibleMarkers.set(index, marker);
						}
					}
				}
			}
		}

		// Refine marker corners using sub pixel accuracy (not sure how necessary this is)
		if (visibleMarkers.size() > 0)
		{
			List<Point> preciseCorners = new Vector<>();

			for (int i = 0; i < visibleMarkers.size(); ++i)
			{
				Marker marker = visibleMarkers.get(i);
				preciseCorners.addAll(marker.getPoints());
			}

			MatOfPoint2f prec = new MatOfPoint2f();
			prec.fromList(preciseCorners);

			TermCriteria termCriteria = new TermCriteria(TermCriteria.MAX_ITER | TermCriteria.EPS, 30, 0.01);
			Imgproc.cornerSubPix(grayImage, prec, new Size(5,5), new Size(-1,-1), termCriteria);

			preciseCorners = prec.toList();

			//Copy refined corners position back to markers
			for (int i = 0; i < visibleMarkers.size(); ++i)
			{
				Marker marker = visibleMarkers.get(i);
				marker.setPoints(preciseCorners.subList(i*4, i*4+4));
			}
		}

		return visibleMarkers;
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * Test the detected markers to ensure these are actual markers rather than just similar patterns found in the
	 * image
	 * @param grayImage - source image
	 * @param detectedMarkers - Potential markers that were detected and need to be tested
	 * @return - Markers that have been found to be valid
	 */
	private Vector<Marker> testMarkers(Mat grayImage, Vector<Marker> detectedMarkers)
	{
		Vector<Marker> realMarkers = new Vector<>();

		for(int i = 0; i < detectedMarkers.size(); ++i)
		{
			Marker marker = detectedMarkers.get(i);

			// Find the perspective transformation that brings current marker to rectangular form
			Mat markerTransform = Imgproc.getPerspectiveTransform(marker.getMat(), markerCorners2d);

			// Transform image to get a canonical marker image
			Mat canonicalMarker = new Mat();
			Imgproc.warpPerspective(grayImage, canonicalMarker,  markerTransform, canonicalMarkerSize);

			marker.setCononicalMat(canonicalMarker);
			marker.extractCode();

			if(marker.checkBorder())
			{
				int id = marker.calculateMarkerId();
				if ((id != -1) && (mValidMarkers.contains(id)))
				{
					realMarkers.add(marker);
				}
			}
		}

		return realMarkers;
	}

	// -----------------------------------------------------------------------------------------------------------------

}

