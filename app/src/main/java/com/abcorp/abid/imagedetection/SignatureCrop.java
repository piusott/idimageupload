package com.abcorp.abid.imagedetection;

import android.graphics.Rect;

import org.opencv.core.Point;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;

/**
 * Created by pott on 26/5/17.
 * Basic class that allows us to quickly locate a signature and QR code location on a signature slip, based on the
 * ArUco markers found in the image.
 */

public class SignatureCrop
{
	// Very basic class that contains just the parts of the Marker that I need to locate a signature
	public static class SparseMarker
	{
		public int Id;				// Marker Id

		private List<Point> mPoints;		// The four corners of the marker

		public SparseMarker(int id)
		{
			Id = id;
		}

		public List<Point> getPoints() { return mPoints; }
		public void setPoints(List<Point> points) { mPoints = points; }


		/**
		 * Find the top-left corner of our marker. OpenCV doesn't always give us the four points of the marker in the same
		 * order so we need to find out for ourselves which one the top-right corner is so we can use the marker to read
		 * a correct position on the signature slip.
		 * Since the image is almost certainly tilted in unpredictable ways, we can't just take "lowest left point & lowest
		 * top point". But we can assume that the sum of x and y will always be the lowest for the top-left corner, so
		 * we simply grab that one.
		 * @return - the top-left corner of the marker
		 */
		public Point GetTopLeft()
		{
			Point result = null;
			Map<Double, Integer> ranking = new TreeMap<>();
			if (mPoints.size() == 4)
			{
				for (int i = 0; i < 4; i++)
				{
					Point pt = mPoints.get(i);
					ranking.put((pt.x + pt.y), i);
				}

				Map.Entry<Double, Integer> entry = ranking.entrySet().iterator().next();
				result = mPoints.get(entry.getValue());
			}

			return result;
		}

		// --------------------------------------------------------------------------------------------------------------

		/**
		 * As for the above function, we also need to be able to find the bottom-right corner of a marker.
		 * We can assume that the sum of x and y will always be the highest for the bottom-right corner.
		 * @return - The bottom right corner of the marker
		 */
		public Point GetBottomRight()
		{
			Point result = null;
			Map<Double, Integer> ranking = new TreeMap<>(Collections.reverseOrder());
			if (mPoints.size() == 4)
			{
				for (int i = 0; i < 4; i++)
				{
					Point pt = mPoints.get(i);
					Double sum = pt.x + pt.y;
					ranking.put(sum, i);
				}

				Map.Entry<Double, Integer> entry = ranking.entrySet().iterator().next();
				result = mPoints.get(entry.getValue());
			}

			return result;
		}

	}

	// -----------------------------------------------------------------------------------------------------------------

	// The two ArUco marker IDs we use on our signature sheet
	public static final int MARKER_ID_TOPLEFT = 530;
	public static final int MARKER_ID_BOTTOMRIGHT = 33;
	public static final int RECT_ID_SIGNATURE = 0;
	public static final int RECT_ID_QR = 1;

	public static Vector<Rect> getSignatureRect(Vector<SparseMarker> markers)
	{
		Rect rectSignature = null;
		Rect rectQR = null;

		if (markers.size() != 2)
			return null;

		///// TODO : use the marker ID to find out which is the top right and which the bottom left marker
		/////        do we want to be smart here and even allow the signature slip to be held upside down?
		SparseMarker marker1 = markers.elementAt(0);
		List<Point> points1 = marker1.getPoints();

		SparseMarker marker2 = markers.elementAt(1);
		List<Point> points2 = marker2.getPoints();

		// For the time being, we only allow signature slips that are the right side up
		if ((marker1.Id != MARKER_ID_TOPLEFT) || (marker2.Id != MARKER_ID_BOTTOMRIGHT))
			return null;

		if ((points1.size() != 4) || (points2.size() != 4))
			return null;

/*
		for (Point pt : points1)
		{
			Log.d(MainActivity.TAG, "pt1 : " + pt.x + " - " + pt.y);
		}
		for (Point pt : points2)
		{
			Log.d(MainActivity.TAG, "pt2 : " + pt.x + " - " + pt.y);
		}
*/

		// We're only interested in the extremities of the image, so the bottom-right corner of the bottom-right
		// marker and the top-left corner of the top-left marker. We measure the image and find the signature and
		// QR code based on the location of these.
		Point markerTopLeftPointTopLeft;
		Point markerTopLeftPointBottomRight;
		Point markerBottomRightPointBottomRight;
		Point markerBottomRightPointTopLeft;
		// find out which of our markers is in the top-left and bottom-right corner
		if (points1.get(0).x < points2.get(0).x)
		{
			markerTopLeftPointTopLeft = marker1.GetTopLeft();
			markerTopLeftPointBottomRight = marker1.GetBottomRight();
			markerBottomRightPointBottomRight = marker2.GetBottomRight();
			markerBottomRightPointTopLeft = marker2.GetTopLeft();
		}
		else
		{
			markerTopLeftPointTopLeft = marker2.GetTopLeft();
			markerTopLeftPointBottomRight = marker2.GetBottomRight();
			markerBottomRightPointBottomRight = marker1.GetBottomRight();
			markerBottomRightPointTopLeft = marker1.GetTopLeft();
		}

//		Log.d(MainActivity.TAG, "Marker 1 topLeft : " + markerTopLeftPointTopLeft);
//		Log.d(MainActivity.TAG, "Marker 2 bottomRight : " + markerBottomRightPointBottomRight);

		// approximate size and location of signature, based on the position and size of the top-left marker.
		double marker1Width = markerTopLeftPointBottomRight.x - markerTopLeftPointTopLeft.x;
/*		int sigTop = (int)(markerTopLeftPointBottomRight.y + (marker1Width / 5.1));
		int sigLeft = (int)(markerTopLeftPointBottomRight.x - (marker1Width / 12));
		int sigWidth = (int)(marker1Width * 2.7);
		int sigHeight = (int)(marker1Width / 1.55);
*/
		int sigTop = (int)(markerTopLeftPointBottomRight.y + (marker1Width / 4.0));
		int sigLeft = (int)(markerTopLeftPointBottomRight.x - (marker1Width / 10.5));
		int sigWidth = (int)(marker1Width * 2.8);
		int sigHeight = (int)(marker1Width / 1.5);

		if (sigLeft < 0)
			sigLeft = 0;

		if (sigTop < 0)
			sigTop = 0;

		rectSignature = new Rect(sigLeft, sigTop, sigLeft + sigWidth, sigTop + sigHeight);

		// also detect the QR code coordinates from those two markers
		double marker2Height = markerBottomRightPointBottomRight.y - markerBottomRightPointTopLeft.y;
		int qrHeight = (int)(marker2Height * 1.5);	// add some whitespace padding around it
/*		int qrLeft = (int)(markerBottomRightPointTopLeft.x - (marker2Height / 3));
		int qrTop = (int)(markerBottomRightPointTopLeft.y - (1.7 * marker2Height));*/
		int qrLeft = (int)(markerBottomRightPointTopLeft.x - (marker2Height / 7));
		int qrTop = (int)(markerBottomRightPointTopLeft.y - (1.5 * marker2Height));

		// square image, so height and width are the same
		rectQR = new Rect(qrLeft, qrTop, qrLeft + qrHeight, qrTop + qrHeight);

		Vector<Rect> result = new Vector<>();
		result.add(RECT_ID_SIGNATURE, rectSignature);
		result.add(RECT_ID_QR, rectQR);
		return result;
	}

	// -----------------------------------------------------------------------------------------------------------------

}
