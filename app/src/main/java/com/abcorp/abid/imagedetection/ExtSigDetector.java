package com.abcorp.abid.imagedetection;

import android.util.Log;

import com.abcorp.abid.idimageupload.MainActivity;
import com.google.android.gms.vision.Frame;

import org.opencv.core.CvType;
import org.opencv.core.Mat;

import java.nio.ByteBuffer;
import java.util.Vector;

/**
 * Created by pott on 26/10/18.
 * Marker detector class that is used to localise the signature and QR code on the image by the coordinates of
 * the markers
 */

public class ExtSigDetector
{
	private Vector<Marker> mDetectedMarkers;
	private MarkerDetector markerDetector;

	// -----------------------------------------------------------------------------------------------------------------

	public ExtSigDetector()
	{
		// populate the list of valid marker ids that we want to find
		Vector<Integer> validMarkers = new Vector<>();
		validMarkers.add(SignatureCrop.MARKER_ID_BOTTOMRIGHT);	// IDs of our two markers on the signature slip, bottom-right marker
		validMarkers.add(SignatureCrop.MARKER_ID_TOPLEFT);			// top-left marker

		markerDetector = new MarkerDetector(validMarkers);
	}

	// -----------------------------------------------------------------------------------------------------------------

	public Vector<Marker> detectedMarkers()
	{
		return mDetectedMarkers;
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * receive an image frame, try to locate markers within the image and if any are found, store the coordinates in
	 * the mDetectedMarkers Vector
	 * @param frame
	 */
	public void receiveFrame(Frame frame)
	{
		if (markerDetector != null)
		{
			ByteBuffer grayImgData = frame.getGrayscaleImageData();
			Frame.Metadata fr = frame.getMetadata();
			int width = fr.getWidth();
			int height = fr.getHeight();

			byte[] yuvRaw = grayImgData.array();

			try
			{
				Mat yuvMat = new Mat(height + (height / 2), width, CvType.CV_8UC1);
				yuvMat.put(0, 0, yuvRaw);

				mDetectedMarkers = markerDetector.processFrame(yuvMat, true);

				/*
				if ((mDetectedMarkers != null) && (mDetectedMarkers.size() > 0))
				{
					Log.d(MainActivity.TAG, "Found " + mDetectedMarkers.size() + " markers");
				}*/
			}
			catch(Exception ex)
			{
				Log.e(MainActivity.TAG, "e : " + ex.getMessage());
			}
		}

	}

	// -----------------------------------------------------------------------------------------------------------------

}
