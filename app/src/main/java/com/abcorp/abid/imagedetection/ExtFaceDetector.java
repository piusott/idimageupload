package com.abcorp.abid.imagedetection;

import android.content.Context;
import android.util.Log;
import android.util.SparseArray;

import com.abcorp.abid.idimageupload.MainActivity;
import com.google.android.gms.internal.vision.zzk;
import com.google.android.gms.vision.face.internal.client.zza;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.zzc;
import com.google.android.gms.vision.face.Face;

import org.opencv.core.CvType;
import org.opencv.core.Mat;

import java.nio.ByteBuffer;
import java.util.HashSet;
import java.util.Vector;

/**
 * Created by pott on 24/5/17.
 * Re-implemented FaceDetector class from com.google.android.gms.vision.face.
 * The reason for re-implementing this is because the FaceDetector class is final, so won't let me override. However
 * I need additional functionality, mainly to allow me to also use a received frame to detect a marker, not just
 * a face.
 */

public class ExtFaceDetector extends Detector<Face>
{
	public static final int NO_LANDMARKS = 0;
	public static final int ALL_LANDMARKS = 1;
	public static final int NO_CLASSIFICATIONS = 0;
	public static final int ALL_CLASSIFICATIONS = 1;
	public static final int FAST_MODE = 0;
	public static final int ACCURATE_MODE = 1;
	private final zzc zzbv;
	private final zza zzbn;
	private final Object mLock;
	private boolean zzbMV;
	private Vector<Marker> mDetectedMarkers;
	private MarkerDetector markerDetector;

	public void release()
	{
		super.release();
		synchronized (this.mLock)
		{
			if (this.zzbMV)
			{
				this.zzbn.zzg();
				this.zzbMV = false;
			}
		}
	}

	// -----------------------------------------------------------------------------------------------------------------

	protected void finalize() throws Throwable
	{
		try
		{
			synchronized (this.mLock)
			{
				if (this.zzbMV)
				{
					Log.w("FaceDetector", "FaceDetector was not released with FaceDetector.release()");
					this.release();
				}
			}
		}
		finally
		{
			super.finalize();
		}

	}

	// -----------------------------------------------------------------------------------------------------------------

	public void setMarkerProcessor()
	{
		Vector<Integer> validMarkers = new Vector<>();
		validMarkers.add(SignatureCrop.MARKER_ID_BOTTOMRIGHT);	// IDs of our two markers on the signature slip, bottom-right marker
		validMarkers.add(SignatureCrop.MARKER_ID_TOPLEFT);			// top-left marker

		markerDetector = new MarkerDetector(validMarkers);
	}

	// -----------------------------------------------------------------------------------------------------------------

	public Vector<Marker> detectedMarkers()
	{
		return mDetectedMarkers;
	}

	// -----------------------------------------------------------------------------------------------------------------

	private void detectMarkers(Frame frame, ByteBuffer grayImgData)
	{
		if (markerDetector != null)
		{
			Frame.Metadata fr = frame.getMetadata();
			int width = fr.getWidth();
			int height = fr.getHeight();

			byte[] yuvRaw = grayImgData.array();

			try
			{
				Mat yuvMat = new Mat(height + (height / 2), width, CvType.CV_8UC1);
				yuvMat.put(0, 0, yuvRaw);

				mDetectedMarkers = markerDetector.processFrame(yuvMat, true);
				/*
				if ((mDetectedMarkers != null) && (mDetectedMarkers.size() > 0))
				{
					Log.d(MainActivity.TAG, "Found " + mDetectedMarkers.size() + "markers");
				}*/
			}
			catch(Exception ex)
			{
				Log.e(MainActivity.TAG, "e : " + ex.getMessage());
			}
		}
	}

	// -----------------------------------------------------------------------------------------------------------------

	public SparseArray<Face> detect(Frame frame)
	{
		if (frame == null)
		{
			throw new IllegalArgumentException("No frame supplied.");
		}
		else
		{
			ByteBuffer grayImgData = frame.getGrayscaleImageData();

			detectMarkers(frame, grayImgData);		// if needed, also check for signature slip markers


			Face[] faces;
			synchronized (this.mLock)
			{
				if (!this.zzbMV)
				{
					throw new RuntimeException("Cannot use detector after release()");
				}

				grayImgData.rewind();
				faces = this.zzbn.zzb(grayImgData, zzk.zzc(frame));
			}

			int nextFaceId = 0;
			HashSet var5 = new HashSet();
			SparseArray detectedFaces = new SparseArray(faces.length);

			for (Face face : faces)
			{
				int faceId = face.getId();
				nextFaceId = Math.max(nextFaceId, faceId);
				if (var5.contains(faceId))
				{
					++nextFaceId;
					faceId = nextFaceId;
				}

				var5.add(Integer.valueOf(faceId));
				int var12 = this.zzbv.zzb(faceId);
				detectedFaces.append(var12, face);
			}

			return detectedFaces;
		}
	}

	// -----------------------------------------------------------------------------------------------------------------

	public boolean setFocus(int var1)
	{
		int var2 = this.zzbv.zzc(var1);
		synchronized (this.mLock)
		{
			if (!this.zzbMV)
			{
				throw new RuntimeException("Cannot use detector after release()");
			}
			else
			{
				return this.zzbn.zzd(var2);
			}
		}
	}

	// -----------------------------------------------------------------------------------------------------------------

	public boolean isOperational()
	{
		return this.zzbn.isOperational();
	}

	private ExtFaceDetector()
	{
		this.zzbv = new zzc();
		this.mLock = new Object();
		this.zzbMV = true;
		throw new IllegalStateException("Default constructor called");
	}

	private ExtFaceDetector(zza var1)
	{
		this.zzbv = new zzc();
		this.mLock = new Object();
		this.zzbMV = true;
		this.zzbn = var1;
	}

	// -----------------------------------------------------------------------------------------------------------------

	public static class Builder
	{
		private final Context mContext;
		private int mLandmarkType = 0;
		private boolean mProminentFaceOnly = false;
		private int mClassificationType = 0;
		private boolean mTrackingEnabled = true;
		private int mode = 0;
		private float mMinFaceSize = -1.0F;

		public Builder(Context var1)
		{
			this.mContext = var1;
		}

		public ExtFaceDetector.Builder setLandmarkType(int var1)
		{
			if (var1 != 0 && var1 != 1)
			{
				throw new IllegalArgumentException((new StringBuilder(34)).append("Invalid landmark type: ").append(var1).toString());
			}
			else
			{
				this.mLandmarkType = var1;
				return this;
			}
		}

		public ExtFaceDetector.Builder setProminentFaceOnly(boolean var1)
		{
			this.mProminentFaceOnly = var1;
			return this;
		}

		public ExtFaceDetector.Builder setClassificationType(int var1)
		{
			if (var1 != 0 && var1 != 1)
			{
				throw new IllegalArgumentException((new StringBuilder(40)).append("Invalid classification type: ").append(var1).toString());
			}
			else
			{
				this.mClassificationType = var1;
				return this;
			}
		}

		public ExtFaceDetector.Builder setTrackingEnabled(boolean var1)
		{
			this.mTrackingEnabled = var1;
			return this;
		}

		public ExtFaceDetector.Builder setMode(int var1)
		{
			switch(var1)
			{
				case 0:
				case 1:
					this.mode = var1;
					return this;
				default:
					throw new IllegalArgumentException((new StringBuilder(25)).append("Invalid mode: ").append(var1).toString());
			}
		}

		public ExtFaceDetector.Builder setMinFaceSize(float var1)
		{
			if (var1 >= 0.0F && var1 <= 1.0F)
			{
				this.mMinFaceSize = var1;
				return this;
			}
			else
			{
				throw new IllegalArgumentException((new StringBuilder(47)).append("Invalid proportional face size: ").append(var1).toString());
			}
		}

		public ExtFaceDetector build()
		{
			com.google.android.gms.vision.face.internal.client.zzc var1 = new com.google.android.gms.vision.face.internal.client.zzc();
			var1.mode = this.mode;
			var1.zzcd = this.mLandmarkType;
			var1.zzce = this.mClassificationType;
			var1.zzcf = this.mProminentFaceOnly;
			var1.zzcg = this.mTrackingEnabled;
			var1.zzch = this.mMinFaceSize;

			zza var2 = new zza(this.mContext, var1);
			return new ExtFaceDetector(var2);
		}
	}
}
