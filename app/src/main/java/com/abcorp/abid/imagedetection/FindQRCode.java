package com.abcorp.abid.imagedetection;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.FileInputStream;
import java.io.IOException;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.RGBLuminanceSource;
import com.google.zxing.Result;
import com.google.zxing.common.HybridBinarizer;

/**
 * Created by pott on 28/2/17.
 */

public class FindQRCode
{

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 *
	 * @param filePath
	 *
	 * @return Qr Code value
	 *
	 * @throws IOException
	 * @throws NotFoundException
	 */
	public static QRData readQRCode(String filePath) throws IOException, NotFoundException
	{
		Bitmap bMap = BitmapFactory.decodeStream(new FileInputStream(filePath));

		return decipherQRCode(bMap);
	}

	// -----------------------------------------------------------------------------------------------------------------

	public static QRData readQRCode(Bitmap sourceImage) throws NotFoundException
	{
		return decipherQRCode(sourceImage);
	}

	// -----------------------------------------------------------------------------------------------------------------

	private static QRData decipherQRCode(Bitmap sourceImage) throws NotFoundException
	{
		if (sourceImage == null)
			return new QRData("");

		int[] intArray = new int[sourceImage.getWidth() * sourceImage.getHeight()];
		//copy pixel data from the Bitmap into the 'intArray' array
		sourceImage.getPixels(intArray, 0, sourceImage.getWidth(), 0, 0, sourceImage.getWidth(), sourceImage.getHeight());

		LuminanceSource source = new RGBLuminanceSource(sourceImage.getWidth(), sourceImage.getHeight(), intArray);
		BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));

		MultiFormatReader mfr = new MultiFormatReader();
		Result qrResult = mfr.decode(bitmap);
		//Result qrCodeResult = new MultiFormatReader().decode(bitmap);
		return new QRData(qrResult.getText());
	}

	// -----------------------------------------------------------------------------------------------------------------

	public static class QRData
	{
		public final String CardType;
		public final String TemplateCode;
		public final String HolderId;
		public final String HolderName;
		public final String CheckDigit;

		public QRData(String cardType, String templateCode, String holderId, String holderName, String checkDigit)
		{
			CardType = cardType;
			TemplateCode = templateCode;
			HolderId = holderId;
			HolderName = holderName;
			CheckDigit = checkDigit;
		}

		public QRData(String sourceData)
		{
			final String fieldDelimiter = ",";
			String[] tokens = sourceData.split(fieldDelimiter, -1);
			if (tokens.length >= 5)
			{
				CardType = tokens[0];
				TemplateCode = tokens[1];
				HolderId = tokens[2];
				CheckDigit = tokens[3];
				HolderName = tokens[4];
			}
			else
			{
				CardType = "";
				TemplateCode = "";
				HolderId = "";
				CheckDigit = "";
				HolderName = "";
			}
		}

		// --------------------------------------------------------------------------------------------------------------

	}

	// -----------------------------------------------------------------------------------------------------------------

	public QRData splitQRCode(String sourceData)
	{
		return new QRData(sourceData);
	}

	// -----------------------------------------------------------------------------------------------------------------

}
