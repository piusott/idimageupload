package com.abcorp.abid.servicecomms;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.abcorp.abid.idimageupload.MainActivity;
import com.abcorp.abid.idimageupload.R;
import com.abcorp.abid.idimageupload.RunTimeProperties;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RestClient
{
	private String mServerURL;
	private String m_Token;
	private String mDeviceId;
	private String m_AppVersion;
	private String mIMEI;

	private final String SVC_TOKEN = "tokenmob";

	private ServerResponseHandler mHandler;
	private Context mCtx;

	// -----------------------------------------------------------------------------------------------------------------

	public RestClient(ServerResponseHandler handler, Context ctx)
	{
		mHandler = handler;
		mCtx = ctx;

		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctx);
		//prefs.getBoolean("keystring", true);
		mDeviceId = prefs.getString(mCtx.getString(R.string.key_device_id),"WA400");
		mServerURL = prefs.getString(mCtx.getString(R.string.key_url_abid_id),"http://abidtest.abcorp.com");
		if (mServerURL.endsWith("/") == false) mServerURL = mServerURL + "/";

		m_AppVersion = "0.0.1.2";   // TODO : get from a global appversion

		if (ActivityCompat.checkSelfPermission(mCtx, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED)
		{
			TelephonyManager telephonyManager = (TelephonyManager) mCtx.getSystemService(Context.TELEPHONY_SERVICE);
			mIMEI = telephonyManager.getDeviceId();
		}
		else
			mIMEI = "";		// will cause any authentication requests to fail at the server end

		Log.d(MainActivity.TAG, "Using REST services at: " + mServerURL + " from device: " + mDeviceId);
	}

	// -----------------------------------------------------------------------------------------------------------------
/*
	public void tokenResponseReceived(ServerResponseHandler handler)
	{
		Log.d(MainActivity.TAG, "tokenResponseReceived in RestClient called");
		handler.tokenResponseReceived("tokenResponseReceived in RestClient called");
	}*/

	// -----------------------------------------------------------------------------------------------------------------

	public void ConnectionTest()
	{
		RequestQueue queue = Volley.newRequestQueue(mCtx);		// instantiate the RequestQueue

		// Request a string response from the provided URL.
		StringRequest req = new StringRequest(Request.Method.POST, mServerURL + SVC_TOKEN, new Response.Listener<String>()
		{

			@Override
			public void onResponse(String response)
			{
				Log.d(MainActivity.TAG, "Received response: " + response);

				try
				{
					JSONObject jsonBody = new JSONObject(response);
					m_Token = jsonBody.getString("access_token");
					int expiryMinutes = jsonBody.getInt("expires_in");
					mHandler.tokenResponseReceived(m_Token, expiryMinutes);
				}
				catch(JSONException e)
				{
					e.printStackTrace();
					mHandler.authError(e.getMessage());
				}
			}
		}, new Response.ErrorListener()
		{
			@Override
			public void onErrorResponse(VolleyError error)
			{
				Log.d(MainActivity.TAG, "Request error: " + error.getMessage());
				Log.d(MainActivity.TAG, " - " + error.getCause());
				String errorMsg;
				if (error.networkResponse != null)
					errorMsg = new String(error.networkResponse.data);
				else
					errorMsg = "Response Error: " + error.getClass().getName();

				Log.d(MainActivity.TAG, "networkResponse: " + errorMsg);
				mHandler.authError(errorMsg);
			}
		})
		{
			@Override
			protected Map<String, String> getParams()
			{
				RunTimeProperties prop = RunTimeProperties.getInstance();

				Map<String, String> params = new HashMap<>();
				params.put("deviceid", mDeviceId);
				params.put("imei", mIMEI);
				params.put("latitude", Double.toString(prop.LoginLocation.latitude));
				params.put("longitude", Double.toString(prop.LoginLocation.longitude));
				params.put("appversion", m_AppVersion);
				params.put("userlogin", prop.TextProperties.get(RunTimeProperties.KEY_USERLOGIN));
				params.put("officeid", prop.TextProperties.get(RunTimeProperties.KEY_LOGINOFFICEID));
				params.put("grant_type", "password");

				return params;
			}

			@Override
			public Map<String, String> getHeaders()
			{
				Log.d(MainActivity.TAG, "getHeaders called");

				Map<String, String> params = new HashMap<>();
				params.put("Content-Type", "application/x-www-form-urlencoded");
				return params;
			}

			@Override
			public String getBodyContentType()
			{
				Log.d(MainActivity.TAG, "getBodyContentType called");
				return "application/x-www-form-urlencoded";
			}
		};

		queue.add(req);	// Add the request to the RequestQueue
	}

	// -----------------------------------------------------------------------------------------------------------------

}
