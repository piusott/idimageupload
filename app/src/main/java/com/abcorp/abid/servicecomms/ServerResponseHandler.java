package com.abcorp.abid.servicecomms;

public interface ServerResponseHandler
{

	/**
	 * Server response handler on authentication request
	 * @param token Authentication token received from server
	 * @param expiresMinutes Minutes before this token expires
	 */
	void tokenResponseReceived(String token, int expiresMinutes);


	/**
	 * Authentication error. Response handler if the authentication was refused by the server, or the server could not
	 * be reached.
	 * @param errorMessage Descriptive error message
	 */
	void authError(String errorMessage);

	// TODO : should we just have a separate ErrorReceived handler function rather than passing an error message in all of these?
	// imagesReceived(String holderId, String shortnameId, Image portrait, Image signature, DateTime, portraitExpiry, DateTime sigExpiry, String error Message);
	// createExisting(String holderId, String shortnameId, String error Message);
	// createNew(String holderId, String shortnameId, String error Message);
	// commandResponse(String command, String error Message);
	// ... etc
}

