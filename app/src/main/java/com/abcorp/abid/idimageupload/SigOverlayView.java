package com.abcorp.abid.idimageupload;

import android.content.Context;
import android.graphics.Bitmap;

import com.abcorp.abid.common.SignatureTransform;

import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;

/**
 * Basic class to display and transform a colour signature image to a 1-bit printable smoothed signature.
 */

public class SigOverlayView extends View
{
	private Bitmap mSignature;			// signature slip image
	private SignatureTransform mSigTransform;	// Signature converter that creates 1-bit smoothed signature
	private ImageView mIVSignature;	// Image where we'll draw the signature to
	private SeekBar mThreshold;		// Threshold slider
	private Boolean mInDrawSig = false;
	private View mParent;
	private RadioGroup mThresholdGroup;

	private int mThresholdValue;		// threshold by which we decide if a spot on the sig image is black or clear
	private int mThickenValue;			// line thickness (0, 1, or 2)

	private int THRESHOLD_OFFSET = 80;	// slider offset to limit our threshold range to 80 - 180

	// -----------------------------------------------------------------------------------------------------------------

	public SigOverlayView(Context context)
	{
		this(context, null, 0);
	}

	public SigOverlayView(Context context, View parentView)
	{
		this(context, null, 0);
		//mThresholdValue = 0;		// default to zero to run autodetect
		mThresholdValue = 50 + THRESHOLD_OFFSET;		// auto-detect is not working well, so default to middle
		mThickenValue = 1; 	// default to medium thickness
		mParent = parentView;

		mThreshold = mParent.findViewById(R.id.seekThreshold);
		mIVSignature = mParent.findViewById(R.id.ivSignature);
		mThresholdGroup = mParent.findViewById(R.id.rbThreshold);
		mThresholdGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
		{
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId)
		 	{
				//mThickenValue
				int selectedId = mThresholdGroup.getCheckedRadioButtonId();
				RadioButton rb = mParent.findViewById(selectedId);
				if (rb != null)
				{
					Object o = rb.getTag();
					if (o != null)
					{
						mThickenValue = Integer.valueOf((String)o);      // Tag holds the thicken value
						invalidate();
					}
				}
		 	}
		});

		mThreshold.setProgress(mThresholdValue - THRESHOLD_OFFSET);	// set to default value to start
		mThreshold.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
		{

			int progress = mThresholdValue;

			@Override
			public void onProgressChanged(SeekBar seekBar, int progressValue, boolean fromUser)
			{
				// We want a threshold range that runs from 0 to 255
				//progress = (int)(progressValue * 2.55);
				progress = progressValue;
				setThreshold(progress);
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar)
			{
				// nothing to do
			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar)
			{
				// nothing to do
			}
		});
	}

	public SigOverlayView(Context context, AttributeSet attrs, int defStyleAttr)
	{
		super(context, attrs, defStyleAttr);
	}

	// -----------------------------------------------------------------------------------------------------------------

	public Bitmap getConvertedSignature()
	{
		return ((BitmapDrawable)mIVSignature.getDrawable()).getBitmap();
	}

	// -----------------------------------------------------------------------------------------------------------------

	@Override
	protected void onDraw(Canvas canvas)
	{
		try
		{
			// partial screen redraw does not work with hardware acceleration so we need to repaint the entire screen
			drawSignature();
		}
		catch(Exception e)
		{
			Log.e(MainActivity.TAG, "Exception on drawing sigoverlay view : " + e.getMessage());
			e.printStackTrace();
		}
		super.onDraw(canvas);

	}

	// -----------------------------------------------------------------------------------------------------------------

	public void setThreshold(int newThreshold)
	{
		// Try a different approach, limit our range somewhat to 80 - 180. I can't see much of a need to ever have
		// to go outside that threshold range.
		mThresholdValue = newThreshold + THRESHOLD_OFFSET;

		if (mInDrawSig == false)		// do not run updates while we're re-positioning the slider dynamically
			invalidate();
	}

	// -----------------------------------------------------------------------------------------------------------------

	private void drawSignature()
	{
		if (mSignature == null)
			return;

		try
		{
			//MainActivity.saveBitmap(mSignature, MainActivity.getOutputMediaFile("1-sig_orig"));
			//SignatureTransform sigTransform = new SignatureTransform(mSignature);

			//sigTransform.Threshold(25);
			mThresholdValue = mSigTransform.Threshold(mThresholdValue);
			//int sliderValue = mThresholdValue * 100 / 255;

			//mInDrawSig = true;
			//mThreshold.setProgress(sliderValue);
			//mInDrawSig = false;

			//Bitmap thresholdSig = mSigTransform.getImage();
			//if (thresholdSig != null)
			//	MainActivity.saveBitmap(thresholdSig, MainActivity.getOutputMediaFile("3-sig_thresh"));
/*
			sigTransform.KillSpots();
			Bitmap spotSig = sigTransform.getImage();
			if (spotSig != null)
				MainActivity.saveBitmap(spotSig, MainActivity.getOutputMediaFile("4-sig_spots"));
*/
			mSigTransform.Thicken(mThickenValue);
			Bitmap resultSignature = mSigTransform.getImage();
			if (resultSignature != null)
			{
				//MainActivity.saveBitmap(resultSignature, MainActivity.getOutputMediaFile("5-sig_final"));
				mIVSignature.setImageBitmap(resultSignature);
			}
			else
			{
				// use "not found" placeholder image
				mIVSignature.setImageResource(R.drawable.sig_not_found);
				mThreshold.setEnabled(false);
				for (int i = 0; i < mThresholdGroup.getChildCount(); i++)
				{
					mThresholdGroup.getChildAt(i).setEnabled(false);
				}
			}
		}
		catch (Exception ex)
		{
			Log.e(MainActivity.TAG, "Exception creating signature : " + ex.getMessage());
		}
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * Assign a signature image and force the view to transform it to 1-bit smoothed image and display it.
	 * @param signature	- signature image to be transformed and displayed
	 */
	public void setData(Bitmap signature)
	{
		mSignature = signature;

		if (mSignature == null)
		{
			// no signature found. Load placeholder image from resources and disable slider and radio buttons
			mIVSignature.setImageResource(R.drawable.sig_not_found);
			mThreshold.setEnabled(false);
			for (int i = 0; i < mThresholdGroup.getChildCount(); i++)
			{
				mThresholdGroup.getChildAt(i).setEnabled(false);
			}

		}
		else
		{
			mThreshold.setEnabled(true);
			for (int i = 0; i < mThresholdGroup.getChildCount(); i++)
			{
				mThresholdGroup.getChildAt(i).setEnabled(true);
			}

			mSigTransform = new SignatureTransform(mSignature);
			// Note : if you set mThresholdValue to 0, it forces an auto-detection of a suitable threshold level. I'm not
			// doing this at the moment because the auto-detection doesn't work that well yet.
			mThresholdValue = 50 + THRESHOLD_OFFSET;   // default to middle

			// If the previous capture didn't find a signature, or the threshold was left at the default, the setProgress
			// function does not execute since there is nothing to set. But we rely on it to redraw the screen, so if
			// the threshold progress is unchanged, force a screen redraw.
			if (mThreshold.getProgress() == 50)
				invalidate();
			else
				mThreshold.setProgress(mThresholdValue - THRESHOLD_OFFSET);   // set to default value to start
			//invalidate();	// not needed anymore. mThreshold.setProgress will already call invalidate()
		}
	}

	// -----------------------------------------------------------------------------------------------------------------

}


