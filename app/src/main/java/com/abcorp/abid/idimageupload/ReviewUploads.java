package com.abcorp.abid.idimageupload;

import android.app.ListFragment;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;


/**
 * Fragment that shows the log of uploads made from this phone. Since this is an offline demo without real uploads
 * we just emulate the behaviour at this stage.
 */
public class ReviewUploads extends ListFragment
{

	private static ArrayList<UploadLogEntry> mReviewData; // = new ArrayList<UploadLogEntry>();

	private Spinner mCardTypeSelect;


	private class UploadLogEntry
	{
		private String LicenceNo;		// MDL number
		private String OfficeId;			// DoT six character office ID
		private String CheckDigit;		// MDL Check Digit
		private String CardType;			// LICS card type (WAD, WAS, WAL, WPC)
		private String Template;			// Card template code
		private String UserId;			// TRELIS id of the upload user
		private String UploadedDate;	// Date the upload was made
		private String UploadStatus;	// Status of the upload

		/*
		Not needed for the time being. I just used them to quickly create a few manual test records...

		public UploadLogEntry() {};	// default constructor
		public UploadLogEntry(String licenceNo, String officeId, String checkDigit, String cardType, String template,
									 String userId, String uploadedDate, String uploadStatus)
		{
			LicenceNo = licenceNo;
			OfficeId = officeId;
			CheckDigit = checkDigit;
			CardType = cardType;
			Template = template;
			UserId = userId;
			UploadedDate = uploadedDate;
			UploadStatus = uploadStatus;
		}
		*/
	}


	public ReviewUploads()
	{
		// Required empty public constructor
	}

	// -----------------------------------------------------------------------------------------------------------------

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		// Inflate the layout for this fragment
		View rootView = inflater.inflate(R.layout.fragment_review, container, false);
/*
		// Load our card types into the dropdown box
		mCardTypeSelect = (Spinner)rootView.findViewById(R.id.review_cardtype_select);
		ArrayAdapter<CharSequence> staticAdapter = ArrayAdapter
				  .createFromResource(getActivity(), R.array.card_types_ext, android.R.layout.simple_spinner_item);

		staticAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mCardTypeSelect.setAdapter(staticAdapter);
*/
		LoadUploadLog();

		return rootView;
	}

	// -----------------------------------------------------------------------------------------------------------------

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
		LoadReviewList();
	}

	private void LoadReviewList()
	{
		ArrayAdapter adapter = new ArrayAdapter<UploadLogEntry>(getActivity(), android.R.layout.simple_list_item_2, android.R.id.text1, mReviewData)
		{
			@Override
			public @NonNull View getView(int position, View convertView, @NonNull ViewGroup parent)
			{
				View view = super.getView(position, convertView, parent);
				TextView text1 = (TextView) view.findViewById(android.R.id.text1);
				TextView text2 = (TextView) view.findViewById(android.R.id.text2);

				UploadLogEntry logEntry = mReviewData.get(position);

				text1.setText(logEntry.CardType + " " + logEntry.Template + " " +
						  logEntry.LicenceNo + " (" + logEntry.CheckDigit + ")");

				text2.setText("Uploaded : " + logEntry.OfficeId + " " + logEntry.UploadedDate +
						  "\nUser ID : " + logEntry.UserId + "\n" + logEntry.UploadStatus);
				return view;
			}
		};

		setListAdapter(adapter);
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * While we're in emulator demo mode, just load the log from a JSON file in our assets folder.
	 *
	 */
	private void LoadUploadLog()
	{
		AssetManager asm = getActivity().getAssets();
		try
		{
			Gson gson = new GsonBuilder().create();

			try
			{
				String persoFile = "upload_log.json";
				InputStream is = asm.open(persoFile);
				int size = is.available();
				byte[] buffer = new byte[size];
				is.read(buffer);
				is.close();
				String jsonData = new String(buffer, "UTF-8");

				Type collectionType = new TypeToken<Collection<UploadLogEntry>>(){}.getType();
				mReviewData = gson.fromJson(jsonData, collectionType);
			}
			catch (FileNotFoundException ex)
			{
				Log.e(MainActivity.TAG, "FileNotFoundException on loading upload log : " + ex.getMessage());
			}
		}
		catch (Exception e)
		{
			Log.e(MainActivity.TAG, "Exception on loading upload log : " + e.getMessage());
		}
	}

}
