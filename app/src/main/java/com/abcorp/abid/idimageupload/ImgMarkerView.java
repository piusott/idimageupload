package com.abcorp.abid.idimageupload;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

import com.abcorp.abid.imagedetection.Marker;

import org.opencv.core.Point;

import java.util.List;
import java.util.Vector;

/**
 * Basic class to draw a bitmap and show all detected markers. I'm just using this to evaluate test images
 */

public class ImgMarkerView extends View
{
	private Bitmap mSourceImage;
	private Vector<Marker> mMarkers;	// array of markers found in image
	Paint mPaint = new Paint();

	// -----------------------------------------------------------------------------------------------------------------

	public ImgMarkerView(Context context)
	{
		this(context, null);
	}

	public ImgMarkerView(Context context, AttributeSet attrs)
	{
		this(context, attrs, 0);
	}

	public ImgMarkerView(Context context, AttributeSet attrs, int defStyleAttr)
	{
		super(context, attrs, defStyleAttr);
	}

	// -----------------------------------------------------------------------------------------------------------------

	@Override
	protected void onDraw(Canvas canvas)
	{
		super.onDraw(canvas);

		if (mSourceImage != null)
		{
			double imgWidth = mSourceImage.getWidth();
			double imgHeight = mSourceImage.getHeight();

			double scale = drawBaseImage(canvas, imgWidth, imgHeight);
			if (mMarkers != null)
			{
				drawMarkerBoxes(canvas, scale);

				cropSignature();
				// ToDo : create cropped signature image and paint it below
				//Bitmap signature = Bitmap.createBitmap(mSourceImage, 0, faceBottom, mSourceImage.getWidth(), sourceImgHeight - faceBottom);

			}
		}
	}

	// -----------------------------------------------------------------------------------------------------------------

	private void cropSignature()
	{
		// ToDo : create cropped signature image and paint it below
		if (mMarkers.size() == 2)
		{

			Marker marker1 = mMarkers.elementAt(0);
			List<Point> points1 = marker1.getPoints();

			Marker marker2 = mMarkers.elementAt(1);
			List<Point> points2 = marker2.getPoints();

			if ((points1.size() == 4) && (points2.size() == 4))
			{
				// The four points of the marker start in the top-right corner of each marker and then go clockwise. We're
				// only interested in the extremities of the image, so the bottom-right corner of the bottom-right marker
				// and the top-left corner of the top-left marker.
				Point topLeft;
				Point bottomRight;
				// find out which of our markers is in the top-left and bottom-right corner
				if (points1.get(0).x < points2.get(0).x)
				{
					topLeft = points1.get(3);
					bottomRight = points2.get(1);
				}
				else
				{
					topLeft = points2.get(3);
					bottomRight = points1.get(1);
				}

				int leftImageBorder = (int)topLeft.x;
				int topImageBorder = (int)topLeft.y;
				int imageWidth = (int)(bottomRight.x - topLeft.x);
				int imageHeight = (int)(bottomRight.y - topLeft.y);

				// The signature starts at 16.95% and is 44.06% wide.
				// It also starts at 51.31% from the top and is 19.74% tall. Make a bit of extra space around it though.
				int sigLeft = leftImageBorder + (int)(imageWidth / 100 * (16.95 - 1));
				int sigTop = topImageBorder + (int)(imageHeight / 100 * (51.31 - 5));
				int sigWidth = (int)(imageWidth / 100 * (44.06 + 4));
				int sigHeight = (int)(imageHeight / 100 * (19.74 + 18));

				Bitmap signature = Bitmap.createBitmap(mSourceImage, sigLeft, sigTop, sigWidth, sigHeight);
				MainActivity.saveBitmap(signature, MainActivity.getOutputMediaFile("Test", "-", "croppedSig"), 85);
			}
		}
	}

	// -----------------------------------------------------------------------------------------------------------------

	private double drawBaseImage(Canvas canvas, double imgWidth, double imgHeight)
	{
		// First find out how large we should paint the image by checking how large our total canvas is
		double viewWidth = canvas.getWidth();
		double viewHeight = canvas.getHeight();
		double scale = Math.min(viewWidth / imgWidth, viewHeight / imgHeight);

		Rect destBounds = new Rect(0, 0, (int)(imgWidth * scale), (int)(imgHeight * scale));
		canvas.drawBitmap(mSourceImage, null, destBounds, null);
		return scale;
	}

	// -----------------------------------------------------------------------------------------------------------------

	private void drawMarkerBoxes(Canvas canvas, double scale)
	{
		//mPaint.setColor(Color.BLACK);
		//mPaint.setTypeface(Typeface.create(Typeface.DEFAULT, 0));
		mPaint.setTextSize(24f);
		mPaint.setAntiAlias(true);

		mPaint.setColor(Color.GREEN);
		mPaint.setStyle(Paint.Style.STROKE);

		float startX, startY, stopX, stopY, labelX, labelY;

		for (int i = 0; i < mMarkers.size(); i++)
		{
			Marker marker = mMarkers.elementAt(i);

			List<Point> points = marker.getPoints();
			int listSize = points.size();
			if (listSize >= 3)
			{
				mPaint.setStrokeWidth(5);
				mPaint.setColor(Color.GREEN);
				Point point = points.get(0);
				Point point2 = points.get(1);
				startY = (float)(point.y * scale);
				startX = (float)(point.x * scale);
				stopX = (float)(point2.x * scale);
				stopY = (float)(point2.y * scale);
				canvas.drawLine(startX, startY, stopX, stopY, mPaint);
				labelX = startX + 20;
				labelY = startY;

				Point point3 = points.get(2);
				startX = stopX;
				startY = stopY;
				stopX = (float)(point3.x * scale);
				stopY = (float)(point3.y * scale);
				canvas.drawLine(startX, startY, stopX, stopY, mPaint);

				Point point4 = points.get(3);
				startX = stopX;
				startY = stopY;
				stopX = (float)(point4.x * scale);
				stopY = (float)(point4.y * scale);
				canvas.drawLine(startX, startY, stopX, stopY, mPaint);

				startX = stopX;
				startY = stopY;
				stopX = (float)(point.x * scale);
				stopY = (float)(point.y * scale);
				canvas.drawLine(startX, startY, stopX, stopY, mPaint);

				mPaint.setStrokeWidth(1);
				mPaint.setColor(Color.YELLOW);
				canvas.drawText("x:" + point.x, labelX, labelY + 20, mPaint);
				canvas.drawText("y:" + point.y, labelX, labelY + 50, mPaint);
			}
		}
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * Assign an image and detected metadata and force the view to display these.
	 * @param sourceImg	- image to be displayed. Ideally should contain at least one detected face
	 * @param markers	- face coordinates of all faces. If you don't want the face rectangles to be displayed, pass null
	 */
	public void setData(Bitmap sourceImg, Vector<Marker> markers)
	{
		mSourceImage = sourceImg;
		mMarkers = markers;
		invalidate();
	}

	// -----------------------------------------------------------------------------------------------------------------

}


