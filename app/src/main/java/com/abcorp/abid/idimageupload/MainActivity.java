package com.abcorp.abid.idimageupload;

import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.app.FragmentTransaction;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.SubMenu;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.Toast;

import com.abcorp.abid.common.SigSlipDetect;
import com.google.android.gms.maps.model.LatLng;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener
{

	public static final String TAG = "ImgUpload";

	private View mLayout;

	private CardStatus mCardStatus;
	private ReviewUploads mReviewUploads;
	private CameraFragment mCameraCapture;
	private FaceOverlayFragment mFaceOverlay;
	//private SigOverlayFragment mSigOverlay;
	private SigSlipFragment mSigSlipCapture;
	private PortCaptureFragment mPortCapture;
	//private ImgMarkerView mImgMarkerView;
	private ImageView mBackdrop;
	private Toolbar mToolbar;

	// I'm sure this is really bad practice and wrong, but I can't for the live of me find an easy way to find out
	// if we're currently showing this Activity or one of the various Fragments. I'm sure that's possible but for
	// whatever stupid reason I can't find it. So I'm just using a flag that is active whenever we're doing a fragment.
	// I need this to prevent the back button from going to a previously used fragment, but still allow the back button
	// so we can redo a photo during a capture.
	private boolean mIsRoot = true;

	// -----------------------------------------------------------------------------------------------------------------

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setupActivity(null);

		setCustomMenuFont(); // Use a custom font for our drawer menu, just because it looks much nicer...

		// create fragments here so we can re-direct button click events as needed to them
		mCardStatus = new CardStatus();
		mReviewUploads = new ReviewUploads();
		mCameraCapture = new CameraFragment();
		mFaceOverlay = new FaceOverlayFragment();
		//mSigOverlay = new SigOverlayFragment();
		mSigSlipCapture = new SigSlipFragment();
		mPortCapture = new PortCaptureFragment();
	}

	// -----------------------------------------------------------------------------------------------------------------

	private void setupActivity(String message)
	{
		mIsRoot = true;
		setContentView(R.layout.activity_main);
		mToolbar = findViewById(R.id.toolbar);
		setSupportActionBar(mToolbar);

		mLayout = findViewById(R.id.content_main);
		mBackdrop = findViewById(R.id.ivMainBackdrop);

		mToolbar.setTitle(getResources().getString(R.string.app_name));

		DrawerLayout drawer = findViewById(R.id.drawer_layout);
		ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
				  this, drawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
		drawer.addDrawerListener(toggle);
		toggle.syncState();

		NavigationView navigationView = findViewById(R.id.nav_view);
		navigationView.setNavigationItemSelectedListener(this);

		if ((message != null) && (message.isEmpty() == false))
		{
			//Snackbar.make(mLayout, message, Snackbar.LENGTH_LONG).setAction("Action", null).show();
			Toast.makeText(this, message, Toast.LENGTH_LONG).show();
		}
	}

	// -----------------------------------------------------------------------------------------------------------------

	//region Custom menu font
	private void setCustomMenuFont()
	{
		// Use a custom font for our drawer menu, just because it looks much nicer...
		Typeface menuFont = Typeface.createFromAsset(getAssets(), "fonts/helr45w.ttf");
		NavigationView navView = findViewById(R.id.nav_view);
		Menu m = navView.getMenu();
		for (int i = 0; i < m.size(); i++)
		{
			MenuItem mi = m.getItem(i);
			SubMenu subMenu = mi.getSubMenu();
			if ((subMenu != null) && (subMenu.size() > 0))
			{
				for (int j = 0; j < subMenu.size(); j++)
				{
					MenuItem subMenuItem = subMenu.getItem(j);
					applyFontToMenuItem(subMenuItem, menuFont);	// set the custom font to the subment item
				}
			}

			applyFontToMenuItem(mi, menuFont);	// set the custom font to the menu item
		}
	}

	// -----------------------------------------------------------------------------------------------------------------

	private void applyFontToMenuItem(MenuItem mi, Typeface font)
	{
		SpannableString mNewTitle = new SpannableString(mi.getTitle());
		mNewTitle.setSpan(new CustomTypefaceSpan("", font), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
		mi.setTitle(mNewTitle);
	}
	// endregion

	// -----------------------------------------------------------------------------------------------------------------

	public void onCheckIDStatus(View view)
	{
		mCardStatus.onCheckIDStatus(view);
	}

	// -----------------------------------------------------------------------------------------------------------------

	@Override
	public void onBackPressed()
	{
		Log.d(TAG, "MainActivity - Back Button pressed");
		DrawerLayout drawer = findViewById(R.id.drawer_layout);
		if (drawer.isDrawerOpen(GravityCompat.START))
		{
			Log.d(TAG, "MainActivity - Closing drawer");
			drawer.closeDrawer(GravityCompat.START);
		}
		else
		{
			if (mIsRoot == false)
			{
				Log.d(TAG, "Handling back button");
				super.onBackPressed();
			}
			else
				Log.d(TAG, "Showing MainActivity - ignoring back button");
		}
	}

	// -----------------------------------------------------------------------------------------------------------------

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	// -----------------------------------------------------------------------------------------------------------------

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if (id == R.id.action_logout)
		{
			// Clear login details
			RunTimeProperties prop = RunTimeProperties.getInstance();
			//prop.LoginLocation = new LatLng(0, 0);	// Reset the login location
			prop.LoginLocation = null;		// Reset the login location
			prop.TextProperties.put(RunTimeProperties.KEY_USERLOGIN, "");
			prop.TextProperties.put(RunTimeProperties.KEY_LOGINOFFICEID, "");

			finish();
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * Called when a single shot (portrait, signature and QR all in one photo) capture has been done.
	 * @param capture - captured photo
	 * @param faceRect - rectangle where we expect the face to be
	 * @param sigRect - rectangle where we expect the signature to be
	 * @param qrRect - rectangle where we expect the QR code to be
	 */
	public void photoCaptureDone(Bitmap capture, RectF faceRect, Rect sigRect, Rect qrRect)
	{
		// Photo capture has been done, now decipher the captured image and show the results
		mFaceOverlay.setBitmap(capture, faceRect, sigRect, qrRect);
		FragmentTransaction ft = getFragmentManager().beginTransaction();

		// I had this disabled initially, but now that we have the fake "upload images" functionality, we can re-enable
		// the back button going to a new camera capture again from the preview.
		ft.addToBackStack(null);

		ft.replace(R.id.content_main, mFaceOverlay);
		ft.commit();
		mIsRoot = false;
	}

	// -----------------------------------------------------------------------------------------------------------------

	public void portCaptureDone(Bitmap portrait, Bitmap signature, String applicantName, String holderId, String cardType)
	{
		// Photo capture has been done, now decipher the captured image and show the results
		mFaceOverlay.setData(portrait, signature, applicantName, holderId, cardType);
		mToolbar.setTitle("Review Capture");
		FragmentTransaction ft = getFragmentManager().beginTransaction();

		// I had this disabled initially, but now that we have the fake "upload images" functionality, we can re-enable
		// the back button going to a new camera capture again from the preview.
		ft.addToBackStack(null);

		ft.replace(R.id.content_main, mFaceOverlay);
		ft.commit();
		mIsRoot = false;
	}

	// -----------------------------------------------------------------------------------------------------------------

	public void sigSlipCaptureDone(Bitmap capture)
	{
		mToolbar.setTitle("Capture Portrait");

		SigSlipDetect sigSlip = new SigSlipDetect(capture);
		mPortCapture.setSigSlipData(sigSlip.SignatureImage, sigSlip.ApplicantName, sigSlip.HolderID, sigSlip.CardType);

		FragmentTransaction ft = getFragmentManager().beginTransaction();
		ft.addToBackStack(null);
		ft.replace(R.id.content_main, mPortCapture);
		ft.commit();
		mIsRoot = false;
	}

	// -----------------------------------------------------------------------------------------------------------------

	private boolean isSingleShot()
	{
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		boolean result = prefs.getBoolean(getString(R.string.key_cam_single_shot), false);
		return result;
	}

	// -----------------------------------------------------------------------------------------------------------------

	@Override
	public boolean onNavigationItemSelected(@NonNull MenuItem item)
	{
		Fragment frg;

		// Handle navigation view item clicks here.
		int id = item.getItemId();

		if (id == R.id.nav_capture)
		{
			mBackdrop.setImageBitmap(null);	// clear the backdrop. We don't want this to show on the unused bits of the screen

			if (isSingleShot())
			{
				mToolbar.setTitle("Capture Photo");
				frg = mCameraCapture;		// Handle the camera action to capture signature, demographics and portrait
			}
			else
			{
				mToolbar.setTitle("Capture Signature Slip");
				frg = mSigSlipCapture;		// Handle the camera action to capture signature and demographics first
				//frg = mPortCapture;
			}

			android.app.FragmentManager fm = getFragmentManager();
			//fm.popBackStack("", FragmentManager.POP_BACK_STACK_INCLUSIVE);
			FragmentTransaction ft = fm.beginTransaction();
			ft.addToBackStack(null);
			ft.replace(R.id.content_main, frg);
			ft.commit();
			mIsRoot = false;
		}
		else if (id == R.id.nav_logreview)
		{
			mToolbar.setTitle("Review Uploads");
			frg = mReviewUploads;
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			ft.addToBackStack(null);
			ft.replace(R.id.content_main, frg);
			ft.commit();
			mIsRoot = false;
		}
		else if (id == R.id.nav_idreview)
		{
			mToolbar.setTitle("Card Status");

			frg = mCardStatus;
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			ft.addToBackStack(null);
			ft.replace(R.id.content_main, frg);
			ft.commit();
			mIsRoot = false;
		}
		else if (id == R.id.nav_map)
		{
			mToolbar.setTitle("Nearest Licence Centre");

			// Show the Map Fragment
			frg = new MapsFragment();
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			ft.addToBackStack(null);
			ft.replace(R.id.content_main, frg);
			ft.commit();
			mIsRoot = false;
		}
		else if (id == R.id.nav_servicetest)
		{
			mToolbar.setTitle(R.string.mnu_title_svctest);

			frg = new SvcTestFragment();
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			ft.addToBackStack(null);
			ft.replace(R.id.content_main, frg);
			ft.commit();
			mIsRoot = false;
			/*
			///// Testing, load a previously done camera image and detect data in it
			Log.d(TAG, "** 1. Menu option selected");
			mBackdrop.setImageBitmap(null);	// clear the backdrop. We don't want this to show on the unused bits of the screen

			Bitmap sourceImg = null;
			File dcim = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM + File.separator + "Camera");
			if (dcim != null)
			{
				// pick a random file to interpret
				//String sourceImgPath = dcim.getAbsolutePath() + File.separator + "IMG_20170516_105339.jpg";
				String sourceImgPath;
				File[] pics = dcim.listFiles();
				if (pics != null)
				{
					Random r = new Random();
					int i = 0;
					for (int j=0; j < 5; j++)	// just in case our random file is a directory, try again to get a file
					{
						i = r.nextInt(pics.length);
						if (pics[i].isFile())
							break;
					}

					sourceImgPath = pics[i].getAbsolutePath();
					/*for (File pic : pics)
					{
						Log.d(MainActivity.TAG, "Image File : [" + pic.getAbsolutePath() + "] : ");
					}* /

					Log.d(TAG, "** 2. Located test file : " + sourceImgPath);
					try
					{
						BitmapFactory.Options options = new BitmapFactory.Options();
						sourceImg = BitmapFactory.decodeFile(sourceImgPath, options);
						if (sourceImg != null)
						{
							Log.d(TAG, "** 3. Setting source image");
							mFaceOverlay.setBitmap(sourceImg, null, null, null);

							Log.d(TAG, "** 4. Done. Replacing fragment");

							FragmentTransaction ft = getFragmentManager().beginTransaction();
							ft.replace(R.id.content_main, mFaceOverlay);
							ft.commit();

							Log.d(TAG, "** 5. Done. running tempTestingSetBitmap");
							mFaceOverlay.tempTestingSetBitmap();
						}
					}
					catch(Exception e)
					{
						Log.e(MainActivity.TAG, "Exception on loading image [" + sourceImgPath + "] : " + e.getMessage());
					}
				}
			}*/
			//Toast.makeText(this, "Web Service Test - Not yet implemented", Toast.LENGTH_LONG).show();
			//Snackbar.make(mLayout, "Camera Test - Not yet implemented", Snackbar.LENGTH_LONG).setAction("Action", null).show();
		}
		else if (id == R.id.nav_settings)
		{
			// launch settings activity
			startActivity(new Intent(MainActivity.this, SettingsActivity.class));
		}
		else if (id == R.id.nav_change_pwd)
		{
			Toast.makeText(this, "Change Password - Not yet implemented", Toast.LENGTH_LONG).show();
			//Snackbar.make(mLayout, "Change Password - Not yet implemented", Snackbar.LENGTH_LONG).setAction("Action", null).show();
			/*
			Bitmap sourceImg = null;
			try
			{
				File mediaStorageDir =
						  new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "ABCorpIUS");

				if (! mediaStorageDir.exists())
				{
					if (! mediaStorageDir.mkdirs())
					{
						Log.e(MainActivity.TAG, "Required media storage does not exist");
						return true;
					}
				}

				String sourceImgPath = mediaStorageDir.getPath() + File.separator + "Testsignature.jpg";
				BitmapFactory.Options options = new BitmapFactory.Options();
				sourceImg = BitmapFactory.decodeFile(sourceImgPath, options);
				if (sourceImg != null)
				{
					try
					{
						SignatureTransform sigTransform = new SignatureTransform(sourceImg);

						//sigTransform.Threshold(25);
//			sigTransform.Threshold(0);
						Bitmap thresholdSig = sigTransform.getImage();
						if (thresholdSig != null)
							MainActivity.saveBitmap(thresholdSig, MainActivity.getOutputMediaFile("3-sig_thresh"));
/*
			sigTransform.KillSpots();
			Bitmap spotSig = sigTransform.getImage();
			if (spotSig != null)
				MainActivity.saveBitmap(spotSig, MainActivity.getOutputMediaFile("4-sig_spots"));
* /
						sigTransform.Thicken(2);
						Bitmap resultSignature = sigTransform.getImage();
						if (resultSignature != null)
						{
							MainActivity.saveBitmap(resultSignature, MainActivity.getOutputMediaFile("5-sig_final"));
						}
					}
					catch (Exception ex)
					{
						Log.e(MainActivity.TAG, "Exception creating signature : " + ex.getMessage());
					}

				}
			}
			catch(Exception e)
			{
				Log.e(MainActivity.TAG, "Exception on loading test signature : " + e.getMessage());
			}



			/*
			// Load the reference image from the app's resources.
			// It is loaded in BGR (blue, green, red) format.
			mReferenceImage = Utils.loadResource(context, referenceImageResourceID, Imgcodecs.CV_LOAD_IMAGE_COLOR);

			// Create grayscale and RGBA versions of the reference image.
			final Mat referenceImageGray = new Mat();
			Imgproc.cvtColor(mReferenceImage, referenceImageGray, Imgproc.COLOR_BGR2GRAY);
			Imgproc.cvtColor(mReferenceImage, mReferenceImage, Imgproc.COLOR_BGR2RGBA);

			// Store the reference image's corner coordinates, in pixels.
			mReferenceCorners.put(0, 0, new double[] {0.0, 0.0});
			mReferenceCorners.put(1, 0, new double[] {referenceImageGray.cols(), 0.0});
			mReferenceCorners.put(2, 0, new double[] {referenceImageGray.cols(), referenceImageGray.rows()});
			mReferenceCorners.put(3, 0, new double[] {0.0, referenceImageGray.rows()});

			// Detect the reference features and compute their
			// descriptors.
			mFeatureDetector.detect(referenceImageGray, mReferenceKeypoints);
			mDescriptorExtractor.compute(referenceImageGray, mReferenceKeypoints, mReferenceDescriptors);
			*/
		}
		else if (id == R.id.nav_reset_pwd)
		{
			Toast.makeText(this, "Reset Password - Not yet implemented", Toast.LENGTH_LONG).show();
			//Snackbar.make(mLayout, "Reset Password - Not yet implemented", Snackbar.LENGTH_LONG).setAction("Action", null).show();
		}

		DrawerLayout drawer = findViewById(R.id.drawer_layout);
		drawer.closeDrawer(GravityCompat.START);
		return true;
	}

	// -----------------------------------------------------------------------------------------------------------------

	public void restoreBackdrop()
	{
		Drawable myDrawable = getDrawable(R.drawable.img_backdrop);
		mBackdrop.setImageDrawable(myDrawable);

		setupActivity("Upload Complete");		// rebuild the screen to show the MainActivity startup
	}

	// -----------------------------------------------------------------------------------------------------------------

	public static String getOutputMediaFile(String cardType, String holderId, String suffix)
	{
		return getOutputMediaFile(cardType, holderId, suffix, false);
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * Function to generate the a file name and path that we'll store our images under
	 * @param cardType - The cardtype shortname, e.g. WPC, WAD, etc.
	 * @param holderId - The relevant holder Id of the capture record.
	 * @param suffix - Suffix to attach to the filename (before the extension), e.g. "sig", "port", etc.
	 * @param useDateDirPrefix	- if true, then we create or use a new subdirectory for each day
	 * @return - the generated file name and path
	 */
	public static String getOutputMediaFile(String cardType, String holderId, String suffix, boolean useDateDirPrefix)
	{
		File mediaStorageDir =
				  new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "ABCorpIUS");

		if (! mediaStorageDir.exists())
		{
			if (! mediaStorageDir.mkdirs())
			{
				Log.e(MainActivity.TAG, "Could not create media storage: " + mediaStorageDir.getPath());
				return null;
			}
		}

		String fileNameAndPath;
		if (useDateDirPrefix)
		{
			Date currentDate = new Date();
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
			mediaStorageDir = new File(mediaStorageDir.getPath() + File.separator + dateFormat.format(currentDate));

			if (! mediaStorageDir.exists())
			{
				if (! mediaStorageDir.mkdirs())
				{
					Log.e(MainActivity.TAG, "Could not create daily directory: " + mediaStorageDir.getPath());
					return null;
				}
			}

			dateFormat = new SimpleDateFormat("HHmmss");
			fileNameAndPath = String.format("%s%s%s_%s_%s_%s",
					  mediaStorageDir.getPath(),
					  File.separator,
					  cardType,
					  holderId,
					  dateFormat.format(currentDate),
					  suffix);
		}
		else
		{
			fileNameAndPath = String.format("%s%s%s_%s_%s",
					  mediaStorageDir.getPath(),
					  File.separator,
					  cardType,
					  holderId,
					  suffix);
		}

		return fileNameAndPath;
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * Save a bitmap to the local file system
	 * @param img - The image to save
	 * @param fileName - output filename, minus the extension
	 * @param quality - compression ratio. If 100 we use lossless PNG, anything lower we use JPEG
	 */
	public static void saveBitmap(Bitmap img, String fileName, int quality)
	{
		Bitmap.CompressFormat format = Bitmap.CompressFormat.PNG;
		String extension = ".png";
		if (quality < 100)
		{
			// Since PNG is lossless, the compression factor is ignored. It is however used with JPG
			extension = ".jpg";
			format = Bitmap.CompressFormat.JPEG;
		}

		try
		{
			img.compress(format, quality, new FileOutputStream(fileName + extension));
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
			Log.e(MainActivity.TAG, "FileNotFoundException in saveBitmap : " + e.getMessage());
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Log.e(MainActivity.TAG, "Exception in saveBitmap : " + e.getMessage());
		}
	}

	// -----------------------------------------------------------------------------------------------------------------

	public static void saveTextFile(String textData, String fileName)
	{
		try
		{
			File mediaStorageDir =
					  new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "ABCorpIUS");

			if (! mediaStorageDir.exists())
			{
				if (! mediaStorageDir.mkdirs())
				{
					Log.e(MainActivity.TAG, "Required media storage does not exist");
					return;
				}
			}

			//File file = new File(this.getExternalFilesDir(null), "testfile.txt");
			File file = new File(mediaStorageDir.getPath(), fileName);
			FileOutputStream fileOutput = new FileOutputStream(file);
			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutput);
			outputStreamWriter.write(textData);
			outputStreamWriter.flush();
			fileOutput.getFD().sync();
			outputStreamWriter.close();

			/*
			//OutputStreamWriter os = new OutputStreamWriter(context.openFileOutput("config.txt", Context.MODE_PRIVATE));

			//(ctx.openFileOutput(idFile, Context.MODE_PRIVATE)
			OutputStreamWriter osw = new OutputStreamWriter(context);   //(context.openFileOutput("config.txt", Context.MODE_PRIVATE));
			osw.write(textData);
			osw.close();
			*/
		}
		catch (IOException e)
		{
			Log.e(TAG, "File write failed: " + e.toString());
		}

	}
}
