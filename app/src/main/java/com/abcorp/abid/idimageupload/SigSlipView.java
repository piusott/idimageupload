package com.abcorp.abid.idimageupload;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.abcorp.abid.imagedetection.Marker;
import com.abcorp.abid.imagedetection.SignatureCrop;
import com.google.android.gms.vision.CameraSource;

import org.opencv.core.Point;

import java.util.List;
import java.util.Locale;
import java.util.Vector;

/**
 * Viewer class that lets me overlay the camera with detected objects (markers, signature box, QR code). This is used
 * as feedback to the operator to let them know that we've got a valid signature slip detected.
 * Note that this does basically the same as the functionality in the FaceGraphic class. The reason why this is duplicated
 * is that I'm just building a PoC at the moment and I want to get the customer's input on whether they prefer a single
 * capture or a multiple capture (separate portrait and sigslip capture) approach. In the unlikely event that we'll
 * need both, we can move this functionality into a common class.
 */
public class SigSlipView extends View
{
	private static final float ID_TEXT_SIZE = 40.0f;
	private static final float BOX_STROKE_WIDTH = 5.0f;

	private final Object mLock = new Object();

	private Vector<SignatureCrop.SparseMarker> mAdjustedSigMarkers;	// Markers that have been adjusted for the preview size and rotated
	private volatile Rect mSigRect;
	private volatile Rect mQRRect;

	private Paint mPaintMarker;
	private Paint mPaintSigBox;
	private Paint mPaintQR;

	private int mCanvasWidth;

	private int mPreviewWidth;
	private float mWidthScaleFactor = 1.0f;
	private int mPreviewHeight;
	private float mHeightScaleFactor = 1.0f;

	private int mFacing = CameraSource.CAMERA_FACING_BACK;

	// -----------------------------------------------------------------------------------------------------------------
/*
	public SigSlipView(Context context)
	{
		super(context);
		Log.d(MainActivity.TAG, "Box2 Constructor 1");
		InitPaint();
	}
*/
	// -----------------------------------------------------------------------------------------------------------------

	public SigSlipView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		Log.d(MainActivity.TAG, "Box2 Constructor 2");
		InitPaint();
	}

	// -----------------------------------------------------------------------------------------------------------------
/*
	public SigSlipView(Context context, AttributeSet attrs, int defStyleAttr)
	{
		super(context, attrs, defStyleAttr);
		Log.d(MainActivity.TAG, "Box2 Constructor 3");
		InitPaint();
	}
*/
	// -----------------------------------------------------------------------------------------------------------------

	private void InitPaint()
	{
		mPaintMarker = new Paint();
		mPaintMarker.setStyle(Paint.Style.STROKE);
		mPaintMarker.setColor(Color.CYAN);
		mPaintMarker.setStrokeWidth(BOX_STROKE_WIDTH);
		mPaintMarker.setTextSize(ID_TEXT_SIZE);

		mPaintQR = new Paint();
		mPaintQR.setColor(Color.GREEN);
		mPaintQR.setStyle(Paint.Style.STROKE);
		mPaintQR.setStrokeWidth(BOX_STROKE_WIDTH);

		// make the signature box somewhat transparent so we can see if part of the signature would get cropped off
		mPaintSigBox = new Paint();
		/*mPaintSigBox.setColor(Color.BLACK);
		mPaintSigBox.setAlpha(75);
		mPaintSigBox.setStyle(Paint.Style.STROKE);
		mPaintSigBox.setStrokeWidth(SIGBOX_STROKE_WIDTH);*/
		mPaintSigBox.setColor(Color.YELLOW);
		mPaintSigBox.setStyle(Paint.Style.STROKE);
		mPaintSigBox.setStrokeWidth(BOX_STROKE_WIDTH);
	}

	// -----------------------------------------------------------------------------------------------------------------

	public Rect getSigRectangle()
	{
		return mSigRect;
	}

	// -----------------------------------------------------------------------------------------------------------------

	public Rect getQRRectangle()
	{
		return mQRRect;
	}

	// -----------------------------------------------------------------------------------------------------------------

	public void setMarkers(Vector<Marker> markers)
	{
		synchronized (mLock)
		{
			if (mAdjustedSigMarkers == null)
				mAdjustedSigMarkers = new Vector<>();

			mAdjustedSigMarkers.clear();

			for(Marker m: markers)
			{
				SignatureCrop.SparseMarker rotatedMarker = new SignatureCrop.SparseMarker(m.getId());

				List<Point> points = new Vector<>();

				for (Point p : m.getPoints())
				{
					Point pRotated = rotateAndAdjust(p, mCanvasWidth);
					points.add(pRotated);
				}
				rotatedMarker.setPoints(points);
				mAdjustedSigMarkers.add(rotatedMarker);
			}
		}
		postInvalidate();
	}

	// -----------------------------------------------------------------------------------------------------------------

	private Point rotateAndAdjust(Point p, int canvasWidth)
	{
		// change coordinates according to our preview image size
		float fromX = translateX((float)p.x);
		float fromY = translateY((float)p.y);

		// Markers are detected on landscape image, rotate back to portrait
		return rotate90Degrees(fromX, fromY, canvasWidth);
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * The coordinates are detected on a landscape image while we're showing a portrait image. To get the correct
	 * coordinates we need to rotate the data clockwise by 90 degrees
	 * @return rotated point
	 */
	private Point rotate90Degrees(float x, float y, int canvasWidth)
	{
		Point result = new Point();
		result.y = x;
		result.x = canvasWidth - y;
		return result;
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * Adjusts the x coordinate from the preview's coordinate system to the view coordinate
	 * system.
	 */
	private float translateX(float x)
	{
		if (mFacing == CameraSource.CAMERA_FACING_FRONT)
		{
			return getWidth() - scaleX(x);
		}
		else
		{
			return scaleX(x);
		}
	}

	/**
	 * Adjusts the y coordinate from the preview's coordinate system to the view coordinate
	 * system.
	 */
	private float translateY(float y)
	{
		return scaleY(y);
	}

	/**
	 * Adjusts a horizontal value of the supplied value from the preview scale to the view
	 * scale.
	 */
	private float scaleX(float horizontal)
	{
		return horizontal * mWidthScaleFactor;
	}

	/**
	 * Adjusts a vertical value of the supplied value from the preview scale to the view scale.
	 */
	private float scaleY(float vertical)
	{
		return vertical * mHeightScaleFactor;
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * Sets the camera attributes for size and facing direction, which informs how to transform
	 * image coordinates later.
	 */
	public void setCameraInfo(int previewWidth, int previewHeight, int facing)
	{
		synchronized (mLock)
		{
			mPreviewWidth = previewWidth;
			mPreviewHeight = previewHeight;
			mFacing = facing;
		}
		postInvalidate();
	}

	// -----------------------------------------------------------------------------------------------------------------

	@Override
	protected void onDraw(Canvas canvas)
	{
		super.onDraw(canvas);

		if (mCanvasWidth == 0)
		{
			mCanvasWidth = canvas.getWidth();
			Log.d(MainActivity.TAG, "Canvas Size (w - h): " + mCanvasWidth + " - " + canvas.getHeight());
		}

		synchronized (mLock)
		{
			if ((mPreviewWidth != 0) && (mPreviewHeight != 0))
			{
				mWidthScaleFactor = (float) canvas.getWidth() / (float) mPreviewWidth;
				mHeightScaleFactor = (float) canvas.getHeight() / (float) mPreviewHeight;
			}

			if (mAdjustedSigMarkers != null)
			{
				for (SignatureCrop.SparseMarker m : mAdjustedSigMarkers)
				{
					drawSigMarker(m, canvas);
				}

				if (mAdjustedSigMarkers.size() >= 2)
				{
					Vector<Rect> imgRect = SignatureCrop.getSignatureRect(mAdjustedSigMarkers);
					if ((imgRect != null) && (imgRect.size() == 2))
					{
						mSigRect = imgRect.get(SignatureCrop.RECT_ID_SIGNATURE);
						mQRRect = imgRect.get(SignatureCrop.RECT_ID_QR);

						canvas.drawRect(mSigRect, mPaintSigBox);
						canvas.drawRect(mQRRect, mPaintQR);
					}

				}
			}
		}
	}

	// -----------------------------------------------------------------------------------------------------------------

	private void drawSigMarker(SignatureCrop.SparseMarker marker, Canvas canvas)
	{
		List<Point> pt = marker.getPoints();

		if (pt.size() == 4)
		{
			Point p1 = pt.get(0);
			Point p2 = pt.get(1);
			Point p3 = pt.get(2);
			Point p4 = pt.get(3);

			// We need to draw the markers as lines rather than a rectangle because of the way the camera is
			// likely angled towards the sheet, they're unlikely to be perfectly square.
			canvas.drawLine((float)p1.x, (float)p1.y, (float)p2.x, (float)p2.y, mPaintMarker);
			canvas.drawLine((float)p2.x, (float)p2.y, (float)p3.x, (float)p3.y, mPaintMarker);
			canvas.drawLine((float)p3.x, (float)p3.y, (float)p4.x, (float)p4.y, mPaintMarker);
			canvas.drawLine((float)p4.x, (float)p4.y, (float)p1.x, (float)p1.y, mPaintMarker);

			Point p = marker.GetTopLeft();
			canvas.drawText(String.format(Locale.ENGLISH, "%d", marker.Id), (float)p.x + 5f, (float)p.y + 35f, mPaintMarker);
		}
	}

	// -----------------------------------------------------------------------------------------------------------------

}
