package com.abcorp.abid.idimageupload;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.abcorp.abid.map.MapHelper;
import com.abcorp.abid.map.OfficeLocation;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Collection;

public class MapsFragment extends Fragment
{
	private GoogleMap mMap;
	private MapView mMapView;

	// -----------------------------------------------------------------------------------------------------------------

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View rootView = inflater.inflate(R.layout.fragment_maps, container, false);

		mMapView = (MapView) rootView.findViewById(R.id.mapView);
		mMapView.onCreate(savedInstanceState);

		mMapView.onResume(); // needed to get the map to display immediately

		try
		{
			MapsInitializer.initialize(getActivity().getApplicationContext());
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

		mMapView.getMapAsync(new OnMapReadyCallback()
		{
			@Override
			public void onMapReady(GoogleMap map)
			{
				mMap = map;
				double currentMinDistance = -1;
				LatLng nearestOffice = null;

				Context ctx = getActivity();

				if (!MapHelper.isMapPermissionsGranted(ctx))
					return;		// We are not allowed locations, so we can't continue here

				// try-catch is not really needed here, but because I'm using a static access check in the above
				// isMapPermissionGranted the compiler would complain otherwise.
				try
				{
					mMap.setMyLocationEnabled(true);
					LatLng userLocation = MapHelper.getMyLocation(ctx);

					Collection<OfficeLocation> offices = MapHelper.Offices(ctx);
					for (OfficeLocation ofc : offices)
					{
						BitmapDescriptor iconColour;
						switch(ofc.Type)
						{
							case "DoT Office":
								iconColour = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE);
								break;

							case "Agency":
								iconColour = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED);
								break;

							case "Handheld":
								iconColour = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE);
								break;

							default:
								iconColour = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN);
						}

						LatLng latLng = new LatLng(ofc.Latitude, ofc.Longitude);
						mMap.addMarker(new MarkerOptions()
								  .position(latLng)
								  .title(ofc.Name)
								  .snippet(ofc.OfficeId + ", " + ofc.Address + ", " + ofc.Phone)
								  .icon(iconColour));

						// find out if this site is the closest to our position so far
						double distance = MapHelper.getDistanceToSite(latLng, userLocation);
						if ((currentMinDistance == -1) || (currentMinDistance > distance))
						{
							currentMinDistance = distance;
							nearestOffice = latLng;
						}
					}

	/*
					// For dropping a marker at a point on the Map
					LatLng ABCorpOffice = new LatLng(-38.008104, 145.218504);
					mMap.addMarker(new MarkerOptions().position(ABCorpOffice).title("ABCorp Australasia").snippet("Dandenong South"));
	*/


					// For zooming automatically to the location of the marker
					if (nearestOffice != null)
					{
						CameraPosition cameraPosition = new CameraPosition.Builder().target(nearestOffice).zoom(12).build();
						mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
					}
				}
				catch (SecurityException ex)
				{
					Log.e(MainActivity.TAG, "Exception in onMapReady : " + ex.getMessage());
				}

			}
		});

		return rootView;
	}


	// -----------------------------------------------------------------------------------------------------------------

	@Override
	public void onResume()
	{
		super.onResume();
		mMapView.onResume();
	}

	@Override
	public void onPause()
	{
		super.onPause();
		mMapView.onPause();
	}

	@Override
	public void onDestroy()
	{
		super.onDestroy();
		mMapView.onDestroy();
	}

	@Override
	public void onLowMemory()
	{
		super.onLowMemory();
		mMapView.onLowMemory();
	}

	// -----------------------------------------------------------------------------------------------------------------

}
