package com.abcorp.abid.idimageupload;

import android.Manifest;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.abcorp.abid.map.MapHelper;
import com.abcorp.abid.map.OfficeLocation;
import com.google.android.gms.maps.model.LatLng;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Collection;
import java.util.Locale;

/**
 * Created by pott on 16/3/17.
 * Basic login activity.
 * For our prototype, there is no online credential validation. All we're doing is allow a small number of pre-set
 * user names and passwords.
 * We do however enforce location restrictions. This means that we can only log in to the application if the user is
 * currently within 100 metres of any of the DoT sites, the DoT head quarters, or the ABCorp sites in Perth, Dandenong
 * and South Melbourne. The exception is the ABCorp/pass credentials which work from anywhere.
 */

public class LoginActivity extends AppCompatActivity
{

	private AppCompatEditText mUserName;
	private AppCompatEditText mPassword;
	private View mLayout;

	private static final int REQUEST_MAP = 0;	// Arbitrary Id to identify our map permission request
	private static final int REQUEST_CAM = 1;	// Arbitrary Id to identify our camera permission request
	private static final int REQUEST_PHN = 2;	// Arbitrary Id to identify our phone permission request

	protected PendingIntent singleUpdatePI;
	protected LocationManager locationManager;
	protected Criteria criteria;
	private LatLng mCurrentLocation = null;

	protected static String SINGLE_LOCATION_UPDATE_ACTION = "com.abcorp.abid.SINGLE_LOCATION_UPDATE_ACTION";

	// -----------------------------------------------------------------------------------------------------------------

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		setupVariables();

		if ((ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) ||
				  (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED))
		{
			// Location permission has not been granted.
			RequestMapPermission();
		}
		else
		{
			// Location permissions is already available, we can continue.
			Log.i(MainActivity.TAG, "LOCATION permission has already been granted.");
		}

		// Make sure we have camera access
		if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
		{
			// We need access to the camera to run this app.
			RequestCameraPermission();
		}
		else
		{
			// Camera permissions is already available, we can continue.
			Log.i(MainActivity.TAG, "CAMERA permission has already been granted.");
		}

		// Make sure we have access to the handset details
		if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED)
		{
			// We need access to the handset details to allow us to uniquely identify each handset to the server.
			RequestTelephonyPermission();
		}
		else
		{
			// Handset permissions are already available, we can continue.
			Log.i(MainActivity.TAG, "READ_PHONE_STATE permission has already been granted.");
		}

		Context ctx = getApplicationContext();

		locationManager = (LocationManager)ctx.getSystemService(Context.LOCATION_SERVICE);
		// Coarse accuracy is specified here to get the fastest possible result.
		// The calling Activity will likely (or have already) request ongoing
		// updates using the Fine location provider.
		criteria = new Criteria();
		criteria.setAccuracy(Criteria.ACCURACY_LOW);

		Intent updateIntent = new Intent(SINGLE_LOCATION_UPDATE_ACTION);
		singleUpdatePI = PendingIntent.getBroadcast(ctx, 0, updateIntent, PendingIntent.FLAG_UPDATE_CURRENT);
	}

	// -----------------------------------------------------------------------------------------------------------------

	protected BroadcastReceiver singleUpdateReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			context.unregisterReceiver(singleUpdateReceiver);

			String key = LocationManager.KEY_LOCATION_CHANGED;
			Location location = (Location)intent.getExtras().get(key);

			if (location != null)
				mCurrentLocation = new LatLng(location.getLatitude(), location.getLongitude());

			locationManager.removeUpdates(singleUpdatePI);
		}
	};

	// -----------------------------------------------------------------------------------------------------------------

	@Override
	public void onResume()
	{
		super.onResume();
		/*
		// Disabled the deprecated initDebug static loader for the time being. This requires that we compile the jniLibs
		// into the APK which increases  its size massively. Instead we use initAsync which uses the OpenCV manager that
		// needs to be installed on the phone.
		if (OpenCVLoader.initDebug())
		{
			Log.d(MainActivity.TAG, "OpenCV library found inside package. Using it!");
		}
		else */
		{
			//Log.d(MainActivity.TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization");
			OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);
		}

		mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);

		RequestLocationUpdate();
	}

	// -----------------------------------------------------------------------------------------------------------------

	private void RequestLocationUpdate()
	{
		// force a location update so the next login can use updated coordinates.
		IntentFilter locIntentFilter = new IntentFilter(SINGLE_LOCATION_UPDATE_ACTION);
		getApplicationContext().registerReceiver(singleUpdateReceiver, locIntentFilter);
		try
		{
			locationManager.requestSingleUpdate(criteria, singleUpdatePI);
		}
		catch (SecurityException ex)
		{
			// Should actually never happen, the permissions have been checked on startup. But I'm using this exception
			// handler to silence a compiler warning.
			Log.e(MainActivity.TAG, "Security Exception on RequestLocationUpdate");
		}
	}

	// -----------------------------------------------------------------------------------------------------------------

	private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this)
	{

		@Override
		public void onManagerConnected(int status)
		{
			switch (status)
			{
				case LoaderCallbackInterface.SUCCESS:
				{
					// Nothing to do really...
					Log.i(MainActivity.TAG, "OpenCV loaded successfully");
				}
				break;

				default:
				{
					super.onManagerConnected(status);
				}
				break;
			}
		}
	};

	// -----------------------------------------------------------------------------------------------------------------


	/**
	 * Simple and dumb login authentication. For this prototype application we're just interested in making sure that
	 * we can demonstrate restricting operation to a given office site radius.
	 *
	 * For the demo we just hardcode the following users (password is 'pass' for all accounts) :
	 * - ABCorp, ABnote => generic test account. Works anywhere
	 * - ABCorpPerth => Works in the ABCorp WA (Malaga) office
	 * - ABCorpSthM => Works in the ABCorp South Melbourne office
	 * - DotHQ => Works in either of the DoT headquarter sites (Perth / East Perth / Innaloo)
	 * - DoT => works in a selection of DoT capture station sites
	 * - Post => works at Australia Post Bourke St. Melbourne
	 * @param view - only used to show snackbar messages
	 */
	public void authenticateLogin(View view)
	{
		boolean isValid = false;
		Double maxDistance = 100d;	// test this with 50 metres to start with....
		Double distToSite = 0d;
		String siteName = "";
		OfficeLocation detectedOffice = null;

		// For this demo we're not doing actual user authentication, we just allow the default user to log in
		String user = getResources().getString(R.string.default_login_user);
		String pass = getResources().getString(R.string.default_login_pass);

		Context ctx = getApplicationContext();

		// Check if the user is near the office that they're registered against.
		Collection<OfficeLocation> offices = MapHelper.Offices(ctx);
		if (mCurrentLocation == null)
		{
			//LatLng myLocation = MapHelper.getMyLocation(ctx);
			mCurrentLocation = MapHelper.getMyLocation(ctx);
		}

		String loginUser = mUserName.getText().toString();
		String loginPass = mPassword.getText().toString();

		// for the demo, all users have the same password
		if (loginPass.equals(pass))
		{
			boolean foundSite = false;

			// I'm too lazy to filter users to a particular location for this demo. For the moment I'm just checking
			// to see if the user is within a given radius of any of the sites.
			for (OfficeLocation ofc : offices)
			{
				LatLng officeSite = new LatLng(ofc.Latitude, ofc.Longitude);
				distToSite = MapHelper.getDistanceToSite(officeSite, mCurrentLocation);
				if (distToSite <= maxDistance)
				{
					detectedOffice = ofc;
					foundSite = true;
					siteName = ofc.Name;
					Log.d(MainActivity.TAG, "Site " + siteName + " is " + distToSite + " metres from our location." +
							  " Accepting this as a valid site");
					break;
				}
			}

			// This test account works everywhere
			if (loginUser.equalsIgnoreCase("ABCorp"))
			{
				foundSite = true;
				Log.d(MainActivity.TAG, "Allowing the ABCorp login to work from anywhere");
			}

			if (foundSite)
			{
				// For the moment, all those accounts work everywhere....
				switch(loginUser)
				{
					case "ABCorp":
					case "Abcorp":
					case "ABnote":
						isValid = true;
						break;

					case "ABCorpPerth":
						isValid = true;
						break;

					case "ABCorpSthM":
						isValid = true;
						break;

					case "DotHQ":
						isValid = true;
						break;

					case "DoT":
						isValid = true;
						break;

					case "Post":
						isValid = true;
						break;
				}
			}
			else
			{
				Log.d(MainActivity.TAG, "No valid site found within " + maxDistance + " metres from our location." +
						  " We don't allow login");

				// Not sure why, but the Snackbar text is not shown, but Toast works.
				//Snackbar.make(mLayout, "You are not at a registered DoT location. Login not allowed.", Snackbar.LENGTH_LONG).setAction("Action", null).show();
				Toast.makeText(ctx, "You are not at a registered DoT location. Login not allowed.", Toast.LENGTH_LONG).show();

				// This is poorly designed at this stage. We should ensure the location is accurate before we check the
				// login. But it'll do for a quick demo where I'm just using this to ensure that if we have a non-valid
				// attempt, we are not stuck on outdated location settings.
				RequestLocationUpdate();
				return;
			}
		}

		if (isValid && (detectedOffice != null))
		{
			RunTimeProperties prop = RunTimeProperties.getInstance();
			prop.LoginLocation = mCurrentLocation;
			prop.TextProperties.put(RunTimeProperties.KEY_USERLOGIN, loginUser);
			prop.TextProperties.put(RunTimeProperties.KEY_LOGINOFFICEID, detectedOffice.OfficeId);

			DecimalFormat formatter = new DecimalFormat("#.##", DecimalFormatSymbols.getInstance(Locale.ENGLISH));
			formatter.setRoundingMode(RoundingMode.DOWN);
			String loginMsg = "Login Successful at " + siteName + " - " + formatter.format(distToSite) + " metres";

			Toast.makeText(ctx, loginMsg, Toast.LENGTH_SHORT).show();
			//Snackbar.make(view, loginMsg, Snackbar.LENGTH_SHORT).setAction("Action", null).show();
			android.util.Log.i(MainActivity.TAG, loginMsg + ", starting MainActivity intent");
			Intent intent = new Intent(LoginActivity.this, MainActivity.class);
			startActivity(intent);
		}
		else
		{
			Toast.makeText(ctx, String.format("Use %s/%s to log in to this demo", user, pass), Toast.LENGTH_LONG).show();
			/*
			Snackbar.make(view, String.format("Use %s/%s to log in to this demo", user, pass),
					  Snackbar.LENGTH_INDEFINITE).setAction("Action", null).show();
			 */
		}
	}

	// -----------------------------------------------------------------------------------------------------------------

	private void setupVariables()
	{
		try
		{
			mUserName = findViewById(R.id.login_user);
			mPassword = findViewById(R.id.login_pass);
			mLayout = findViewById(R.id.content_login);
		}
		catch(Exception ex)
		{
			android.util.Log.e(MainActivity.TAG, "Exception in LoginActivity.setupVariables : " + ex.getMessage());
		}
	}

	// -----------------------------------------------------------------------------------------------------------------

	//region Request access permissions

	// -----------------------------------------------------------------------------------------------------------------

	private void RequestMapPermission()
	{
		Log.d(MainActivity.TAG, "We don't have map permission yet, request it");

		final String[] requests = new String[]
				  {
							 // Manifest.permission.ACCESS_COARSE_LOCATION, 		// We don't appear to require coarse if we have Fine location
							 android.Manifest.permission.ACCESS_FINE_LOCATION
				  };

		if ((ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.ACCESS_FINE_LOCATION)) ||
				  (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)))
		{
			// If the user had previously refused access, provide the rationale why we need GPS location access.
			Log.i(MainActivity.TAG,	"Displaying map permission rationale to provide additional context.");
			Snackbar.make(mLayout, R.string.perm_info,
					  Snackbar.LENGTH_INDEFINITE)
					  .setAction("OK", new View.OnClickListener()
					  {
						  @Override
						  public void onClick(View view)
						  {
							  ActivityCompat.requestPermissions(LoginActivity.this, requests, REQUEST_MAP);
						  }
					  })
					  .show();
		}
		else
		{
			ActivityCompat.requestPermissions(this, requests, REQUEST_MAP);
		}
	}

	// -----------------------------------------------------------------------------------------------------------------

	private void RequestCameraPermission()
	{
		Log.d(MainActivity.TAG, "We don't have camera permission yet, request it");

		final String[] requests = new String[]
				  {
						android.Manifest.permission.CAMERA,
						android.Manifest.permission.WRITE_EXTERNAL_STORAGE
				  };

		if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA))
		{
			// If the user had previously refused access, provide the rationale why we need Camera location access.
			Log.i(MainActivity.TAG,	"Displaying camera permission rationale to provide additional context.");
			Snackbar.make(mLayout, R.string.perm_cam_info,
					  Snackbar.LENGTH_INDEFINITE)
					  .setAction("OK", new View.OnClickListener()
					  {
						  @Override
						  public void onClick(View view)
						  {
							  ActivityCompat.requestPermissions(LoginActivity.this, requests, REQUEST_CAM);
						  }
					  })
					  .show();
		}
		else
		{
			ActivityCompat.requestPermissions(this, requests, REQUEST_CAM);
		}

	}

	// -----------------------------------------------------------------------------------------------------------------

	private void RequestTelephonyPermission()
	{
		Log.d(MainActivity.TAG, "We don't have handset permission yet, request it");

		final String[] requests = new String[] { android.Manifest.permission.READ_PHONE_STATE};

		if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_PHONE_STATE))
		{
			// If the user had previously refused access, provide the rationale why we need Camera location access.
			Log.i(MainActivity.TAG,	"Displaying phone permission rationale to provide additional context.");

			Snackbar.make(mLayout, R.string.perm_phone_info,
				  Snackbar.LENGTH_INDEFINITE)
				  .setAction("OK", new View.OnClickListener()
				  {
					  @Override
					  public void onClick(View view)
					  {
						  ActivityCompat.requestPermissions(LoginActivity.this, requests, REQUEST_PHN);
					  }
				  })
				  .show();
		}
		else
		{
			ActivityCompat.requestPermissions(this, requests, REQUEST_PHN);
		}
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * Callback received when a permissions request has been completed.
	 */
	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
	{
		if ((requestCode == REQUEST_MAP) || (requestCode == REQUEST_CAM))
		{
			String type = "LOCATION";
			if (requestCode == REQUEST_CAM)
				type = "CAMERA";
			else if (requestCode == REQUEST_PHN)
				type = "PHONE";

			Log.i(MainActivity.TAG, "Received response for permission request.");

			Context ctx = getApplicationContext();

			// pto 20180829 : There's an issue with Snackbar under Android 5.1. For the time being, just use Toast instead
			// Check if the required permission were been granted
			if (verifyPermissions(grantResults))
			{
				// Location permission has been granted, we can allow operation
				Log.i(MainActivity.TAG, type + " permission has now been granted.");

				Toast.makeText(ctx, type + R.string.perm_granted, Toast.LENGTH_SHORT).show();
				//Snackbar.make(mLayout, type + R.string.perm_granted, Snackbar.LENGTH_SHORT).show();
			}
			else
			{
				Log.i(MainActivity.TAG, type + " permission was NOT granted.");
				//Toast.makeText(ctx, type + R.string.perm_denied, Toast.LENGTH_SHORT).show();
				Snackbar.make(mLayout, type + R.string.perm_denied, Snackbar.LENGTH_SHORT).show();
			}
		}
		else
		{
			super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		}
	}

	// -----------------------------------------------------------------------------------------------------------------

	private static boolean verifyPermissions(int[] grantResults)
	{
		// At least one result must be checked.
		if(grantResults.length < 1)
		{
			return false;
		}

		// Verify that each required permission has been granted, otherwise return false.
		for (int result : grantResults)
		{
			if (result != PackageManager.PERMISSION_GRANTED)
			{
				return false;
			}
		}
		return true;
	}

	// -----------------------------------------------------------------------------------------------------------------

	//endregion

	// -----------------------------------------------------------------------------------------------------------------

}
