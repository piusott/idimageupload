package com.abcorp.abid.idimageupload;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.abcorp.abid.imagedetection.FaceDetect;
import com.abcorp.abid.imagedetection.FindQRCode;
import com.abcorp.abid.imagedetection.Marker;
import com.abcorp.abid.imagedetection.MarkerDetector;
import com.abcorp.abid.imagedetection.SignatureCrop;
import com.google.android.gms.vision.face.Face;
import com.google.zxing.NotFoundException;

import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

/**
 * Fragment that displays the data deciphered from the captured photo. The photo would normally include a face, a
 * signature and a QR code with applicant details. Try to find each of these.
 */

public class FaceOverlayFragment extends Fragment
{

	private SigOverlayView mSigOverlayView;
	private Bitmap mSourceImage;			// Source image from camera capture
	private Bitmap mSignature = null;	// Signature slip part of source image
	private Bitmap mQRImage;				// Cropped image that has the QR code in it

	private String mHolderId = "";		// if pre-detected, these will hold data
	private String mCardType = "";
	private String mApplicantName = "";

	private TextView mHolderIdView;
	private TextView mNameView;
	private TextView mCardTypeView;
	private ImageView mIVPortrait;

	private AlertDialog mPopup = null;
	private Timer mOneShotTimer;		// timer used to simulate an image upload process
	private MyTimerTask mTimerTask;

	private FaceDetect mFaceDetect;		// Helper class that uses the Google face finder API

	// The following are coordinates of the face, signature and QR code that were read during the live stream detection.
	// I'm using these as a shortcut and to speed up the capture process. If we have them available, use them to crop
	// the respective images. If not, then we need to run another detection process on the captured image.
	private RectF mFaceRect;				// Face coordinates read from the FaceGraphic overlay.
	private Rect mSigRect;					// Signature coordinates read from the FaceGraphic overlay.
	private Rect mQRRect;					// Face coordinates read from the FaceGraphic overlay.

	// TODO : very ugly hardcoding. This needs to be done properly. The value is the size factor that allows us to
	//        scale up from the preview image size to the captured image.
	private static final float IMG_SIZE_FACTOR = 2.37f;

	private static final int DEFAULT_IMG_QUALITY = 85;		// default image compression factor

	// -----------------------------------------------------------------------------------------------------------------

	public FaceOverlayFragment()
	{
		mFaceRect = null;
		mSigRect = null;
		mQRRect = null;
	}

	// -----------------------------------------------------------------------------------------------------------------


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View rootView = inflater.inflate(R.layout.fragment_capturepreview, container, false);

		mHolderIdView = rootView.findViewById(R.id.edHolderId);
		mNameView = rootView.findViewById(R.id.edName);
		mCardTypeView = rootView.findViewById(R.id.edCardType);
		mHolderIdView.setText("-");
		mNameView.setText("-");
		mCardTypeView.setText("-");

		mIVPortrait = rootView.findViewById(R.id.ivPortrait);
		mSigOverlayView = new SigOverlayView(getActivity(), rootView);

		RelativeLayout relativeLayout = rootView.findViewById(R.id.face_overlay_main);
		relativeLayout.addView(mSigOverlayView);

		Button upload = rootView.findViewById(R.id.btnSend);
		upload.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				// save the captured portrait, signature and demographics data locally.
				Bitmap bmPortrait = ((BitmapDrawable)mIVPortrait.getDrawable()).getBitmap();
				if (bmPortrait != null)
				{
					/*
					** Commented this out for the moment. Storing the original image in JPG format with a compression
					** factor of 85 seems to be a lot quicker still than rescaling it and then saving it. Plus the quality
					** is better.
					*
					// Saving an image is a very slow process, especially for large images. So reduce the image size before
					// saving them. Make sure we don't distort the image in the process.
					final int scaledHeight = 640;
					double ratio = (double)bmPortrait.getWidth() / (double)bmPortrait.getHeight();
					int calcWidth = (int)(scaledHeight * ratio);
					Log.d(MainActivity.TAG, "Saving rescaled portrait: 640x" + calcWidth);

					Bitmap bmScaled = Bitmap.createScaledBitmap(bmPortrait, calcWidth, scaledHeight, true);
					MainActivity.saveBitmap(bmScaled,
							  MainActivity.getOutputMediaFile(mCardType, mHolderId, "port", true), DEFAULT_IMG_QUALITY);
					*/

					MainActivity.saveBitmap(bmPortrait,
							  MainActivity.getOutputMediaFile(mCardType, mHolderId, "port", true), DEFAULT_IMG_QUALITY);

					// Store the converted b/w signature, not the colour original
					Bitmap bmSig = mSigOverlayView.getConvertedSignature();
					if (bmSig != null)
					{
						MainActivity.saveBitmap(bmSig, MainActivity.getOutputMediaFile(mCardType, mHolderId, "sig", true), 100);
					}

					// create a popup that shows a progressbar for two seconds to 'simulate' a capture upload. After two
					// seconds the popup closes itself and returns the app to the MainActivity
					Activity activity = (Activity)v.getContext();
					mPopup = MakePopupDialog(activity, R.layout.upload_popup);
					mTimerTask = new MyTimerTask();
					mOneShotTimer = new Timer();
					mOneShotTimer.schedule(mTimerTask, 2000);
				}
			}
		});

		mFaceDetect = new FaceDetect(getActivity());

		setBitmap();

		return rootView;
	}

	// -----------------------------------------------------------------------------------------------------------------

	class MyTimerTask extends TimerTask
	{

		@Override
		public void run()
		{
			Activity activity = getActivity();
			activity.runOnUiThread(new Runnable()
			{

				@Override
				public void run()
				{
					// close the popup screen again
					if (mPopup != null)
					{
						mPopup.cancel();
						mPopup = null;

						((MainActivity)getActivity()).restoreBackdrop();
					}
				}
			});
		}
	}

	// -----------------------------------------------------------------------------------------------------------------

	private AlertDialog MakePopupDialog(Activity activity, int dialogId)
	{
		LayoutInflater inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View popupView = inflater.inflate(dialogId, null);
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		builder.setView(popupView);
		AlertDialog result = builder.create();
		result.show();

		mPopup = result;
		return result;
	}

	// -----------------------------------------------------------------------------------------------------------------

	private void setBitmap()
	{
		if (mSourceImage != null)
		{
			Log.d(MainActivity.TAG, "** 6. Before running findFaces");
			if (mFaceRect == null)
			{
				Log.d(MainActivity.TAG, "We don't have an overlay face rectangle, detect again");
				mFaceDetect.findFaces(mSourceImage);
			}
			else
			{
				Log.d(MainActivity.TAG, "We already have an overlay face rectangle. No need to run detect");
			}
			Log.d(MainActivity.TAG, "** 7. After running findFaces");
		}
		else
		{
			/*
			// just for testing, load a sample bitmap here...
			Context ctx = getActivity();
			AssetManager asm = ctx.getAssets();

			String imgFileName = "test6sigslip.jpg";
			try
			{
				InputStream isImg = asm.open(imgFileName);
				int fileSize = isImg.available();
				byte[] rawImgData = new byte[fileSize];
				BufferedInputStream buf = new BufferedInputStream(isImg);
				buf.read(rawImgData, 0, rawImgData.length);
				buf.close();

				Bitmap portrait = BitmapFactory.decodeByteArray(rawImgData, 0, rawImgData.length);
				mFaceDetect.findFaces(portrait);
			}
			catch(IOException e)
			{
				Log.e(MainActivity.TAG, "IOException on loading image [" + imgFileName + "] : " + e.getMessage());
			}
			catch(Exception e)
			{
				Log.e(MainActivity.TAG, "Exception on loading image [" + imgFileName + "] : " + e.getMessage());
			}
			*/
		}

		Log.d(MainActivity.TAG, "** 8. Before running decipherImage");
		decipherImage();
		Log.d(MainActivity.TAG, "** 9. After running decipherImage");
	}

	// -----------------------------------------------------------------------------------------------------------------

	public void setData(Bitmap portrait, Bitmap signature, String applicantName, String holderId, String cardType)
	{
		mSourceImage = portrait;
		mSignature = signature;
		mHolderId = holderId;
		mApplicantName = applicantName;
		mCardType = cardType;
	}

	// -----------------------------------------------------------------------------------------------------------------

	public void setBitmap(Bitmap source, RectF faceRect, Rect sigRect, Rect qrRect)
	{
		mSourceImage = source;
		mFaceRect = faceRect;
		mSigRect = sigRect;
		mQRRect = qrRect;

		// clear data that may have been passed in if we did a dual shot (separate sigslip/portrait) capture previously
		// but have now switched to single shot.
		mSignature = null;
		mHolderId = "";
		mApplicantName = "";
		mCardType = "";
	}

	// -----------------------------------------------------------------------------------------------------------------

	private void decipherImage()
	{
		Rect faceRectangle = getFaceRectangle();
		int faceBottom = faceRectangle.top + faceRectangle.height();

		if (mSignature == null)		// we haven't already been passed in a valid signature image
		{
			try
			{
				cropSignatureAndQR(mSourceImage);

				if ((mSignature == null) || (mQRImage == null))
				{
					// try again with a cropped image. Maybe we've cropped out part of the sigslip that contains the markers
					int horzPadding = (int) (((float) faceRectangle.width() / 100) * 10); // 10% at either side = total padding = 20%
					Log.d(MainActivity.TAG, "** 8.1. Creating temp signature and QR image");
					Bitmap tmpImg = Bitmap.createBitmap(mSourceImage, faceRectangle.left - horzPadding, faceBottom, faceRectangle.width() + (2 * horzPadding), mSourceImage.getHeight() - faceBottom);
					MainActivity.saveBitmap(tmpImg, MainActivity.getOutputMediaFile("test", "-", "croppedSlip"), DEFAULT_IMG_QUALITY);

					// reset these to force re-detection on the tmp image. The passed in rectangles refer to the whole image
					// but now we want to process a cropped one.
					mSigRect = null;
					mQRRect = null;
					cropSignatureAndQR(tmpImg);
				}

				if ((mSignature == null) || (mQRImage == null))
				{
					if (mSignature == null) Log.i(MainActivity.TAG, "No signature detected");

					if (mQRImage == null) Log.i(MainActivity.TAG, "No QR code detected");
				}
			}
			catch(Exception e)
			{
				Log.e(MainActivity.TAG, "Exception on creating sigslip image : " + e.getMessage());
				mSignature = null;
				mQRImage = null;
			}
		}

		Log.d(MainActivity.TAG, "** 8.2. Before finding QR code");
		if (faceRectangle.height() > 0)	// we have a face
		{
			int sourceImgHeight = mSourceImage.getHeight();

			if (mApplicantName.length() > 0)		// if we have pre-populated these, do not attempt to detect again
			{
				Log.d(MainActivity.TAG, "** 8.2.1. Using pre-populated data");
				mNameView.setText(mApplicantName);
				mCardTypeView.setText(mCardType);
				mHolderIdView.setText(mHolderId);
			}
			else
			{
				try
				{
					Log.d(MainActivity.TAG, "** 8.2.1. Attempting to detect QR data");
					// our signature has now been cropped and will no longer have the QR code data on it. We need to
					// create another temp bitmap based on the image markers and locate the rough area of the QR code,
					// then read it from that one
					if (mQRImage == null)
					{
						// If we haven't managed to find ArUco markers to help us locate the QR code in the image, create a
						// larger image that starts from somewhere below the face and try again.
						mQRImage = Bitmap.createBitmap(mSourceImage, 0, faceBottom, mSourceImage.getWidth(), sourceImgHeight - faceBottom);
					}
					FindQRCode.QRData qrValue = FindQRCode.readQRCode(mQRImage);
					Log.d(MainActivity.TAG, "** 8.2. Read QR code complete");
					mApplicantName = qrValue.HolderName;		// keep these for later use when we save the images
					mCardType = qrValue.CardType;
					mHolderId = qrValue.HolderId;

					mNameView.setText(qrValue.HolderName);
					mCardTypeView.setText(qrValue.CardType);
					mHolderIdView.setText(String.format("%s %s", qrValue.HolderId, qrValue.CheckDigit));
					// Toast.makeText(getActivity(), qrValue, Toast.LENGTH_LONG).show();
				}
				catch(NotFoundException e)
				{
					Log.e(MainActivity.TAG, "NotFoundException on reading QR Code : " + e.getMessage());
				}
			}

			try
			{
				Bitmap portrait = Bitmap.createBitmap(mSourceImage,
						  faceRectangle.left,
						  faceRectangle.top,
						  faceRectangle.width(),
						  faceRectangle.height());

				mIVPortrait.setImageBitmap(portrait);
				Log.d(MainActivity.TAG, "** 8.3. Set portrait image");
			}
			catch(Exception e)
			{
				Log.e(MainActivity.TAG, "Exception on creating portrait image : " + e.getMessage());
			}

			Log.d(MainActivity.TAG, "** 8.4. About to set signature image");
			mSigOverlayView.setData(mSignature);
		}
		else
		{
			Toast.makeText(getActivity(), "No face detected in captured photo", Toast.LENGTH_LONG).show();
		}

	}

	// -----------------------------------------------------------------------------------------------------------------

	private Rect getFaceRectangle()
	{
		Rect result = new Rect();

		if (mFaceRect != null)
		{
			// If available, use the face coordinates detected from the camera overlay. This is a much faster (although
			// less precise) approach.
			// The factor 2.37 is the difference between the camera overlay image and our finished bitmap.
			// TODO : this shouldn't be hardcoded. We need to calculate the difference based on camera image size and bitmap
			result.left = (int)(mFaceRect.left * IMG_SIZE_FACTOR);
			result.top = (int)(mFaceRect.top * IMG_SIZE_FACTOR);
			result.right = (int)(mFaceRect.right * IMG_SIZE_FACTOR);
			result.bottom = (int)(mFaceRect.bottom * IMG_SIZE_FACTOR);
			Log.d(MainActivity.TAG, "Using overlay face rect : " + mFaceRect);
		}
		else
		{
			Face firstFace = mFaceDetect.getFirstFace();
			if (firstFace != null)
			{
				Log.d(MainActivity.TAG, "Using mFaceDetect face finder");

				PointF facePos = firstFace.getPosition();
				int faceWidth = (int) firstFace.getWidth();
				int faceHeight = (int) firstFace.getHeight();

				// TODO : make sure our generated rectangle does not run outside of the source image
				// We need to add some padding around the face to create a proper portrait image
				int horzPadding = (int) (((float) faceWidth / 100) * 15); // 15% at either side = total padding = 30%

				// For vertical padding we add less at the top than at the bottom because we want to see some of the
				// neck and chest while we don't need much padding at the top
				int vertPaddingTop = (int) (((float) faceHeight / 100) * 8);
				int vertPaddingBottom = (int) (((float) faceHeight / 100) * 32);
				result.top = (int) (facePos.y - vertPaddingTop);
				if (result.top < 0) result.top = 0;

				result.left = (int) (facePos.x - horzPadding);
				if (result.left < 0)
					result.left = 0;

				int portWidth = faceWidth + (2 * horzPadding);
				if ((portWidth + result.left) > mSourceImage.getWidth())
					portWidth = (mSourceImage.getWidth() - result.left);

				int portHeight = faceHeight + vertPaddingTop + vertPaddingBottom;
				if ((portHeight + result.top) > mSourceImage.getHeight())
					portHeight = mSourceImage.getHeight() - result.top;

				result.right = result.left + portWidth;
				result.bottom = result.top + portHeight;

				Log.d(MainActivity.TAG, "Detected Face Rect : " + result);
			}
		}

		Log.d(MainActivity.TAG, "getFaceRectangle face coordinates : " + result);

		return result;
	}

	// -----------------------------------------------------------------------------------------------------------------

	private Vector<Rect> findSigAndQRRect(Bitmap sourceImage)
	{
		Vector<Rect> result = new Vector<>();

		if ((mSigRect != null) && (mQRRect != null))
		{
			Log.d(MainActivity.TAG, "Using signature and marker coordinates from overlay");

			// If available, use the signature coordinates detected from the camera overlay. This is a much faster (although
			// less precise) approach.
			// The factor 2.37 is the difference between the camera overlay image and our finished bitmap.
			// TODO : this shouldn't be hardcoded. We need to calculate the difference based on camera image size and bitmap
			Rect sigRect = new Rect();
			sigRect.left = (int)(mSigRect.left * IMG_SIZE_FACTOR);
			sigRect.top = (int)(mSigRect.top * IMG_SIZE_FACTOR);
			sigRect.right = (int)(mSigRect.right * IMG_SIZE_FACTOR);
			sigRect.bottom = (int)(mSigRect.bottom * IMG_SIZE_FACTOR);
			Log.d(MainActivity.TAG, "Using overlay sig rect : " + mSigRect);

			Rect qrRect = new Rect();
			qrRect.left = (int)(mQRRect.left * IMG_SIZE_FACTOR);
			qrRect.top = (int)(mQRRect.top * IMG_SIZE_FACTOR);
			qrRect.right = (int)(mQRRect.right * IMG_SIZE_FACTOR);
			qrRect.bottom = (int)(mQRRect.bottom * IMG_SIZE_FACTOR);

			result.add(SignatureCrop.RECT_ID_SIGNATURE, sigRect);
			result.add(SignatureCrop.RECT_ID_QR, qrRect);
		}
		else
		{
			Log.d(MainActivity.TAG, "Detecting signature and marker from captured image");

			// detect signature markers from captured image
			Vector<Integer> validMarkers = new Vector<>();
			validMarkers.add(SignatureCrop.MARKER_ID_TOPLEFT);   // IDs of our two markers on the signature slip, bottom-right marker
			validMarkers.add(SignatureCrop.MARKER_ID_BOTTOMRIGHT);   // top-left marker

			MarkerDetector md = new MarkerDetector(validMarkers);
			Vector<Marker> markers = md.processFrame(sourceImage);

			if (markers.size() == 2)
			{
				Vector<SignatureCrop.SparseMarker> sigMarkers = new Vector<>();
				Marker m1 = markers.elementAt(0);
				SignatureCrop.SparseMarker sm1 = new SignatureCrop.SparseMarker(m1.getId());
				sm1.setPoints(m1.getPoints());
				Marker m2 = markers.elementAt(1);
				SignatureCrop.SparseMarker sm2 = new SignatureCrop.SparseMarker(m2.getId());
				sm2.setPoints(m2.getPoints());

				if (m1.getId() == SignatureCrop.MARKER_ID_TOPLEFT)
				{
					sigMarkers.add(0, sm1);
					sigMarkers.add(1, sm2);
				}
				else
				{
					sigMarkers.add(0, sm2);
					sigMarkers.add(1, sm1);
				}

				result = SignatureCrop.getSignatureRect(sigMarkers);
			}
		}

		return result;
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * Crop the signature based on the ArUco Markers on the signature slip. Also create the image that holds the QR
	 * code so we can speed up detecting the QR code data.
	 */
	private void cropSignatureAndQR(Bitmap sourceImage)
	{
//		MainActivity.saveBitmap(sourceImage, MainActivity.getOutputMediaFile("source"));

		Vector<Rect> imgRecs = findSigAndQRRect(sourceImage);
		if ((imgRecs != null) && (imgRecs.size() == 2))
		{
			Rect sigRect = imgRecs.get(SignatureCrop.RECT_ID_SIGNATURE);

			Log.d(MainActivity.TAG, "** 8.1.4 Creating signature image");
			if (sigRect.left < 0)
				sigRect.left = 0;

			if (sigRect.top < 0)
				sigRect.top = 0;

			if (sigRect.right > (sourceImage.getWidth()))
				sigRect.right = sourceImage.getWidth();

			if (sigRect.bottom > sourceImage.getHeight())
				sigRect.bottom = sourceImage.getHeight();

			mSignature = Bitmap.createBitmap(sourceImage, sigRect.left, sigRect.top, sigRect.width(), sigRect.height());
			//// Testing - save Sig image so I can check if the cropping rectangle looks ok
			MainActivity.saveBitmap(mSignature, MainActivity.getOutputMediaFile("test", "-", "SIG"), DEFAULT_IMG_QUALITY);

			Rect qrRect = imgRecs.get(SignatureCrop.RECT_ID_QR);
			Log.d(MainActivity.TAG, "** 8.1.5 Creating QR image");

			if (qrRect.left < 0)
				qrRect.left = 0;

			if (qrRect.top < 0)
				qrRect.top = 0;

			if (qrRect.right > (sourceImage.getWidth()))
				qrRect.right = sourceImage.getWidth();

			if (qrRect.bottom > sourceImage.getHeight())
				qrRect.bottom = sourceImage.getHeight();

			mQRImage = Bitmap.createBitmap(sourceImage, qrRect.left, qrRect.top, qrRect.width(), qrRect.height());

			//// Testing - save QR image so I can check if the cropping rectangle looks ok
			MainActivity.saveBitmap(mQRImage, MainActivity.getOutputMediaFile("test", "-","QR"), DEFAULT_IMG_QUALITY);
		}
	}

	// -----------------------------------------------------------------------------------------------------------------

}


