package com.abcorp.abid.idimageupload;


import android.Manifest;
import android.app.Fragment;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.abcorp.abid.camera.ExtCameraSource;
import com.abcorp.abid.camera.ExtCameraSourcePreview;
import com.abcorp.abid.imagedetection.ExtFaceDetector;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.MultiProcessor;
import com.google.android.gms.vision.Tracker;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.samples.vision.face.facetracker.ui.camera.GraphicOverlay;
import com.google.android.gms.samples.vision.face.facetracker.ui.camera.FaceGraphic;

import org.opencv.android.CameraBridgeViewBase;
import org.opencv.core.Mat;

import java.io.IOException;

/**
 * Photo capture fragment. Used to capture a picture of the applicant, the signature and the QR code. We want to
 * capture all these in a single image and then pick out the bits we need.
 */

public class CameraFragment extends Fragment implements CameraBridgeViewBase.CvCameraViewListener2
{
	private ExtCameraSource mCameraSource = null;
	private ExtCameraSourcePreview mPreview;
	private GraphicOverlay mGraphicOverlay;
	private RectF mCurrentFaceRect;
	private Rect mCurrentSigRect;
	private Rect mCurrentQRRect;

	private static final int RC_HANDLE_GMS = 9001;
	private static final int RC_HANDLE_CAMERA_PERM = 2;	// permission request codes need to be < 256

	private ExtFaceDetector mDetector;

	// -----------------------------------------------------------------------------------------------------------------

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.fragment_camera, container, false);

		Button takePictureButton = view.findViewById(R.id.btn_takepicture);
		assert takePictureButton != null;

		mPreview = view.findViewById(R.id.preview);
		mGraphicOverlay = view.findViewById(R.id.faceOverlay);

		// Check for the camera permission before accessing the camera.  If the
		// permission is not granted yet, request permission.
		int rc = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA);
		if (rc == PackageManager.PERMISSION_GRANTED)
		{
			createCameraSource();
		}
		else
		{
			requestCameraPermission();
		}

		// Trap the capture button.
		takePictureButton.setOnClickListener(
				  new View.OnClickListener()
				  {
					  @Override
					  public void onClick(View v)
					  {
						  // get an image from the camera and save it to a file
						  if(mCameraSource == null)
						  {
							  Log.e(MainActivity.TAG, "mCameraSource is null");
							  return;
						  }

						  Log.d(MainActivity.TAG, "Current Focus Mode : " + mCameraSource.getFocusMode());
						  mCameraSource.autoFocus(afCallback);

						  //mCameraSource.takePicture(shutterCallback, jpegCallback);
					  }

					  private final ExtCameraSource.AutoFocusCallback afCallback = new ExtCameraSource.AutoFocusCallback()
					  {

						  @Override
						  public void onAutoFocus(boolean success)
						  {
							  Log.d(MainActivity.TAG, "AutoFocus complete - take picture");
							  mCameraSource.takePicture(shutterCallback, jpegCallback);
						  }
					  };

				  }
		);

		return view;
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * Required to be implemented by CvCameraViewListener2 but not used here
	 * @param width -  the width of the frames that will be delivered
	 * @param height - the height of the frames that will be delivered
	 */
	public void onCameraViewStarted(int width, int height)
	{
	}

	/**
	 * Required to be implemented by CvCameraViewListener2 but not used here
	 */
	public void onCameraViewStopped()
	{
	}

	/**
	 * Required to be implemented by CvCameraViewListener2 but not used here
	 * @param inputFrame frame received
	 * @return Mat of the inputFrame
	 */
	public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame)
	{
		return inputFrame.rgba();
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * Handles the requesting of the camera permission.  This includes
	 * showing a "Snackbar" message of why the permission is needed then
	 * sending the request.
	 */
	private void requestCameraPermission()
	{
		Log.w(MainActivity.TAG, "Camera permission is not granted. Requesting permission");

		final String[] permissions = new String[]{Manifest.permission.CAMERA};

		if (!ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.CAMERA))
		{
			ActivityCompat.requestPermissions(getActivity(), permissions, RC_HANDLE_CAMERA_PERM);
			return;
		}

		final Activity thisActivity = getActivity();

		View.OnClickListener listener = new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				ActivityCompat.requestPermissions(thisActivity, permissions, RC_HANDLE_CAMERA_PERM);
			}
		};

		Snackbar.make(mGraphicOverlay, R.string.perm_cam_info,
				  Snackbar.LENGTH_INDEFINITE)
				  .setAction("ok", listener)
				  .show();
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * Creates and starts the camera.  Note that this uses a higher resolution in comparison
	 * to other detection examples to enable the barcode detector to detect small barcodes
	 * at long distances.
	 */
	private void createCameraSource()
	{
		Context context = getActivity();
		mDetector = new ExtFaceDetector.Builder(context)
				  .setClassificationType(ExtFaceDetector.ALL_CLASSIFICATIONS)
				  .build();

		mDetector.setProcessor(
				  new MultiProcessor.Builder<>(new GraphicFaceTrackerFactory())
							 .build());

		mDetector.setMarkerProcessor();	// Also check for signature markers

		if (!mDetector.isOperational())
		{
			// Note: The first time that an app using face API is installed on a device, GMS will
			// download a native library to the device in order to do detection.  Usually this
			// completes before the app is run for the first time.  But if that download has not yet
			// completed, then the above call will not detect any faces.
			//
			// isOperational() can be used to check if the required native library is currently
			// available.  The detector will automatically become operational once the library
			// download completes on device.
			Log.w(MainActivity.TAG, "Face detector dependencies are not yet available.");
		}

		mCameraSource = new ExtCameraSource.Builder(context, mDetector)
				  .setRequestedPreviewSize(640, 480)
				  .setFacing(CameraSource.CAMERA_FACING_BACK)
				  .setRequestedFps(30.0f)
				  .build();
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * Restarts the camera.
	 */
	@Override
	public void onResume()
	{
		super.onResume();
		startCameraSource();
	}

	/**
	 * Stops the camera.
	 */
	@Override
	public void onPause()
	{
		super.onPause();
		mPreview.stop();
	}

	/**
	 * Releases the resources associated with the camera source, the associated detector, and the
	 * rest of the processing pipeline.
	 */
	@Override
	public void onDestroy()
	{
		super.onDestroy();
		if (mCameraSource != null)
		{
			mCameraSource.release();
		}
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * Callback for the result from requesting permissions. This method
	 * is invoked for every call on {@link #requestPermissions(String[], int)}.
	 * <p>
	 * <strong>Note:</strong> It is possible that the permissions request interaction
	 * with the user is interrupted. In this case you will receive empty permissions
	 * and results arrays which should be treated as a cancellation.
	 * </p>
	 *
	 * @param requestCode  The request code passed in {@link #requestPermissions(String[], int)}.
	 * @param permissions  The requested permissions. Never null.
	 * @param grantResults The grant results for the corresponding permissions
	 *                     which is either {@link PackageManager#PERMISSION_GRANTED}
	 *                     or {@link PackageManager#PERMISSION_DENIED}. Never null.
	 * @see #requestPermissions(String[], int)
	 */
	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
	{
		if (requestCode != RC_HANDLE_CAMERA_PERM)
		{
			Log.d(MainActivity.TAG, "Got unexpected permission result: " + requestCode);
			super.onRequestPermissionsResult(requestCode, permissions, grantResults);
			return;
		}

		if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
		{
			Log.d(MainActivity.TAG, "Camera permission granted - initialize the camera source");
			// we have permission, so create the camerasource
			createCameraSource();
			return;
		}

		Log.e(MainActivity.TAG, "Permission not granted: results len = " + grantResults.length +
				  " Result code = " + (grantResults.length > 0 ? grantResults[0] : "(empty)"));

		Toast.makeText(getActivity(), "Sorry!!!, you can't use this app without granting permission", Toast.LENGTH_LONG).show();
		////////finish();
	}

	// -----------------------------------------------------------------------------------------------------------------
	// Camera Source Preview
	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * Starts or restarts the camera source, if it exists.  If the camera source doesn't exist yet
	 * (e.g., because onResume was called before the camera source was created), this will be called
	 * again when the camera source is created.
	 */
	private void startCameraSource()
	{
		// check that the device has play services available.
		int code = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(getActivity());
		if (code != ConnectionResult.SUCCESS)
		{
			Dialog dlg = GoogleApiAvailability.getInstance().getErrorDialog(getActivity(), code, RC_HANDLE_GMS);
			dlg.show();
		}

		if (mCameraSource != null)
		{
			try
			{
				mPreview.start(mCameraSource, mGraphicOverlay);
			}
			catch (IOException e)
			{
				Log.e(MainActivity.TAG, "Unable to start camera source.", e);
				mCameraSource.release();
				mCameraSource = null;
			}
			catch(SecurityException ex)
			{
				Log.e(MainActivity.TAG, "SecurityException : Camera use has not been authorised.", ex);
				mCameraSource.release();
				mCameraSource = null;
			}
		}
	}

	// -----------------------------------------------------------------------------------------------------------------
	// Graphic Face Tracker
	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * Factory for creating a face tracker to be associated with a new face. The multiprocessor
	 * uses this factory to create face trackers as needed -- one for each individual.
	 */
	private class GraphicFaceTrackerFactory implements MultiProcessor.Factory<Face>
	{
		@Override
		public Tracker<Face> create(Face face)
		{
			return new GraphicFaceTracker(mGraphicOverlay);
		}
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * Face tracker for each detected individual. This maintains a face graphic within the app's
	 * associated face overlay.
	 */
	private class GraphicFaceTracker extends Tracker<Face>
	{
		private GraphicOverlay mOverlay;
		private FaceGraphic mFaceGraphic;


		GraphicFaceTracker(GraphicOverlay overlay)
		{
			mOverlay = overlay;
			mFaceGraphic = new FaceGraphic(overlay);
		}

		/**
		 * Start tracking the detected face instance within the face overlay.
		 */
		@Override
		public void onNewItem(int faceId, Face item)
		{
			mFaceGraphic.setId(faceId);
		}

		/**
		 * Update the position/characteristics of the face within the overlay.
		 */
		@Override
		public void onUpdate(ExtFaceDetector.Detections<Face> detectionResults, Face face)
		{
			mOverlay.add(mFaceGraphic);
			mFaceGraphic.updateFace(face);

			// TODO : don't leave this ugliness in there. I'm just testing feasibility of showing markers and face
			mFaceGraphic.setMarkers(mDetector.detectedMarkers());

			// More ugliness. For this proof of concept, I'm doing quick rather than elegant implementation. I want
			// the face, signature and QR code locations detected in the overlay rather than detecting them from the
			// captured picture. There are a couple of reasons for this :
			//   1. It's much quicker. The captured image is significantly larger than our preview which makes the
			//      detection, particularly face, quite slow.
			//   2. I'm also having a lot of issues detecting the ArUco markers on the larger image. Not sure why but
			//      since we're very likely to already have a signature and QR rectangle available, use that instead.
			//      This also makes it possible to guarantee that we can capture a signature and QR because we could
			//      only enable the capture button if a signature and face has been found.
			mCurrentFaceRect = mFaceGraphic.getFaceRectangle();
			mCurrentSigRect = mFaceGraphic.getSigRectangle();
			mCurrentQRRect = mFaceGraphic.getQRRectangle();
		}

		/**
		 * Hide the graphic when the corresponding face was not detected.  This can happen for
		 * intermediate frames temporarily (e.g., if the face was momentarily blocked from
		 * view).
		 */
		@Override
		public void onMissing(ExtFaceDetector.Detections<Face> detectionResults)
		{
			mOverlay.remove(mFaceGraphic);
		}

		/**
		 * Called when the face is assumed to be gone for good. Remove the graphic annotation from
		 * the overlay.
		 */
		@Override
		public void onDone()
		{
			mOverlay.remove(mFaceGraphic);
		}
	}

	// -----------------------------------------------------------------------------------------------------------------

	private final ExtCameraSource.ShutterCallback shutterCallback = new ExtCameraSource.ShutterCallback()
	{
		@Override
		public void onShutter()
		{
			Log.d(MainActivity.TAG, "onShutter");
			Toast.makeText(getActivity(), R.string.capture_info, Toast.LENGTH_SHORT).show();
		}
	};

	// -----------------------------------------------------------------------------------------------------------------

	private ExtCameraSource.PictureCallback jpegCallback = new ExtCameraSource.PictureCallback()
	{
		@Override
		public void onPictureTaken(byte[] data)
		{
			// decipher the captured image and show the results
			MainActivity activity = (MainActivity)getActivity();
			Bitmap capturedImage = BitmapFactory.decodeByteArray(data, 0, data.length);

			// if the preview was rotated, make sure we rotate the captured image to handle this correctly
			if (capturedImage.getWidth() > capturedImage.getHeight())
			{
				Log.d(MainActivity.TAG, "Rotating image before processing");
				capturedImage = rotate(capturedImage, 90);
			}
			activity.photoCaptureDone(capturedImage, mCurrentFaceRect, mCurrentSigRect, mCurrentQRRect);

			Log.d(MainActivity.TAG, "Face Rectangle : " + mCurrentFaceRect);
		}

		private Bitmap rotate(Bitmap in, int angle)
		{
			Matrix mat = new Matrix();
			mat.postRotate(angle);
			return Bitmap.createBitmap(in, 0, 0, in.getWidth(), in.getHeight(), mat, true);
		}
	};

	// -----------------------------------------------------------------------------------------------------------------

}
