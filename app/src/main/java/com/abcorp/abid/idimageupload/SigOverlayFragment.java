package com.abcorp.abid.idimageupload;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.abcorp.abid.common.SigSlipDetect;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Fragment that displays the signature and QR data deciphered from the captured photo.
 */

public class SigOverlayFragment extends Fragment
{

	private SigOverlayView mSigOverlayView;
	private Bitmap mSourceImage;			// Source image from camera capture

	private AlertDialog mPopup = null;
	private Timer mOneShotTimer;		// timer used to simulate an image upload process
	private MyTimerTask mTimerTask;

	private TextView mHolderId;
	private TextView mName;
	private TextView mCardType;

	// -----------------------------------------------------------------------------------------------------------------

	public SigOverlayFragment()
	{
	}

	// -----------------------------------------------------------------------------------------------------------------


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View rootView = inflater.inflate(R.layout.fragment_sigcapturepreview, container, false);

		mHolderId = rootView.findViewById(R.id.edHolderId);
		mName = rootView.findViewById(R.id.edName);
		mCardType = rootView.findViewById(R.id.edCardType);
		mHolderId.setText("-");
		mName.setText("-");
		mCardType.setText("-");

		mSigOverlayView = new SigOverlayView(getActivity(), rootView);

		RelativeLayout relativeLayout = rootView.findViewById(R.id.face_overlay_main);
		relativeLayout.addView(mSigOverlayView);

		Button upload = rootView.findViewById(R.id.btnSend);
		upload.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				// create a popup that shows a progressbar for two seconds to 'simulate' a capture upload. After two
				// seconds the popup closes itself and returns the app to the MainActivity
				Activity activity = (Activity)v.getContext();
				mPopup = MakePopupDialog(activity, R.layout.upload_popup);
				mTimerTask = new MyTimerTask();
				mOneShotTimer = new Timer();
				mOneShotTimer.schedule(mTimerTask, 2000);
			}
		});

		SigSlipDetect sigSlipDetect = new SigSlipDetect(mSourceImage);
		mName.setText(sigSlipDetect.ApplicantName);
		mCardType.setText(sigSlipDetect.CardType);
		mHolderId.setText(sigSlipDetect.HolderID);
		mSigOverlayView.setData(sigSlipDetect.SignatureImage);

		return rootView;
	}

	// -----------------------------------------------------------------------------------------------------------------

	class MyTimerTask extends TimerTask
	{

		@Override
		public void run()
		{
			Activity activity = getActivity();
			activity.runOnUiThread(new Runnable()
			{

				@Override
				public void run()
				{
					// close the popup screen again
					if (mPopup != null)
					{
						mPopup.cancel();
						mPopup = null;

						((MainActivity)getActivity()).restoreBackdrop();
					}
				}
			});
		}
	}

	// -----------------------------------------------------------------------------------------------------------------

	private AlertDialog MakePopupDialog(Activity activity, int dialogId)
	{
		LayoutInflater inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		assert inflater != null;
		View popupView = inflater.inflate(dialogId, null);
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		builder.setView(popupView);
		AlertDialog result = builder.create();
		result.show();

		mPopup = result;
		return result;
	}

	// -----------------------------------------------------------------------------------------------------------------

	public void setBitmap(Bitmap source)
	{
		mSourceImage = source;
	}

	// -----------------------------------------------------------------------------------------------------------------

}


