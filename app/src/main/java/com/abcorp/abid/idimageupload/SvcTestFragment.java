package com.abcorp.abid.idimageupload;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.abcorp.abid.servicecomms.RestClient;
import com.abcorp.abid.servicecomms.ServerResponseHandler;

public class SvcTestFragment extends Fragment
{
	// -----------------------------------------------------------------------------------------------------------------

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View rootView = inflater.inflate(R.layout.fragment_svctest, container, false);

		final Context ctx = getActivity().getApplicationContext();
		final TextView edResponse = rootView.findViewById(R.id.response_info);
		edResponse.setTextColor(ContextCompat.getColor(ctx, R.color.defaultText));
		edResponse.setText("Waiting for server response...");

		ServerResponseHandler handler = new ServerResponseHandler()
		{
			@Override
			public void tokenResponseReceived(String token, int expiresMinutes)
			{
				Log.d(MainActivity.TAG, "SvcTestFragment.tokenResponseReceived: " + expiresMinutes);
				edResponse.setTextColor(ContextCompat.getColor(ctx, R.color.successColour));
					edResponse.setText(
							  String.format("Connection Successful.\nReceived token that expires in %d minutes.",
										 expiresMinutes));

			}

			@Override
			public void authError(String msg)
			{
				edResponse.setTextColor(ContextCompat.getColor(ctx, R.color.errorColour));
				edResponse.setText(msg);
			}
		};
		RestClient client = new RestClient(handler, ctx);
		client.ConnectionTest();

		return rootView;
	}

	// -----------------------------------------------------------------------------------------------------------------


	// -----------------------------------------------------------------------------------------------------------------

}