package com.abcorp.abid.idimageupload;

import com.google.android.gms.maps.model.LatLng;

import java.util.HashMap;

/**
 * Collection of static properties that we need to access while running the app
 */
public class RunTimeProperties
{
	// -----------------------------------------------------------------------------------------------------------------

	// Keys to use with the TextProperties HashMap
	public final static String KEY_USERLOGIN = "UserLogin";
	public final static String KEY_LOGINOFFICEID = "LoginOffice";

	// -----------------------------------------------------------------------------------------------------------------

	public static HashMap<String, String> TextProperties;
	public static LatLng LoginLocation;

	private static RunTimeProperties mInstance = null;

	// -----------------------------------------------------------------------------------------------------------------

	private RunTimeProperties()
	{
		TextProperties = new HashMap<>();
	}

	// -----------------------------------------------------------------------------------------------------------------

	public static synchronized RunTimeProperties getInstance()
	{
		if (mInstance == null)
			mInstance = new RunTimeProperties();

		return mInstance;
	}

	// -----------------------------------------------------------------------------------------------------------------

}
