package com.abcorp.abid.idimageupload;

import android.Manifest;
import android.app.Activity;
import android.app.Fragment;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.abcorp.abid.camera.ExtSigCameraSource;
import com.abcorp.abid.camera.ExtSigCameraSourcePreview;
import com.google.android.gms.vision.CameraSource;

import org.opencv.android.CameraBridgeViewBase;
import org.opencv.core.Mat;

import java.io.IOException;

/**
 * Signature slip capture fragment. Used to capture a picture of the signature slip, containing a signature and the
 * QR code. If we're happy with what we've read, we store it and then move on to capture the applicant's face.
 * This is an alternative to the CameraFragment class which does everything in one shot. Some clients may not want
 * to have the applicants hold the signature slip in front of them.
 * Much of this Fragment is the same code as the CameraFragment, they just probably share a base class instead. However,
 * this is a Proof of Concept aimed to get feedback from end-users. It is quite likely that we're only going to end up
 * using only one anyway.
 */
public class SigSlipFragment extends Fragment implements CameraBridgeViewBase.CvCameraViewListener2
{
	private ExtSigCameraSource mCameraSource = null;
	private ExtSigCameraSourcePreview mPreview;
	private static SigSlipView mGraphicOverlay;

	private static final int RC_HANDLE_CAMERA_PERM = 2;	// permission request codes need to be < 256

	// -----------------------------------------------------------------------------------------------------------------

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.fragment_camera_sig, container, false);

		mPreview = view.findViewById(R.id.preview_sig_cam);
		mGraphicOverlay = view.findViewById(R.id.markerOverlay);

		// Check for the camera permission before accessing the camera.  If the
		// permission is not granted yet, request permission.
		int rc = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA);
		if (rc == PackageManager.PERMISSION_GRANTED)
		{
			createCameraSource();
		}
		else
		{
			requestCameraPermission();
		}

		return view;
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * Required to be implemented by CvCameraViewListener2 but not used here
	 * @param width -  the width of the frames that will be delivered
	 * @param height - the height of the frames that will be delivered
	 */
	public void onCameraViewStarted(int width, int height)
	{
	}

	/**
	 * Required to be implemented by CvCameraViewListener2 but not used here
	 */
	public void onCameraViewStopped()
	{
	}

	/**
	 * Required to be implemented by CvCameraViewListener2 but not used here
	 * @param inputFrame frame received
	 * @return Mat of the inputFrame
	 */
	public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame)
	{
		return inputFrame.rgba();
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * Handles the requesting of the camera permission.  This includes
	 * showing a "Snackbar" message of why the permission is needed then
	 * sending the request.
	 */
	private void requestCameraPermission()
	{
		Log.w(MainActivity.TAG, "Camera permission is not granted. Requesting permission");

		final String[] permissions = new String[]{Manifest.permission.CAMERA};

		if (!ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.CAMERA))
		{
			ActivityCompat.requestPermissions(getActivity(), permissions, RC_HANDLE_CAMERA_PERM);
			return;
		}

		final Activity thisActivity = getActivity();

		View.OnClickListener listener = new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				ActivityCompat.requestPermissions(thisActivity, permissions, RC_HANDLE_CAMERA_PERM);
			}
		};

		Snackbar.make(mGraphicOverlay, R.string.perm_cam_info,
				  Snackbar.LENGTH_INDEFINITE)
				  .setAction("ok", listener)
				  .show();
	}

	// -----------------------------------------------------------------------------------------------------------------


	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * Creates and starts the camera.  Note that this uses a higher resolution in comparison
	 * to other detection examples to enable the barcode detector to detect small barcodes
	 * at long distances.
	 */
	private void createCameraSource()
	{
		mCameraSource = new ExtSigCameraSource.Builder(getActivity(), mGraphicOverlay)
				  .setRequestedPreviewSize(640, 480)
				  .setFacing(CameraSource.CAMERA_FACING_BACK)
				  .setRequestedFps(30.0f)
				  .build();

		ExtSigCameraSource.SigSlipDetectedCallback sigSlipDetected = new ExtSigCameraSource.SigSlipDetectedCallback()
		{
			public void onSigSlipDetected()
			{
				Log.d(MainActivity.TAG, "SigSlip detected - take picture");
				mCameraSource.takePicture(shutterCallback, jpegCallback);
			}
		};

		mCameraSource.setSigSlipCallback(sigSlipDetected);
	}

	private final ExtSigCameraSource.AutoFocusCallback afCallback = new ExtSigCameraSource.AutoFocusCallback()
	{

		@Override
		public void onAutoFocus(boolean success)
		{
			Log.d(MainActivity.TAG, "SigSlip AutoFocus complete - take picture");
/*
			ExtSigCameraSource.PictureStartCallback startCallback = new ExtSigCameraSource.PictureStartCallback();
			startCallback.mDelegate = shutterCallback;
			ExtSigCameraSource.PictureDoneCallback doneCallback = new ExtSigCameraSource.PictureDoneCallback();
			doneCallback.mDelegate = jpegCallback;

*/
		}
	};

	// -----------------------------------------------------------------------------------------------------------------

	public void releaseCamera()
	{
		if (mCameraSource != null)
		{
			mCameraSource.release();
		}
	}

	/**
	 * Restarts the camera.
	 */
	@Override
	public void onResume()
	{
		super.onResume();
		startCameraSource();
	}

	/**
	 * Stops the camera.
	 */
	@Override
	public void onPause()
	{
		super.onPause();
		mPreview.stop();
	}

	/**
	 * Releases the resources associated with the camera source, the associated detector, and the
	 * rest of the processing pipeline.
	 */
	@Override
	public void onDestroy()
	{
		super.onDestroy();
		releaseCamera();
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * Callback for the result from requesting permissions. This method
	 * is invoked for every call on {@link #requestPermissions(String[], int)}.
	 * <p>
	 * <strong>Note:</strong> It is possible that the permissions request interaction
	 * with the user is interrupted. In this case you will receive empty permissions
	 * and results arrays which should be treated as a cancellation.
	 * </p>
	 *
	 * @param requestCode  The request code passed in {@link #requestPermissions(String[], int)}.
	 * @param permissions  The requested permissions. Never null.
	 * @param grantResults The grant results for the corresponding permissions
	 *                     which is either {@link PackageManager#PERMISSION_GRANTED}
	 *                     or {@link PackageManager#PERMISSION_DENIED}. Never null.
	 * @see #requestPermissions(String[], int)
	 */
	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
	{
		if (requestCode != RC_HANDLE_CAMERA_PERM)
		{
			Log.d(MainActivity.TAG, "Got unexpected permission result: " + requestCode);
			super.onRequestPermissionsResult(requestCode, permissions, grantResults);
			return;
		}

		if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
		{
			Log.d(MainActivity.TAG, "Camera permission granted - initialize the camera source");
			// we have permission, so create the camerasource
			createCameraSource();
			return;
		}

		Log.e(MainActivity.TAG, "Permission not granted: results len = " + grantResults.length +
				  " Result code = " + (grantResults.length > 0 ? grantResults[0] : "(empty)"));

		Toast.makeText(getActivity(), "Sorry!!!, you can't use this app without granting permission", Toast.LENGTH_LONG).show();
		////////finish();
	}

	// -----------------------------------------------------------------------------------------------------------------
	// Camera Source Preview
	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * Starts or restarts the camera source, if it exists.  If the camera source doesn't exist yet
	 * (e.g., because onResume was called before the camera source was created), this will be called
	 * again when the camera source is created.
	 */
	private void startCameraSource()
	{
		if (mCameraSource != null)
		{
			try
			{
				mPreview.start(mCameraSource, mGraphicOverlay);
			}
			catch (IOException e)
			{
				Log.e(MainActivity.TAG, "Unable to start camera source.", e);
				mCameraSource.release();
				mCameraSource = null;
			}
			catch(SecurityException ex)
			{
				Log.e(MainActivity.TAG, "SecurityException : Camera use has not been authorised.", ex);
				mCameraSource.release();
				mCameraSource = null;
			}
		}
	}

	// -----------------------------------------------------------------------------------------------------------------

	private final ExtSigCameraSource.ShutterCallback shutterCallback = new ExtSigCameraSource.ShutterCallback()
	{
		@Override
		public void onShutter()
		{
			Log.d(MainActivity.TAG, "onShutter");
			Toast.makeText(getActivity(), R.string.capture_info, Toast.LENGTH_SHORT).show();
		}
	};

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * Callback function that is triggered if we have detected the signature slip markers and taken a photo
	 */
	private ExtSigCameraSource.PictureCallback jpegCallback = new ExtSigCameraSource.PictureCallback()
	{
		@Override
		public void onPictureTaken(byte[] data)
		{
			// decipher the captured image and show the results
			MainActivity activity = (MainActivity)getActivity();
			Bitmap capturedImage = BitmapFactory.decodeByteArray(data, 0, data.length);

			// if the preview was rotated, make sure we rotate the captured image to handle this correctly
			if (capturedImage.getWidth() > capturedImage.getHeight())
			{
				Log.d(MainActivity.TAG, "Rotating image before processing");
				capturedImage = rotate(capturedImage, 90);
			}

			activity.sigSlipCaptureDone(capturedImage);
		}

		private Bitmap rotate(Bitmap in, int angle)
		{
			Matrix mat = new Matrix();
			mat.postRotate(angle);
			return Bitmap.createBitmap(in, 0, 0, in.getWidth(), in.getHeight(), mat, true);
		}
	};

	// -----------------------------------------------------------------------------------------------------------------

}
