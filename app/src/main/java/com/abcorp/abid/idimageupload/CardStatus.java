package com.abcorp.abid.idimageupload;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.app.Fragment;
import android.support.design.widget.Snackbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;


/**
 * Fragment that queries and displays the status of a particular card that has been uploaded and processed earlier
 * Normally this would interrogate the cloud database at ABCorp, but for this demo we're just emulating the
 * behaviour
 */
public class CardStatus extends Fragment
{

	private Button mIdCheckButton;
	private TextView mEdLicNo;
	private Spinner mCardTypeSelect;
	private TextView mStatus;
	private TextView mUploadedDate;
	private TextView mProcessedDate;
	private TextView mBatchId;
	private TextView mMailedDate;


	private static class ProdDetails
	{
		String Status;
		String UploadDate;
		String ProcessedDate;
		String BatchId;
		String MailoutDate;

		ProdDetails(String status, String uploadDate, String processedDate, String batchId, String mailoutDate)
		{
			Status = status;
			UploadDate = uploadDate;
			ProcessedDate = processedDate;
			BatchId = batchId;
			MailoutDate = mailoutDate;
		}
	}

	// Quick and dirty emulation of accessing the cloud database to check on IDs produced
	private static final Map<String, ProdDetails> mValidLicences = createMap();
	private static Map<String, ProdDetails> createMap()
	{
		Map<String, ProdDetails> cloudData = new LinkedHashMap<String, ProdDetails>();
		cloudData.put("WAD1234567", new ProdDetails("Valid", "11 Dec. 2016 OLABRT", "13 Dec. 2016", "26345", "14 Dec. 2016"));
		cloudData.put("WAL7654321", new ProdDetails("Valid", "5 Jan. 2016 LICTST", "6 Jan. 2016", "26005", "6 Jan. 2016"));
		cloudData.put("WPC3916991", new ProdDetails("In Resubmit", "14 Sep. 2015 LICFR1", "15 Sep. 2015", "25278", "15 Sep. 2016"));
		cloudData.put("WAD3985155", new ProdDetails("Submitted", "24 Feb. 2016 OLABUN", "27 Feb. 2016", "26055", "27 Feb. 2016"));
		cloudData.put("WAD4519726", new ProdDetails("Refused", "30 Jun. 2016 LICTMN", "1 Jul. 2016", "26189", "3 Jul. 2016"));
		cloudData.put("WAL4522592", new ProdDetails("Valid", "11 Dec. 2016 OLAPTH", "13 Dec. 2016", "26345", "14 Dec. 2016"));
		cloudData.put("WPC3591841", new ProdDetails("Valid", "5 May 2016 OLAMR2", "6 May 2016", "26159", "6 May 2016"));
		cloudData.put("WAD4044581", new ProdDetails("Valid", "9 Aug. 2016 APORTY", "10 Aug. 2016", "26235", "14 Aug. 2016"));
		cloudData.put("WAS4087581", new ProdDetails("Valid", "17 Oct. 2016 LICTS2", "19 Oct. 2016", "26287", "22 Oct. 2016"));
		cloudData.put("WPC4182765", new ProdDetails("Valid", "7 Dec. 2016 LICTS3", "8 Dec. 2016", "26335", "9 Dec. 2016"));
		return cloudData;
	}

	public CardStatus()
	{
		// Required empty public constructor
	}

	// -----------------------------------------------------------------------------------------------------------------

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		// Inflate the layout for this fragment
		View rootView = inflater.inflate(R.layout.fragment_card_status, container, false);

		mIdCheckButton = rootView.findViewById(R.id.btn_idcheck);
		mIdCheckButton.setEnabled(false);	// only enable if a licence ID has been entered

		mEdLicNo = rootView.findViewById(R.id.editIDNumber);
		mStatus = rootView.findViewById(R.id.lbl_cardstatus_val);
		mUploadedDate = rootView.findViewById(R.id.lbl_card_uploaddate_val);
		mProcessedDate = rootView.findViewById(R.id.lbl_card_processed_val);
		mBatchId = rootView.findViewById(R.id.lbl_card_batch_val);
		mMailedDate = rootView.findViewById(R.id.lbl_card_mailed_val);

		// Load our card types into the dropdown box
		mCardTypeSelect = rootView.findViewById(R.id.cardtype_select);
		ArrayAdapter<CharSequence> staticAdapter = ArrayAdapter
				  .createFromResource(getActivity(), R.array.card_types, android.R.layout.simple_spinner_item);

		staticAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mCardTypeSelect.setAdapter(staticAdapter);

		// Enable the check button only if we have got a 7 digit ID number
		mEdLicNo.addTextChangedListener(new TextWatcher()
		{

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after)
			{
				// nothing to do
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count)
			{
				// disable the check button unless we have a full licence ID
				mIdCheckButton.setEnabled(s.length() == 7);
			}

			@Override
			public void afterTextChanged(Editable s)
			{
				// nothing to do
			}
		});


		return rootView;
	}

	// -----------------------------------------------------------------------------------------------------------------

	private void hideKeyboard()
	{
		View view = getActivity().getCurrentFocus();
		if (view != null)
		{
			InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
		}
	}

	// -----------------------------------------------------------------------------------------------------------------

	public void onCheckIDStatus(View view)
	{
		hideKeyboard();		// hide the keyboard if it's still showing to make sure we don't cover our images

		String cardType = "WAD";		// default to driver
		String selectedCardType = mCardTypeSelect.getSelectedItem().toString();
		if (selectedCardType.length() > 3)
			cardType = selectedCardType.substring(0, 3);

		Context ctx = getActivity();
		AssetManager asm = ctx.getAssets();

		String typeAndId = cardType + mEdLicNo.getText().toString();
		if (mValidLicences.containsKey(typeAndId))
		{
			String imgFileName = "clouddb/" + typeAndId + "Port.png";
			loadCloudImage(imgFileName, R.id.cardstatus_portrait, asm);

			imgFileName = "clouddb/" + typeAndId + "Sig.png";
			loadCloudImage(imgFileName, R.id.cardstatus_signature, asm);

			ProdDetails prodDetails = mValidLicences.get(typeAndId);
			mStatus.setText(prodDetails.Status);
			mUploadedDate.setText(prodDetails.UploadDate);
			mProcessedDate.setText(prodDetails.ProcessedDate);
			mBatchId.setText(prodDetails.BatchId);
			mMailedDate.setText(prodDetails.MailoutDate);
		}
		else
		{
			String imgFileName = "clouddb/portrait_placeholder.png";
			loadCloudImage(imgFileName, R.id.cardstatus_portrait, asm);

			imgFileName = "clouddb/signature_placeholder.png";
			loadCloudImage(imgFileName, R.id.cardstatus_signature, asm);

			mStatus.setText("-");
			mUploadedDate.setText("-");
			mProcessedDate.setText("-");
			mBatchId.setText("-");
			mMailedDate.setText("-");

			// Display a hint for our demo that allows us to test with different numbers
			Random r = new Random();
			// Random number within our map, but ignore the first entry which we always want to show
			int index = r.nextInt(mValidLicences.size() - 2) + 1;
			String randomKey = mValidLicences.keySet().toArray()[index].toString();
			Snackbar.make(view, "Try the demo IDs WAD1234567 or " + randomKey, Snackbar.LENGTH_INDEFINITE)
					  .setAction("Action", null).show();
		}
	}

	// -----------------------------------------------------------------------------------------------------------------

	private void loadCloudImage(String fileName, int targetViewId, AssetManager asm)
	{
		try
		{
			InputStream isImg = asm.open(fileName);
			int fileSize = isImg.available();
			byte[] rawImgData = new byte[fileSize];
			BufferedInputStream buf = new BufferedInputStream(isImg);
			buf.read(rawImgData, 0, rawImgData.length);
			buf.close();

			Bitmap portrait = BitmapFactory.decodeByteArray(rawImgData, 0, rawImgData.length);
			ImageView iv = (ImageView)getView().findViewById(targetViewId);
			//int absWidth = ScreenSize.CalculatedPos(670);
			//int absHeight = (int)(absWidth/1.592);    // CR80 card height to width ratio
			//Bitmap bitmapScaled = Bitmap.createScaledBitmap(card.BaseStockFront, absWidth, absHeight, true);
			//ivBaseStock.setImageBitmap(bitmapScaled);
			iv.setImageBitmap(portrait);
		}
		catch (IOException e)
		{
			Log.e(MainActivity.TAG, "IOException on loading image [" + fileName + "] : " + e.getMessage());
		}
		catch(Exception e)
		{
			Log.e(MainActivity.TAG, "Exception on loading image [" + fileName + "] : " + e.getMessage());
		}
	}

	// -----------------------------------------------------------------------------------------------------------------

}
